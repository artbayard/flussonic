$(window).scroll(function () {
    var _block = $('.js-parallax'),
        _item = _block.find('svg'),
        yScrollPosition = window.scrollY;

    _item.css("transform", "translate3d(0, " + yScrollPosition * .7 + "px, 0)");
});
