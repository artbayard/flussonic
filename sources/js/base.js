AOS.init();

$(document).ready(function () {
    var body = $("body");
    var btn__menu = $(".btn__menu");
    var btn__search = $(".btn__search");
    var btn__close = $(".btn__close");
    var search__wrap = $(".search__wrap");
    var search__input = search__wrap.find('input');

    btn__menu.on("click", function (e) {
        body.toggleClass("menu_show");

        $($(this).data('target')).toggleClass("show");
    });

    btn__close.on("click", function (e) {
        body.toggleClass("menu_show");

        $(btn__menu.data('target')).toggleClass("show");
    });

    btn__search.on('click', function (e) {
        search__wrap.toggleClass("is_active");
        btn__search.toggleClass("is_active");
        search__input.focus();
        e.stopPropagation();
    });

    search__input.focusout(function (e) {
        search__wrap.removeClass("is_active");
        btn__search.removeClass("is_active");
    });

    $('[data-collapse]').on('click', function (e) {
        var _this = $(this);
        var _target = $(_this.data('collapse'));

        $(_this).toggleClass("show");
        $(_target).toggleClass("show");

        if (_target.hasClass('show')) {
            _target.css('height', _target.find('.about__list').outerHeight());
        }
        else {
            _target.css('height', 0);
        }
    });
});


$(window).scroll(function () {
    var header = $('header'),
        scroll = $(window).scrollTop();

    if (scroll > 0) {
        if (!header.hasClass('shadow')) {

            header.addClass('shadow');
        }
    } else {
        header.removeClass('shadow');
    }
});

$(window).scroll(function () {
    var _shift = 55;
    var _scrollPos = 0;
    var _st = $(this).scrollTop();

    if (_st > _scrollPos) {
        _shift = _shift + 55;
    } else {
        _shift = _shift - 55;
    }

    _shift = _shift + "px";

    $('.toparea__svg svg').css("transform", "translateY(" + (0 - (_st * .55))  + ")");

});

$(document).ready(function () {
    $('main section').viewportChecker({
        classToAdd: 'fade_in', // Class to add to the elements when they are visible,
        classToAddForFullView: '', // Class to add when an item is completely visible in the viewport
        classToRemove: '', // Class to remove before adding 'classToAdd' to the elements
        invertBottomOffset: false,
        offset: "30%", // The offset of the elements (let them appear earlier or later). This can also be percentage based by adding a '%' at the end
        repeat: false, // Add the possibility to remove the class if the elements are not visible
    });
});