var map,
    pointsOnMap,
    mapStyle = [{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#003cff"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#003cff"},{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry","stylers":[{"weight":"0.85"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"weight":"2.00"},{"color":"#b6c6ff"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#003cff"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#003cff"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"weight":"1"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b6c6ff"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry.stroke","stylers":[{"color":"#b6c6ff"}]},{"featureType":"transit","elementType":"geometry.fill","stylers":[{"color":"#b6c6ff"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#95baff"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]}];

pointsOnMap = [
    [55.856034, 37.558921, 1, {
        'head'    : 'офис Эрливидео',
        'address' : 'Адрес: Россия, Москва, Дмитровское шоссе, 71Б, офис 204',
        'tel'     : 'Телефон: +7 (499) 499-34-04'
    }],
];

// Function return array of markers that was create from "locations" and added to "map"
function setMarkers(map, locations, marker_url) {
    var markers = [];
    var image = new google.maps.MarkerImage(marker_url, null, null, null, new google.maps.Size(28,43));
    for (var i = 0; i < locations.length; i++) {
        var point    = locations[i];
        var myLatlng = new google.maps.LatLng(point[0], point[1]);
        var marker   = new google.maps.Marker({
            position : myLatlng,
            map      : map,
            icon     : image,
            title    : point[3].head,
            zIndex   : point[2]
        });
        marker.infoContent = point[3];
        markers.push(marker);
    }
    return markers;
}

// After function is complete all marker in array will contain object with info for infowindow
function setInfoWindowContent(arrayOfMarkers, infoWindow) {
    for (var i = 0; i < arrayOfMarkers.length; i++) {
        google.maps.event.addListener(arrayOfMarkers[i], 'click', function() {
            var content = composeInfoWindowContent(this.infoContent);
            infoWindow.setContent(content);
            infoWindow.open(map, this);
        });
    }
}

function composeInfoWindowContent(data) {
    return '<ul class="marker-info">' +
            '<li class="marker-info__head">'     + data.head    + '</li>' +
            '<li class="marker-info__address">'  + data.address + '</li>' +
            '<li class="marker-info__tel">'      + data.tel     + '</li>' +
        '</ul>';
}

function initMap(el) {
    var container, mapOptions;

    switch (typeof el) {
        case 'string':
            container = document.querySelector(el);
            break;
        case 'object':
            container = el;
            break;
        default:
            container = document.getElementById('map');
    }

    mapOptions = {
        zoom: 11,
        disableDefaultUI: false,
        scrollwheel: false,
        center: new google.maps.LatLng(55.856034, 37.558921),
        styles: mapStyle
    };

    map = new google.maps.Map(container, mapOptions);
    window.qwe = map;

    var mapMarkers = setMarkers(map, pointsOnMap, $(container).attr('data-marker-url'));
    // var mapInfoWindow = new google.maps.InfoWindow();

    // setInfoWindowContent(mapMarkers, mapInfoWindow);
}

function loadMap(el) {
    var script    = document.createElement('script');
    script.type   = 'text/javascript';
    var language = location.host === "erlyvideo.ru" ? "ru" : "en";
    script.src    = 'https://maps.googleapis.com/maps/api/js?v=3&signed_in=false&key=AIzaSyBUaGJ4VHtXY9cAxBMvJjV1WObfQWX70dQ&language='+language;
    script.onload = function() {
        initMap(el);
    };
    document.head.appendChild(script);
}
