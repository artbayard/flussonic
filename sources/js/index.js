$(document).ready(function () {
    $('#title').addClass('hover');

    $("main section.is_hover").hover(function () {
        $("main section.is_hover").removeClass('hover');
        $(this).addClass('hover');
    });

    $("header").hover(function () {
        $("main section.index__title").addClass('hover');
    });

    $("main section.is_hover").mouseleave(function () {
        if (!$(this).hasClass('index__title')) {
            $("main section.is_hover").removeClass('hover');
        }
    });

    loadMap('#map');
});
