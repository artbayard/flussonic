$(document).ready(function () {
    $('body').append('<div class="blackout"></div>');

    $('[data-popup]').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var _blackout = $('.blackout'),
            _popup = $('.popup-box'),
            _this = $(this),
            _idName = _this.data('popup'),
            _id = $(_idName),
            _scrollPos = $(window).scrollTop();

        _blackout.removeClass('show');
        _popup.removeClass('show');

        _blackout.addClass('show');
        _id.addClass('show');

        $('html,body').css('overflow', 'hidden');

        $('html').scrollTop(_scrollPos);
    });

    $('[class*=popup-box]').click(function (e) {
        e.stopPropagation();
    });

    $('.popup-close').click(function () {
        var _scrollPos = $(window).scrollTop();

        $('.blackout').removeClass('show');
        $(this).parents('.popup-box').removeClass('show');

        $("html,body").css("overflow", "auto");
        $('html').scrollTop(_scrollPos);
    });

    $('.blackout').click(function () {
        var _scrollPos = $(window).scrollTop();

        $('.popup-box').removeClass('show');
        $('.blackout').removeClass('show');

        $("html,body").css("overflow", "auto");
        $('html').scrollTop(_scrollPos);
    });
});