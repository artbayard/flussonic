webpackJsonp([0], {
    0: function (t, e, i) {
        "use strict";

        function s(t) {
            return t && t.__esModule ? t : {
                "default": t
            }
        }

        function r() {
            var t = (0, _["default"])(window).scrollTop(),
                e = 0 + .55 * t + "px";
            T.css("transform", "translateY(" + e + ")"), w.css("transform", "translateY(" + e + ")")
        }

        function n(t) {
            var e = (0, _["default"])(".img-anim img");
            e.addClass("is-hidden"), setTimeout(function () {
                e.removeClass("is-hidden")
            }, 800)
        }

        function a(t, e, i, s) {
            if (t.length) {
                var r = t.offset(),
                    n = r.top,
                    a = r.left,
                    i = a + t.width() + i,
                    s = n + s;
                console.log(" left " + i + " top " + s), e.css({
                    top: s,
                    left: i
                })
            }
        }

        function o(t, e, i, s) {
            if (t.length) {
                var r = t.offset(),
                    n = r.top,
                    a = r.left,
                    s = t.height(),
                    o = (0, _["default"])(document).width() - a - i;
                console.log(" bottom " + n + " right " + a), e.css({
                    right: o,
                    height: s
                })
            }
        }

        function l() {
            (0, _["default"])(".img-anim.is-channels").parents(".row").hasClass("fade-in") || (0, _["default"])(".js-line-channels").addClass("is-visible")
        }

        function h(t) {
            var e = arguments.length <= 1 || void 0 === arguments[1] ? 0 : arguments[1],
                i = arguments.length <= 2 || void 0 === arguments[2] ? _["default"].noop : arguments[2];
            if (!((0, _["default"])(window).width() <= 767)) {
                if ("string" == typeof t && (t = (0, _["default"])(t)), /drawsvg-initialized/.test(t.attr("class"))) return void t.drawsvg("animate");
                var s = t.hasClass("fast") ? 500 : 8e3;
                t.drawsvg({
                    duration: s,
                    stagger: 0,
                    reverse: !1,
                    callback: i
                }), setTimeout(function () {
                    t.drawsvg("animate")
                }, e)
            }
        }

        function u(t) {
            if (!(0, _["default"])(this).hasClass("is-active") && "default" != (0, _["default"])(this).data("href")) {
                (0, _["default"])(".hero-text, .hero-btn, .hero-figure").toggleClass("is-active"), h(".hero-figure.is-active svg");
                var e = (0, _["default"])("#project_type");
                if (e) {
                    var i = e.val(),
                        s = "iptv" == i ? "vsaas" : "iptv";
                    e.val(s)
                }
                var r = (0, _["default"])(this).data("link");
                if (r) {
                    var n = "watcher_trial" == r ? "/trial?watcher" : "/trial";
                    window.history.pushState({
                        link: r
                    }, "", n)
                }
            }
        }

        function c(t, e) {
            for (var i = e, s = 0, r = 0; r < t.length; r++) {
                var n = t[r],
                    a = n[0],
                    o = n[1];
                if (i <= 0) break;
                0 == o ? (s += i * a, i = 0) : (s += Math.min(i, o) * a, i -= o)
            }
            return s
        }
        var f = i(1),
            _ = s(f),
            d = i(2);
        s(d);
        i(3), i(4), i(12), i(13);
        var p = i(14),
            m = s(p),
            g = i(17),
            v = s(g);
        i(18);
        i(19);
        i(20), window.$ = window.jQuery = _["default"], (0, m["default"])(), (0, v["default"])(), (0, _["default"])(".menu-button, .menu .close-button").on("click touchend", function (t) {
            t.preventDefault(), (0, _["default"])("body").toggleClass("is-show-menu")
        });
        var y = (0, _["default"])(".js-accord"),
            x = (0, _["default"])(".js-accord-block");
        (0, _["default"])(".js-accord-top").on("click", function () {
            var t = (0, _["default"])(this),
                e = t.parents(".js-accord"),
                i = e.find(".js-accord-block");
            return e.hasClass("is-active") ? (e.removeClass("is-active"), i.slideUp(300)) : (y.removeClass("is-active"), x.slideUp(300), e.addClass("is-active"), i.slideDown(300)), setTimeout(function () {
                header_bg()
            }, 350), !1
        }), y.each(function () {
            var t = (0, _["default"])(this),
                e = t.find(x);
            t.hasClass("is-active") && e.show()
        }), (0, _["default"])(".js-sort").on("click", function () {
            return (0, _["default"])(this).toggleClass("is-active"), !1
        }), (0, _["default"])(".js-select").each(function () {
            var t = (0, _["default"])(this),
                e = t.children("option").length;
            t.wrap('<div class="select"></div>'), t.after('<div class="styledSelect"></div>');
            var i = t.next("div.styledSelect");
            i.text(t.children("option").eq(0).text());
            for (var s = (0, _["default"])("<ul />", {
                "class": "options"
            }).insertAfter(i), r = 0; r < e; r++)(0, _["default"])("<li />", {
                text: t.children("option").eq(r).text(),
                rel: t.children("option").eq(r).val()
            }).appendTo(s);
            var n = s.children("li");
            i.on("click", function (t) {
                t.stopPropagation(), (0, _["default"])("div.styledSelect.is-active").each(function () {
                    (0, _["default"])(this).removeClass("is-active").next("ul.options").hide()
                }), (0, _["default"])(this).toggleClass("is-active").next("ul.options").toggle();
                var e = (0, _["default"])(this).next("ul.options").find("li"),
                    i = (0, _["default"])(this).next("ul.options");
                if (e.hasClass("is-active")) {
                    var s = (0, _["default"])(this).next("ul.options").find("li.is-active"),
                        r = ((0, _["default"])(this).next("ul.options").find("li:first"), s.position()),
                        n = r.top,
                        a = n + 10;
                    i.css("top", -a)
                }
            }), n.on("click", function (e) {
                e.stopPropagation(), i.text((0, _["default"])(this).text()).removeClass("is-active"), t.val((0, _["default"])(this).attr("rel")), n.removeClass("is-active"), (0, _["default"])(this).addClass("is-active"), s.hide()
            }), (0, _["default"])(document).on("click", function () {
                i.removeClass("is-active"), s.hide()
            })
        });
        var T = ((0, _["default"])(".js-svg-anim"), (0, _["default"])(".js-svg-anim svg").each(function (t, e) {
            var i = (0, _["default"])(e),
                s = i.parent().hasClass("fast") ? 500 : 8e3;
            i.drawsvg({
                duration: s,
                easing: "linear"
            })
        })),
            w = (0, _["default"])(".js-svg-anim-type svg").drawsvg(),
            b = (0, _["default"])(".js-svg-anim-ip > svg").drawsvg({
                duration: 800,
                stagger: 0
            }),
            P = (0, _["default"])(".js-svg-anim-type.is-plate > svg").drawsvg({
                duration: 1400,
                stagger: 0
            }),
            k = (0, _["default"])(".js-svg-anim-line svg");
        setTimeout(function () {
            T.fadeIn(300).drawsvg("animate"), w.fadeIn(300).drawsvg("animate"), b.fadeIn(300).drawsvg("animate"), P.fadeIn(300).drawsvg("animate")
        }, 800), setTimeout(function () {
            k.fadeIn(600)
        }, 1400), (0, _["default"])(window).on("scroll", function (t) {
            r()
        }), n();
        var O = (0, _["default"])(".img-anim-line.is-user"),
            C = (0, _["default"])(".js-bot-user-line"),
            S = (0, _["default"])(".img-anim.is-device");
        (0, _["default"])(window).resize(function () {
            a(C, O, 3, 12), o(S, O, 12, -300)
        }), setTimeout(function () {
            a(C, O, 3, 12), o(S, O, 12, -300)
        }, 1e3), setTimeout(function () {
            (0, _["default"])(".js-top-iptv-line, .img-anim.is-cam, .img-anim.is-cub").addClass("is-visible"), O.addClass("is-visible")
        }, 1200), l(), (0, _["default"])(document).on("scroll", function () {
            l()
        }), (0, _["default"])(".js-search-btn").on("click", function () {
            var t = (0, _["default"])(this),
                e = t.parents(".js-right-parent"),
                i = e.find(".js-search-input"),
                s = e.find(".js-search-wrap");
            e.toggleClass("is-active"), s.toggleClass("is-active"), i.focus()
        }), (0, _["default"])(".js-right-parent").on("click", function (t) {
            t.stopPropagation()
        }), (0, _["default"])("body").on("click", function () {
            (0, _["default"])(".js-right-parent").removeClass("is-active"), (0, _["default"])(".js-search-wrap").removeClass("is-active")
        }), (0, _["default"])(document).ready(function () {
            var t = window.location.hash,
                e = window.location.href.split("#")[1];
            return !(!t || t != e) && void (0, _["default"])("html, body").animate({
                scrollTop: (0, _["default"])(t).offset().top - 30
            }, 600)
        }), h(".hero-figure.is-active svg", 300), (0, _["default"])(".hero-btn:not(.noclick)").on("click touchend", u),
            function () {
                var t = (0, _["default"])(window).width();
                if ((0, _["default"])(window).resize(function () {
                    t = (0, _["default"])(window).width(), t > 899 && (0, _["default"])(".js-wather-list").removeAttr("style")
                }), (0, _["default"])(".js-wather-list")) {
                    var e = (0, _["default"])(".js-wather-list"),
                        i = (e.find(".is-active a"), window.location.host.match("erlyvideo.ru") ? "Оглавление" : "Contents"),
                        s = (0, _["default"])(".js-doc-menu-view");
                    s.append("<span>" + i + "</span>")
                }
            }(), (0, _["default"])(".js-doc-menu-view").on("click", function () {
                var t = ((0, _["default"])(this), (0, _["default"])(window).width());
                if (t < 1129) {
                    var e = (0, _["default"])(".js-wather-list");
                    e.slideToggle(300)
                }
            }), (0, _["default"])(".js-accord-btn").on("click", function () {
                (0, _["default"])(".js-my-accord").toggleClass("is-open"), (0, _["default"])(".js-accord-cont").slideToggle(300)
            }), "#features" == window.location.hash && (0, _["default"])("#features").parent().find(".js-accord-btn").click(), (0, _["default"])(".js-bfilter-view").on("click", function () {
                (0, _["default"])(".js-bfilter-list").slideToggle(150)
            }), (0, _["default"])(".js-bfilter-item").on("click", function (t) {
                t.stopPropagation();
                var e = (0, _["default"])(this),
                    i = e.find("span").html(),
                    s = (0, _["default"])(".js-bfilter-view .l"),
                    r = (0, _["default"])(".js-bfilter-list");
                (0, _["default"])(".js-bfilter-item").removeClass("is-active"), e.addClass("is-active"), s.text(i), e.hasClass("js-bfilter-news") ? ((0, _["default"])(".type-post").addClass("is-hidden-post"), (0, _["default"])(".type-news").removeClass("is-hidden-post")) : e.hasClass("js-bfilter-articles") ? ((0, _["default"])(".type-news").addClass("is-hidden-post"), (0, _["default"])(".type-post").removeClass("is-hidden-post")) : (0, _["default"])(".type-post, .type-news").removeClass("is-hidden-post"), r.slideUp(150)
            }), (0, _["default"])(".js-header") && (window.onscroll = function () {
                var t = window.pageYOffset || document.documentElement.scrollTop,
                    e = (0, _["default"])(".js-header");
                t > 0 ? e.addClass("is-fixed") : e.removeClass("is-fixed")
            }), (0, _["default"])(".try-link").on("click", function (t) {
                t.preventDefault(), (0, _["default"])("#success_message").css({
                    opacity: 0,
                    display: "block"
                });
                var e = (0, _["default"])(this).closest(".licensePlan"),
                    i = e.find(".licensePlan__title").data("title"),
                    s = window[i];
                if (s || (window.watcher_plans && (s = window.watcher_plans.find(function (t) {
                    return t.name == i
                })), window.streamer_plans && (s = window.streamer_plans.find(function (t) {
                    return t.name == i
                }))), "function" == typeof ga) {
                    var r = s.ga_event;
                    ga("send", "event", r, "Click")
                }
                window.qty = e.find(".licensePlan__orderForm__quantity__input > select,input").val(), window.selected_plan = e.find(".licensePlan__title__content").text(), (0, _["default"])("#try-form").modal()
            }), (0, _["default"])(window).bind("popstate", function (t) {
                (0, _["default"])(".hero-text, .hero-btn, .hero-figure").toggleClass("is-active")
            }), (0, _["default"])(document).on("input", ".Watcher input", function (t) {
                t.preventDefault();
                var e = (0, _["default"])(this).closest(".licensePlan"),
                    i = e.find(".licensePlan__title").data("title"),
                    s = window.watcher_plans.find(function (t) {
                        return t.name == i
                    }),
                    r = (0, _["default"])(this).val();
                (!_["default"].isNumeric(r) || r < s.min_cameras) && (r = s.min_cameras);
                var n = c(s.tiers, r);
                e.find(".licensePlan__pricing__amount span").first().text(n)
            }), (0, _["default"])(document).on("input", ".Streamer select", function (t) {
                t.preventDefault();
                var e = (0, _["default"])(this).closest(".licensePlan"),
                    i = e.find(".licensePlan__title").data("title"),
                    s = window.streamer_plans.find(function (t) {
                        return t.name == i
                    }),
                    r = (0, _["default"])(this).val();
                (!_["default"].isNumeric(r) || r < s.min_cameras) && (r = s.min_cameras);
                var n = c(s.tiers, r);
                e.find(".licensePlan__pricing__amount span").first().text(n)
            });
        var A = function (t) {
            (0, _["default"])(document).on("change", "." + t + " select", function (e) {
                e.preventDefault();
                var i = (0, _["default"])(this).closest("." + t),
                    s = window[t],
                    r = (0, _["default"])(this).val();
                (!_["default"].isNumeric(r) || r <= 0) && (r = 1);
                var n = c(s.tiers, r);
                i.find(".licensePlan__pricing__amount span").first().text(n)
            })
        };
        A("Simple"), A("Perpetual")
    },
    12: function (t, e, i) {
        "use strict";

        function s(t) {
            return t && t.__esModule ? t : {
                "default": t
            }
        }
        var r = i(5),
            n = s(r),
            a = i(1),
            o = s(a);
        (0, o["default"])(function () {
            (0, o["default"])("#blog_email_form").submit(function (t) {
                t.preventDefault(), (0, o["default"])("#error_message").css({
                    opacity: 0,
                    display: "none"
                }).html("");
                var e = (0, o["default"])(this).find("#post_permalink").val(),
                    i = (0, o["default"])(this).find("#name").val(),
                    s = (0, o["default"])(this).find("#email").val();
                s && (s = s.toLowerCase().replace(" ", ""), (0, o["default"])(this).find("#email").val(s));
                var r = (0, o["default"])(this).find("#authenticity_token").val(),
                    a = (0, o["default"])("#blog_email_form button"),
                    l = a.find("span"),
                    h = a.find("img");
                if (!s || s.length < 4 || s.indexOf("@") == -1) return (0, o["default"])("#error_message").html("We need your correct email to give you a key").css({
                    opacity: 0,
                    display: "block"
                }).animate({
                    opacity: 1
                }, 200), !1;
                var u = s.split("@")[1],
                    c = {
                        "mailinator.com": !0
                    };
                if (c[u]) return (0, o["default"])("#error_message").html("Please, use good email domain").css({
                    opacity: 0,
                    display: "block"
                }).animate({
                    opacity: 1
                }, 200), !1;
                (0, o["default"])("#success_message").css({
                    opacity: 0,
                    display: "block"
                }), a.attr("disabled", !0);
                var f = a.find("span").text();
                return l.text("Please wait..."), h.removeClass("is-hidden"), o["default"].ajax({
                    url: (0, o["default"])(this).attr("action"),
                    method: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: (0, n["default"])({
                        name: i,
                        email: s,
                        authenticity_token: r,
                        post_permalink: e
                    }),
                    error: function () {
                        (0, o["default"])("#error_message").html("Failed to complete request. Please try again later").css({
                            opacity: 0,
                            display: "block"
                        }).animate({
                            opacity: 1
                        }, 200), a.attr("disabled", !1), l.text(f), h.addClass("is-hidden")
                    },
                    success: function (t) {
                        (0, o["default"])("#success_message").animate({
                            opacity: 1
                        }, 200), a.attr("disabled", !1), l.text(f), h.addClass("is-hidden")
                    }
                }), !1
            })
        })
    },
    17: function (t, e, i) {
        var s, r;
        ! function (i, n) {
            s = [], r = function () {
                return i.svg4everybody = n()
            }.apply(e, s), !(void 0 !== r && (t.exports = r))
        }(this, function () { /*! svg4everybody v2.1.0 | github.com/jonathantneal/svg4everybody */
            function t(t, e) {
                if (e) {
                    var i = document.createDocumentFragment(),
                        s = !t.getAttribute("viewBox") && e.getAttribute("viewBox");
                    s && t.setAttribute("viewBox", s);
                    for (var r = e.cloneNode(!0); r.childNodes.length;) i.appendChild(r.firstChild);
                    t.appendChild(i)
                }
            }

            function e(e) {
                e.onreadystatechange = function () {
                    if (4 === e.readyState) {
                        var i = e._cachedDocument;
                        i || (i = e._cachedDocument = document.implementation.createHTMLDocument(""), i.body.innerHTML = e.responseText, e._cachedTarget = {}), e._embeds.splice(0).map(function (s) {
                            var r = e._cachedTarget[s.id];
                            r || (r = e._cachedTarget[s.id] = i.getElementById(s.id)), t(s.svg, r)
                        })
                    }
                }, e.onreadystatechange()
            }

            function i(i) {
                function s() {
                    for (var i = 0; i < c.length;) {
                        var a = c[i],
                            o = a.parentNode;
                        if (o && /svg/i.test(o.nodeName)) {
                            var l = a.getAttribute("xlink:href");
                            if (r && (!n.validate || n.validate(l, o, a))) {
                                o.removeChild(a);
                                var f = l.split("#"),
                                    _ = f.shift(),
                                    d = f.join("#");
                                if (_.length) {
                                    var p = h[_];
                                    p || (p = h[_] = new XMLHttpRequest, p.open("GET", _), p.send(), p._embeds = []), p._embeds.push({
                                        svg: o,
                                        id: d
                                    }), e(p)
                                } else t(o, document.getElementById(d))
                            }
                        } else ++i
                    }
                    u(s, 67)
                }
                var r, n = Object(i),
                    a = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,
                    o = /\bAppleWebKit\/(\d+)\b/,
                    l = /\bEdge\/12\.(\d+)\b/;
                r = "polyfill" in n ? n.polyfill : a.test(navigator.userAgent) || (navigator.userAgent.match(l) || [])[1] < 10547 || (navigator.userAgent.match(o) || [])[1] < 537;
                var h = {},
                    u = window.requestAnimationFrame || setTimeout,
                    c = document.getElementsByTagName("use");
                r && s()
            }
            return i
        })
    },
    19: function (t, e, i) {
        var s, r;
        (function (i) {
            /*!
             * VERSION: 1.20.2
             * DATE: 2017-06-30
             * UPDATES AND DOCS AT: http://greensock.com
             * 
             * Includes all of the following: TweenLite, TweenMax, TimelineLite, TimelineMax, EasePack, CSSPlugin, RoundPropsPlugin, BezierPlugin, AttrPlugin, DirectionalRotationPlugin
             *
             * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
             * This work is subject to the terms at http://greensock.com/standard-license or for
             * Club GreenSock members, the software agreement that was issued with your membership.
             * 
             * @author: Jack Doyle, jack@greensock.com
             **/
            var n = "undefined" != typeof t && t.exports && "undefined" != typeof i ? i : this || window;
            (n._gsQueue || (n._gsQueue = [])).push(function () {
                "use strict";
                n._gsDefine("TweenMax", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (t, e, i) {
                    var s = function (t) {
                        var e, i = [],
                            s = t.length;
                        for (e = 0; e !== s; i.push(t[e++]));
                        return i
                    },
                        r = function (t, e, i) {
                            var s, r, n = t.cycle;
                            for (s in n) r = n[s], t[s] = "function" == typeof r ? r(i, e[i]) : r[i % r.length];
                            delete t.cycle
                        },
                        n = function (t, e, s) {
                            i.call(this, t, e, s), this._cycle = 0, this._yoyo = this.vars.yoyo === !0 || !!this.vars.yoyoEase, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._dirty = !0, this.render = n.prototype.render
                        },
                        a = 1e-10,
                        o = i._internals,
                        l = o.isSelector,
                        h = o.isArray,
                        u = n.prototype = i.to({}, .1, {}),
                        c = [];
                    n.version = "1.20.2", u.constructor = n, u.kill()._gc = !1, n.killTweensOf = n.killDelayedCallsTo = i.killTweensOf, n.getTweensOf = i.getTweensOf, n.lagSmoothing = i.lagSmoothing, n.ticker = i.ticker, n.render = i.render, u.invalidate = function () {
                        return this._yoyo = this.vars.yoyo === !0 || !!this.vars.yoyoEase, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._yoyoEase = null, this._uncache(!0), i.prototype.invalidate.call(this)
                    }, u.updateTo = function (t, e) {
                        var s, r = this.ratio,
                            n = this.vars.immediateRender || t.immediateRender;
                        e && this._startTime < this._timeline._time && (this._startTime = this._timeline._time, this._uncache(!1), this._gc ? this._enabled(!0, !1) : this._timeline.insert(this, this._startTime - this._delay));
                        for (s in t) this.vars[s] = t[s];
                        if (this._initted || n)
                            if (e) this._initted = !1, n && this.render(0, !0, !0);
                            else if (this._gc && this._enabled(!0, !1), this._notifyPluginsOfEnabled && this._firstPT && i._onPluginEvent("_onDisable", this), this._time / this._duration > .998) {
                                var a = this._totalTime;
                                this.render(0, !0, !1), this._initted = !1, this.render(a, !0, !1)
                            } else if (this._initted = !1, this._init(), this._time > 0 || n)
                                for (var o, l = 1 / (1 - r), h = this._firstPT; h;) o = h.s + h.c, h.c *= l, h.s = o - h.c, h = h._next;
                        return this
                    }, u.render = function (t, e, s) {
                        this._initted || 0 === this._duration && this.vars.repeat && this.invalidate();
                        var r, n, l, h, u, c, f, _, d, p = this._dirty ? this.totalDuration() : this._totalDuration,
                            m = this._time,
                            g = this._totalTime,
                            v = this._cycle,
                            y = this._duration,
                            x = this._rawPrevTime;
                        if (t >= p - 1e-7 && t >= 0 ? (this._totalTime = p, this._cycle = this._repeat, this._yoyo && 0 !== (1 & this._cycle) ? (this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0) : (this._time = y, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1), this._reversed || (r = !0, n = "onComplete", s = s || this._timeline.autoRemoveChildren), 0 === y && (this._initted || !this.vars.lazy || s) && (this._startTime === this._timeline._duration && (t = 0), (x < 0 || t <= 0 && t >= -1e-7 || x === a && "isPause" !== this.data) && x !== t && (s = !0, x > a && (n = "onReverseComplete")), this._rawPrevTime = _ = !e || t || x === t ? t : a)) : t < 1e-7 ? (this._totalTime = this._time = this._cycle = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== g || 0 === y && x > 0) && (n = "onReverseComplete", r = this._reversed), t < 0 && (this._active = !1, 0 === y && (this._initted || !this.vars.lazy || s) && (x >= 0 && (s = !0), this._rawPrevTime = _ = !e || t || x === t ? t : a)), this._initted || (s = !0)) : (this._totalTime = this._time = t, 0 !== this._repeat && (h = y + this._repeatDelay, this._cycle = this._totalTime / h >> 0, 0 !== this._cycle && this._cycle === this._totalTime / h && g <= t && this._cycle-- , this._time = this._totalTime - this._cycle * h, this._yoyo && 0 !== (1 & this._cycle) && (this._time = y - this._time, d = this._yoyoEase || this.vars.yoyoEase, d && (this._yoyoEase || (d !== !0 || this._initted ? this._yoyoEase = d = d === !0 ? this._ease : d instanceof Ease ? d : Ease.map[d] : (d = this.vars.ease, this._yoyoEase = d = d ? d instanceof Ease ? d : "function" == typeof d ? new Ease(d, this.vars.easeParams) : Ease.map[d] || i.defaultEase : i.defaultEase)), this.ratio = d ? 1 - d.getRatio((y - this._time) / y) : 0)), this._time > y ? this._time = y : this._time < 0 && (this._time = 0)), this._easeType && !d ? (u = this._time / y, c = this._easeType, f = this._easePower, (1 === c || 3 === c && u >= .5) && (u = 1 - u), 3 === c && (u *= 2), 1 === f ? u *= u : 2 === f ? u *= u * u : 3 === f ? u *= u * u * u : 4 === f && (u *= u * u * u * u), 1 === c ? this.ratio = 1 - u : 2 === c ? this.ratio = u : this._time / y < .5 ? this.ratio = u / 2 : this.ratio = 1 - u / 2) : d || (this.ratio = this._ease.getRatio(this._time / y))), m === this._time && !s && v === this._cycle) return void (g !== this._totalTime && this._onUpdate && (e || this._callback("onUpdate")));
                        if (!this._initted) {
                            if (this._init(), !this._initted || this._gc) return;
                            if (!s && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = m, this._totalTime = g, this._rawPrevTime = x, this._cycle = v, o.lazyTweens.push(this), void (this._lazy = [t, e]);
                            !this._time || r || d ? r && this._ease._calcEnd && !d && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1)) : this.ratio = this._ease.getRatio(this._time / y)
                        }
                        for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== m && t >= 0 && (this._active = !0), 0 === g && (2 === this._initted && t > 0 && this._init(), this._startAt && (t >= 0 ? this._startAt.render(t, e, s) : n || (n = "_dummyGS")), this.vars.onStart && (0 === this._totalTime && 0 !== y || e || this._callback("onStart"))), l = this._firstPT; l;) l.f ? l.t[l.p](l.c * this.ratio + l.s) : l.t[l.p] = l.c * this.ratio + l.s, l = l._next;
                        this._onUpdate && (t < 0 && this._startAt && this._startTime && this._startAt.render(t, e, s), e || (this._totalTime !== g || n) && this._callback("onUpdate")), this._cycle !== v && (e || this._gc || this.vars.onRepeat && this._callback("onRepeat")), n && (this._gc && !s || (t < 0 && this._startAt && !this._onUpdate && this._startTime && this._startAt.render(t, e, s), r && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[n] && this._callback(n), 0 === y && this._rawPrevTime === a && _ !== a && (this._rawPrevTime = 0)))
                    }, n.to = function (t, e, i) {
                        return new n(t, e, i)
                    }, n.from = function (t, e, i) {
                        return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new n(t, e, i)
                    }, n.fromTo = function (t, e, i, s) {
                        return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, new n(t, e, s)
                    }, n.staggerTo = n.allTo = function (t, e, a, o, u, f, _) {
                        o = o || 0;
                        var d, p, m, g, v = 0,
                            y = [],
                            x = function () {
                                a.onComplete && a.onComplete.apply(a.onCompleteScope || this, arguments), u.apply(_ || a.callbackScope || this, f || c)
                            },
                            T = a.cycle,
                            w = a.startAt && a.startAt.cycle;
                        for (h(t) || ("string" == typeof t && (t = i.selector(t) || t), l(t) && (t = s(t))), t = t || [], o < 0 && (t = s(t), t.reverse(), o *= -1), d = t.length - 1, m = 0; m <= d; m++) {
                            p = {};
                            for (g in a) p[g] = a[g];
                            if (T && (r(p, t, m), null != p.duration && (e = p.duration, delete p.duration)), w) {
                                w = p.startAt = {};
                                for (g in a.startAt) w[g] = a.startAt[g];
                                r(p.startAt, t, m)
                            }
                            p.delay = v + (p.delay || 0), m === d && u && (p.onComplete = x), y[m] = new n(t[m], e, p), v += o
                        }
                        return y
                    }, n.staggerFrom = n.allFrom = function (t, e, i, s, r, a, o) {
                        return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, n.staggerTo(t, e, i, s, r, a, o)
                    }, n.staggerFromTo = n.allFromTo = function (t, e, i, s, r, a, o, l) {
                        return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, n.staggerTo(t, e, s, r, a, o, l)
                    }, n.delayedCall = function (t, e, i, s, r) {
                        return new n(e, 0, {
                            delay: t,
                            onComplete: e,
                            onCompleteParams: i,
                            callbackScope: s,
                            onReverseComplete: e,
                            onReverseCompleteParams: i,
                            immediateRender: !1,
                            useFrames: r,
                            overwrite: 0
                        })
                    }, n.set = function (t, e) {
                        return new n(t, 0, e)
                    }, n.isTweening = function (t) {
                        return i.getTweensOf(t, !0).length > 0
                    };
                    var f = function (t, e) {
                        for (var s = [], r = 0, n = t._first; n;) n instanceof i ? s[r++] = n : (e && (s[r++] = n), s = s.concat(f(n, e)), r = s.length), n = n._next;
                        return s
                    },
                        _ = n.getAllTweens = function (e) {
                            return f(t._rootTimeline, e).concat(f(t._rootFramesTimeline, e))
                        };
                    n.killAll = function (t, i, s, r) {
                        null == i && (i = !0), null == s && (s = !0);
                        var n, a, o, l = _(0 != r),
                            h = l.length,
                            u = i && s && r;
                        for (o = 0; o < h; o++) a = l[o], (u || a instanceof e || (n = a.target === a.vars.onComplete) && s || i && !n) && (t ? a.totalTime(a._reversed ? 0 : a.totalDuration()) : a._enabled(!1, !1))
                    }, n.killChildTweensOf = function (t, e) {
                        if (null != t) {
                            var r, a, u, c, f, _ = o.tweenLookup;
                            if ("string" == typeof t && (t = i.selector(t) || t), l(t) && (t = s(t)), h(t))
                                for (c = t.length; --c > -1;) n.killChildTweensOf(t[c], e);
                            else {
                                r = [];
                                for (u in _)
                                    for (a = _[u].target.parentNode; a;) a === t && (r = r.concat(_[u].tweens)), a = a.parentNode;
                                for (f = r.length, c = 0; c < f; c++) e && r[c].totalTime(r[c].totalDuration()), r[c]._enabled(!1, !1)
                            }
                        }
                    };
                    var d = function (t, i, s, r) {
                        i = i !== !1, s = s !== !1, r = r !== !1;
                        for (var n, a, o = _(r), l = i && s && r, h = o.length; --h > -1;) a = o[h], (l || a instanceof e || (n = a.target === a.vars.onComplete) && s || i && !n) && a.paused(t)
                    };
                    return n.pauseAll = function (t, e, i) {
                        d(!0, t, e, i)
                    }, n.resumeAll = function (t, e, i) {
                        d(!1, t, e, i)
                    }, n.globalTimeScale = function (e) {
                        var s = t._rootTimeline,
                            r = i.ticker.time;
                        return arguments.length ? (e = e || a, s._startTime = r - (r - s._startTime) * s._timeScale / e, s = t._rootFramesTimeline, r = i.ticker.frame, s._startTime = r - (r - s._startTime) * s._timeScale / e, s._timeScale = t._rootTimeline._timeScale = e, e) : s._timeScale
                    }, u.progress = function (t, e) {
                        return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), e) : this._time / this.duration()
                    }, u.totalProgress = function (t, e) {
                        return arguments.length ? this.totalTime(this.totalDuration() * t, e) : this._totalTime / this.totalDuration()
                    }, u.time = function (t, e) {
                        return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time
                    }, u.duration = function (e) {
                        return arguments.length ? t.prototype.duration.call(this, e) : this._duration
                    }, u.totalDuration = function (t) {
                        return arguments.length ? this._repeat === -1 ? this : this.duration((t - this._repeat * this._repeatDelay) / (this._repeat + 1)) : (this._dirty && (this._totalDuration = this._repeat === -1 ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat, this._dirty = !1), this._totalDuration)
                    }, u.repeat = function (t) {
                        return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat
                    }, u.repeatDelay = function (t) {
                        return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay
                    }, u.yoyo = function (t) {
                        return arguments.length ? (this._yoyo = t, this) : this._yoyo
                    }, n
                }, !0), n._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (t, e, i) {
                    var s = function (t) {
                        e.call(this, t), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate;
                        var i, s, r = this.vars;
                        for (s in r) i = r[s], h(i) && i.join("").indexOf("{self}") !== -1 && (r[s] = this._swapSelfInParams(i));
                        h(r.tweens) && this.add(r.tweens, 0, r.align, r.stagger)
                    },
                        r = 1e-10,
                        a = i._internals,
                        o = s._internals = {},
                        l = a.isSelector,
                        h = a.isArray,
                        u = a.lazyTweens,
                        c = a.lazyRender,
                        f = n._gsDefine.globals,
                        _ = function (t) {
                            var e, i = {};
                            for (e in t) i[e] = t[e];
                            return i
                        },
                        d = function (t, e, i) {
                            var s, r, n = t.cycle;
                            for (s in n) r = n[s], t[s] = "function" == typeof r ? r(i, e[i]) : r[i % r.length];
                            delete t.cycle
                        },
                        p = o.pauseCallback = function () { },
                        m = function (t) {
                            var e, i = [],
                                s = t.length;
                            for (e = 0; e !== s; i.push(t[e++]));
                            return i
                        },
                        g = s.prototype = new e;
                    return s.version = "1.20.2", g.constructor = s, g.kill()._gc = g._forcingPlayhead = g._hasPause = !1, g.to = function (t, e, s, r) {
                        var n = s.repeat && f.TweenMax || i;
                        return e ? this.add(new n(t, e, s), r) : this.set(t, s, r)
                    }, g.from = function (t, e, s, r) {
                        return this.add((s.repeat && f.TweenMax || i).from(t, e, s), r)
                    }, g.fromTo = function (t, e, s, r, n) {
                        var a = r.repeat && f.TweenMax || i;
                        return e ? this.add(a.fromTo(t, e, s, r), n) : this.set(t, r, n)
                    }, g.staggerTo = function (t, e, r, n, a, o, h, u) {
                        var c, f, p = new s({
                            onComplete: o,
                            onCompleteParams: h,
                            callbackScope: u,
                            smoothChildTiming: this.smoothChildTiming
                        }),
                            g = r.cycle;
                        for ("string" == typeof t && (t = i.selector(t) || t), t = t || [], l(t) && (t = m(t)), n = n || 0, n < 0 && (t = m(t), t.reverse(), n *= -1), f = 0; f < t.length; f++) c = _(r), c.startAt && (c.startAt = _(c.startAt), c.startAt.cycle && d(c.startAt, t, f)), g && (d(c, t, f), null != c.duration && (e = c.duration, delete c.duration)), p.to(t[f], e, c, f * n);
                        return this.add(p, a)
                    }, g.staggerFrom = function (t, e, i, s, r, n, a, o) {
                        return i.immediateRender = 0 != i.immediateRender, i.runBackwards = !0, this.staggerTo(t, e, i, s, r, n, a, o)
                    }, g.staggerFromTo = function (t, e, i, s, r, n, a, o, l) {
                        return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, this.staggerTo(t, e, s, r, n, a, o, l)
                    }, g.call = function (t, e, s, r) {
                        return this.add(i.delayedCall(0, t, e, s), r)
                    }, g.set = function (t, e, s) {
                        return s = this._parseTimeOrLabel(s, 0, !0), null == e.immediateRender && (e.immediateRender = s === this._time && !this._paused), this.add(new i(t, 0, e), s)
                    }, s.exportRoot = function (t, e) {
                        t = t || {}, null == t.smoothChildTiming && (t.smoothChildTiming = !0);
                        var r, n, a = new s(t),
                            o = a._timeline;
                        for (null == e && (e = !0), o._remove(a, !0), a._startTime = 0, a._rawPrevTime = a._time = a._totalTime = o._time, r = o._first; r;) n = r._next, e && r instanceof i && r.target === r.vars.onComplete || a.add(r, r._startTime - r._delay), r = n;
                        return o.add(a, 0), a
                    }, g.add = function (r, n, a, o) {
                        var l, u, c, f, _, d;
                        if ("number" != typeof n && (n = this._parseTimeOrLabel(n, 0, !0, r)), !(r instanceof t)) {
                            if (r instanceof Array || r && r.push && h(r)) {
                                for (a = a || "normal", o = o || 0, l = n, u = r.length, c = 0; c < u; c++) h(f = r[c]) && (f = new s({
                                    tweens: f
                                })), this.add(f, l), "string" != typeof f && "function" != typeof f && ("sequence" === a ? l = f._startTime + f.totalDuration() / f._timeScale : "start" === a && (f._startTime -= f.delay())), l += o;
                                return this._uncache(!0)
                            }
                            if ("string" == typeof r) return this.addLabel(r, n);
                            if ("function" != typeof r) throw "Cannot add " + r + " into the timeline; it is not a tween, timeline, function, or string.";
                            r = i.delayedCall(0, r)
                        }
                        if (e.prototype.add.call(this, r, n), r._time && r.render((this.rawTime() - r._startTime) * r._timeScale, !1, !1), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
                            for (_ = this, d = _.rawTime() > r._startTime; _._timeline;) d && _._timeline.smoothChildTiming ? _.totalTime(_._totalTime, !0) : _._gc && _._enabled(!0, !1), _ = _._timeline;
                        return this
                    }, g.remove = function (e) {
                        if (e instanceof t) {
                            this._remove(e, !1);
                            var i = e._timeline = e.vars.useFrames ? t._rootFramesTimeline : t._rootTimeline;
                            return e._startTime = (e._paused ? e._pauseTime : i._time) - (e._reversed ? e.totalDuration() - e._totalTime : e._totalTime) / e._timeScale, this
                        }
                        if (e instanceof Array || e && e.push && h(e)) {
                            for (var s = e.length; --s > -1;) this.remove(e[s]);
                            return this
                        }
                        return "string" == typeof e ? this.removeLabel(e) : this.kill(null, e)
                    }, g._remove = function (t, i) {
                        e.prototype._remove.call(this, t, i);
                        var s = this._last;
                        return s ? this._time > this.duration() && (this._time = this._duration, this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this
                    }, g.append = function (t, e) {
                        return this.add(t, this._parseTimeOrLabel(null, e, !0, t))
                    }, g.insert = g.insertMultiple = function (t, e, i, s) {
                        return this.add(t, e || 0, i, s)
                    }, g.appendMultiple = function (t, e, i, s) {
                        return this.add(t, this._parseTimeOrLabel(null, e, !0, t), i, s)
                    }, g.addLabel = function (t, e) {
                        return this._labels[t] = this._parseTimeOrLabel(e), this
                    }, g.addPause = function (t, e, s, r) {
                        var n = i.delayedCall(0, p, s, r || this);
                        return n.vars.onComplete = n.vars.onReverseComplete = e, n.data = "isPause", this._hasPause = !0, this.add(n, t)
                    }, g.removeLabel = function (t) {
                        return delete this._labels[t], this
                    }, g.getLabelTime = function (t) {
                        return null != this._labels[t] ? this._labels[t] : -1
                    }, g._parseTimeOrLabel = function (e, i, s, r) {
                        var n, a;
                        if (r instanceof t && r.timeline === this) this.remove(r);
                        else if (r && (r instanceof Array || r.push && h(r)))
                            for (a = r.length; --a > -1;) r[a] instanceof t && r[a].timeline === this && this.remove(r[a]);
                        if (n = this.duration() > 99999999999 ? this.recent().endTime(!1) : this._duration, "string" == typeof i) return this._parseTimeOrLabel(i, s && "number" == typeof e && null == this._labels[i] ? e - n : 0, s);
                        if (i = i || 0, "string" != typeof e || !isNaN(e) && null == this._labels[e]) null == e && (e = n);
                        else {
                            if (a = e.indexOf("="), a === -1) return null == this._labels[e] ? s ? this._labels[e] = n + i : i : this._labels[e] + i;
                            i = parseInt(e.charAt(a - 1) + "1", 10) * Number(e.substr(a + 1)), e = a > 1 ? this._parseTimeOrLabel(e.substr(0, a - 1), 0, s) : n
                        }
                        return Number(e) + i
                    }, g.seek = function (t, e) {
                        return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), e !== !1)
                    }, g.stop = function () {
                        return this.paused(!0)
                    }, g.gotoAndPlay = function (t, e) {
                        return this.play(t, e)
                    }, g.gotoAndStop = function (t, e) {
                        return this.pause(t, e)
                    }, g.render = function (t, e, i) {
                        this._gc && this._enabled(!0, !1);
                        var s, n, a, o, l, h, f, _ = this._dirty ? this.totalDuration() : this._totalDuration,
                            d = this._time,
                            p = this._startTime,
                            m = this._timeScale,
                            g = this._paused;
                        if (t >= _ - 1e-7 && t >= 0) this._totalTime = this._time = _, this._reversed || this._hasPausedChild() || (n = !0, o = "onComplete", l = !!this._timeline.autoRemoveChildren, 0 === this._duration && (t <= 0 && t >= -1e-7 || this._rawPrevTime < 0 || this._rawPrevTime === r) && this._rawPrevTime !== t && this._first && (l = !0, this._rawPrevTime > r && (o = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : r, t = _ + 1e-4;
                        else if (t < 1e-7)
                            if (this._totalTime = this._time = 0, (0 !== d || 0 === this._duration && this._rawPrevTime !== r && (this._rawPrevTime > 0 || t < 0 && this._rawPrevTime >= 0)) && (o = "onReverseComplete", n = this._reversed), t < 0) this._active = !1, this._timeline.autoRemoveChildren && this._reversed ? (l = n = !0, o = "onReverseComplete") : this._rawPrevTime >= 0 && this._first && (l = !0), this._rawPrevTime = t;
                            else {
                                if (this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : r, 0 === t && n)
                                    for (s = this._first; s && 0 === s._startTime;) s._duration || (n = !1), s = s._next;
                                t = 0, this._initted || (l = !0)
                            } else {
                            if (this._hasPause && !this._forcingPlayhead && !e) {
                                if (t >= d)
                                    for (s = this._first; s && s._startTime <= t && !h;) s._duration || "isPause" !== s.data || s.ratio || 0 === s._startTime && 0 === this._rawPrevTime || (h = s), s = s._next;
                                else
                                    for (s = this._last; s && s._startTime >= t && !h;) s._duration || "isPause" === s.data && s._rawPrevTime > 0 && (h = s), s = s._prev;
                                h && (this._time = t = h._startTime, this._totalTime = t + this._cycle * (this._totalDuration + this._repeatDelay))
                            }
                            this._totalTime = this._time = this._rawPrevTime = t
                        }
                        if (this._time !== d && this._first || i || l || h) {
                            if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== d && t > 0 && (this._active = !0), 0 === d && this.vars.onStart && (0 === this._time && this._duration || e || this._callback("onStart")), f = this._time, f >= d)
                                for (s = this._first; s && (a = s._next, f === this._time && (!this._paused || g));)(s._active || s._startTime <= f && !s._paused && !s._gc) && (h === s && this.pause(), s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = a;
                            else
                                for (s = this._last; s && (a = s._prev, f === this._time && (!this._paused || g));) {
                                    if (s._active || s._startTime <= d && !s._paused && !s._gc) {
                                        if (h === s) {
                                            for (h = s._prev; h && h.endTime() > this._time;) h.render(h._reversed ? h.totalDuration() - (t - h._startTime) * h._timeScale : (t - h._startTime) * h._timeScale, e, i), h = h._prev;
                                            h = null, this.pause()
                                        }
                                        s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)
                                    }
                                    s = a
                                }
                            this._onUpdate && (e || (u.length && c(), this._callback("onUpdate"))), o && (this._gc || p !== this._startTime && m === this._timeScale || (0 === this._time || _ >= this.totalDuration()) && (n && (u.length && c(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[o] && this._callback(o)))
                        }
                    }, g._hasPausedChild = function () {
                        for (var t = this._first; t;) {
                            if (t._paused || t instanceof s && t._hasPausedChild()) return !0;
                            t = t._next
                        }
                        return !1
                    }, g.getChildren = function (t, e, s, r) {
                        r = r || -9999999999;
                        for (var n = [], a = this._first, o = 0; a;) a._startTime < r || (a instanceof i ? e !== !1 && (n[o++] = a) : (s !== !1 && (n[o++] = a), t !== !1 && (n = n.concat(a.getChildren(!0, e, s)), o = n.length))), a = a._next;
                        return n
                    }, g.getTweensOf = function (t, e) {
                        var s, r, n = this._gc,
                            a = [],
                            o = 0;
                        for (n && this._enabled(!0, !0), s = i.getTweensOf(t), r = s.length; --r > -1;)(s[r].timeline === this || e && this._contains(s[r])) && (a[o++] = s[r]);
                        return n && this._enabled(!1, !0), a
                    }, g.recent = function () {
                        return this._recent
                    }, g._contains = function (t) {
                        for (var e = t.timeline; e;) {
                            if (e === this) return !0;
                            e = e.timeline
                        }
                        return !1
                    }, g.shiftChildren = function (t, e, i) {
                        i = i || 0;
                        for (var s, r = this._first, n = this._labels; r;) r._startTime >= i && (r._startTime += t), r = r._next;
                        if (e)
                            for (s in n) n[s] >= i && (n[s] += t);
                        return this._uncache(!0)
                    }, g._kill = function (t, e) {
                        if (!t && !e) return this._enabled(!1, !1);
                        for (var i = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), s = i.length, r = !1; --s > -1;) i[s]._kill(t, e) && (r = !0);
                        return r
                    }, g.clear = function (t) {
                        var e = this.getChildren(!1, !0, !0),
                            i = e.length;
                        for (this._time = this._totalTime = 0; --i > -1;) e[i]._enabled(!1, !1);
                        return t !== !1 && (this._labels = {}), this._uncache(!0)
                    }, g.invalidate = function () {
                        for (var e = this._first; e;) e.invalidate(), e = e._next;
                        return t.prototype.invalidate.call(this)
                    }, g._enabled = function (t, i) {
                        if (t === this._gc)
                            for (var s = this._first; s;) s._enabled(t, !0), s = s._next;
                        return e.prototype._enabled.call(this, t, i)
                    }, g.totalTime = function (e, i, s) {
                        this._forcingPlayhead = !0;
                        var r = t.prototype.totalTime.apply(this, arguments);
                        return this._forcingPlayhead = !1, r
                    }, g.duration = function (t) {
                        return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t), this) : (this._dirty && this.totalDuration(), this._duration)
                    }, g.totalDuration = function (t) {
                        if (!arguments.length) {
                            if (this._dirty) {
                                for (var e, i, s = 0, r = this._last, n = 999999999999; r;) e = r._prev, r._dirty && r.totalDuration(), r._startTime > n && this._sortChildren && !r._paused ? this.add(r, r._startTime - r._delay) : n = r._startTime, r._startTime < 0 && !r._paused && (s -= r._startTime, this._timeline.smoothChildTiming && (this._startTime += r._startTime / this._timeScale), this.shiftChildren(-r._startTime, !1, -9999999999), n = 0), i = r._startTime + r._totalDuration / r._timeScale, i > s && (s = i), r = e;
                                this._duration = this._totalDuration = s, this._dirty = !1
                            }
                            return this._totalDuration
                        }
                        return t && this.totalDuration() ? this.timeScale(this._totalDuration / t) : this
                    }, g.paused = function (e) {
                        if (!e)
                            for (var i = this._first, s = this._time; i;) i._startTime === s && "isPause" === i.data && (i._rawPrevTime = 0), i = i._next;
                        return t.prototype.paused.apply(this, arguments)
                    }, g.usesFrames = function () {
                        for (var e = this._timeline; e._timeline;) e = e._timeline;
                        return e === t._rootFramesTimeline
                    }, g.rawTime = function (t) {
                        return t && (this._paused || this._repeat && this.time() > 0 && this.totalProgress() < 1) ? this._totalTime % (this._duration + this._repeatDelay) : this._paused ? this._totalTime : (this._timeline.rawTime(t) - this._startTime) * this._timeScale
                    }, s
                }, !0), n._gsDefine("TimelineMax", ["TimelineLite", "TweenLite", "easing.Ease"], function (t, e, i) {
                    var s = function (e) {
                        t.call(this, e), this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._cycle = 0, this._yoyo = this.vars.yoyo === !0, this._dirty = !0
                    },
                        r = 1e-10,
                        a = e._internals,
                        o = a.lazyTweens,
                        l = a.lazyRender,
                        h = n._gsDefine.globals,
                        u = new i(null, null, 1, 0),
                        c = s.prototype = new t;
                    return c.constructor = s, c.kill()._gc = !1, s.version = "1.20.2", c.invalidate = function () {
                        return this._yoyo = this.vars.yoyo === !0, this._repeat = this.vars.repeat || 0, this._repeatDelay = this.vars.repeatDelay || 0, this._uncache(!0), t.prototype.invalidate.call(this)
                    }, c.addCallback = function (t, i, s, r) {
                        return this.add(e.delayedCall(0, t, s, r), i)
                    }, c.removeCallback = function (t, e) {
                        if (t)
                            if (null == e) this._kill(null, t);
                            else
                                for (var i = this.getTweensOf(t, !1), s = i.length, r = this._parseTimeOrLabel(e); --s > -1;) i[s]._startTime === r && i[s]._enabled(!1, !1);
                        return this
                    }, c.removePause = function (e) {
                        return this.removeCallback(t._internals.pauseCallback, e)
                    }, c.tweenTo = function (t, i) {
                        i = i || {};
                        var s, r, n, a = {
                            ease: u,
                            useFrames: this.usesFrames(),
                            immediateRender: !1
                        },
                            o = i.repeat && h.TweenMax || e;
                        for (r in i) a[r] = i[r];
                        return a.time = this._parseTimeOrLabel(t), s = Math.abs(Number(a.time) - this._time) / this._timeScale || .001, n = new o(this, s, a), a.onStart = function () {
                            n.target.paused(!0), n.vars.time !== n.target.time() && s === n.duration() && n.duration(Math.abs(n.vars.time - n.target.time()) / n.target._timeScale), i.onStart && i.onStart.apply(i.onStartScope || i.callbackScope || n, i.onStartParams || [])
                        }, n
                    }, c.tweenFromTo = function (t, e, i) {
                        i = i || {}, t = this._parseTimeOrLabel(t), i.startAt = {
                            onComplete: this.seek,
                            onCompleteParams: [t],
                            callbackScope: this
                        }, i.immediateRender = i.immediateRender !== !1;
                        var s = this.tweenTo(e, i);
                        return s.duration(Math.abs(s.vars.time - t) / this._timeScale || .001)
                    }, c.render = function (t, e, i) {
                        this._gc && this._enabled(!0, !1);
                        var s, n, a, h, u, c, f, _, d = this._dirty ? this.totalDuration() : this._totalDuration,
                            p = this._duration,
                            m = this._time,
                            g = this._totalTime,
                            v = this._startTime,
                            y = this._timeScale,
                            x = this._rawPrevTime,
                            T = this._paused,
                            w = this._cycle;
                        if (t >= d - 1e-7 && t >= 0) this._locked || (this._totalTime = d, this._cycle = this._repeat), this._reversed || this._hasPausedChild() || (n = !0, h = "onComplete", u = !!this._timeline.autoRemoveChildren, 0 === this._duration && (t <= 0 && t >= -1e-7 || x < 0 || x === r) && x !== t && this._first && (u = !0, x > r && (h = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : r, this._yoyo && 0 !== (1 & this._cycle) ? this._time = t = 0 : (this._time = p, t = p + 1e-4);
                        else if (t < 1e-7)
                            if (this._locked || (this._totalTime = this._cycle = 0), this._time = 0, (0 !== m || 0 === p && x !== r && (x > 0 || t < 0 && x >= 0) && !this._locked) && (h = "onReverseComplete", n = this._reversed), t < 0) this._active = !1, this._timeline.autoRemoveChildren && this._reversed ? (u = n = !0, h = "onReverseComplete") : x >= 0 && this._first && (u = !0), this._rawPrevTime = t;
                            else {
                                if (this._rawPrevTime = p || !e || t || this._rawPrevTime === t ? t : r, 0 === t && n)
                                    for (s = this._first; s && 0 === s._startTime;) s._duration || (n = !1), s = s._next;
                                t = 0, this._initted || (u = !0)
                            } else if (0 === p && x < 0 && (u = !0), this._time = this._rawPrevTime = t, this._locked || (this._totalTime = t, 0 !== this._repeat && (c = p + this._repeatDelay, this._cycle = this._totalTime / c >> 0, 0 !== this._cycle && this._cycle === this._totalTime / c && g <= t && this._cycle-- , this._time = this._totalTime - this._cycle * c, this._yoyo && 0 !== (1 & this._cycle) && (this._time = p - this._time), this._time > p ? (this._time = p, t = p + 1e-4) : this._time < 0 ? this._time = t = 0 : t = this._time)), this._hasPause && !this._forcingPlayhead && !e) {
                                if (t = this._time, t >= m || this._repeat && w !== this._cycle)
                                    for (s = this._first; s && s._startTime <= t && !f;) s._duration || "isPause" !== s.data || s.ratio || 0 === s._startTime && 0 === this._rawPrevTime || (f = s), s = s._next;
                                else
                                    for (s = this._last; s && s._startTime >= t && !f;) s._duration || "isPause" === s.data && s._rawPrevTime > 0 && (f = s), s = s._prev;
                                f && f._startTime < p && (this._time = t = f._startTime, this._totalTime = t + this._cycle * (this._totalDuration + this._repeatDelay))
                            }
                        if (this._cycle !== w && !this._locked) {
                            var b = this._yoyo && 0 !== (1 & w),
                                P = b === (this._yoyo && 0 !== (1 & this._cycle)),
                                k = this._totalTime,
                                O = this._cycle,
                                C = this._rawPrevTime,
                                S = this._time;
                            if (this._totalTime = w * p, this._cycle < w ? b = !b : this._totalTime += p, this._time = m, this._rawPrevTime = 0 === p ? x - 1e-4 : x, this._cycle = w, this._locked = !0, m = b ? 0 : p, this.render(m, e, 0 === p), e || this._gc || this.vars.onRepeat && (this._cycle = O, this._locked = !1, this._callback("onRepeat")), m !== this._time) return;
                            if (P && (this._cycle = w, this._locked = !0, m = b ? p + 1e-4 : -1e-4, this.render(m, !0, !1)), this._locked = !1, this._paused && !T) return;
                            this._time = S, this._totalTime = k, this._cycle = O, this._rawPrevTime = C
                        }
                        if (!(this._time !== m && this._first || i || u || f)) return void (g !== this._totalTime && this._onUpdate && (e || this._callback("onUpdate")));
                        if (this._initted || (this._initted = !0), this._active || !this._paused && this._totalTime !== g && t > 0 && (this._active = !0), 0 === g && this.vars.onStart && (0 === this._totalTime && this._totalDuration || e || this._callback("onStart")), _ = this._time, _ >= m)
                            for (s = this._first; s && (a = s._next, _ === this._time && (!this._paused || T));)(s._active || s._startTime <= this._time && !s._paused && !s._gc) && (f === s && this.pause(), s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = a;
                        else
                            for (s = this._last; s && (a = s._prev, _ === this._time && (!this._paused || T));) {
                                if (s._active || s._startTime <= m && !s._paused && !s._gc) {
                                    if (f === s) {
                                        for (f = s._prev; f && f.endTime() > this._time;) f.render(f._reversed ? f.totalDuration() - (t - f._startTime) * f._timeScale : (t - f._startTime) * f._timeScale, e, i), f = f._prev;
                                        f = null, this.pause()
                                    }
                                    s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)
                                }
                                s = a
                            }
                        this._onUpdate && (e || (o.length && l(), this._callback("onUpdate"))), h && (this._locked || this._gc || v !== this._startTime && y === this._timeScale || (0 === this._time || d >= this.totalDuration()) && (n && (o.length && l(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[h] && this._callback(h)))
                    }, c.getActive = function (t, e, i) {
                        null == t && (t = !0), null == e && (e = !0), null == i && (i = !1);
                        var s, r, n = [],
                            a = this.getChildren(t, e, i),
                            o = 0,
                            l = a.length;
                        for (s = 0; s < l; s++) r = a[s], r.isActive() && (n[o++] = r);
                        return n
                    }, c.getLabelAfter = function (t) {
                        t || 0 !== t && (t = this._time);
                        var e, i = this.getLabelsArray(),
                            s = i.length;
                        for (e = 0; e < s; e++)
                            if (i[e].time > t) return i[e].name;
                        return null
                    }, c.getLabelBefore = function (t) {
                        null == t && (t = this._time);
                        for (var e = this.getLabelsArray(), i = e.length; --i > -1;)
                            if (e[i].time < t) return e[i].name;
                        return null
                    }, c.getLabelsArray = function () {
                        var t, e = [],
                            i = 0;
                        for (t in this._labels) e[i++] = {
                            time: this._labels[t],
                            name: t
                        };
                        return e.sort(function (t, e) {
                            return t.time - e.time
                        }), e
                    }, c.invalidate = function () {
                        return this._locked = !1, t.prototype.invalidate.call(this)
                    }, c.progress = function (t, e) {
                        return arguments.length ? this.totalTime(this.duration() * (this._yoyo && 0 !== (1 & this._cycle) ? 1 - t : t) + this._cycle * (this._duration + this._repeatDelay), e) : this._time / this.duration() || 0
                    }, c.totalProgress = function (t, e) {
                        return arguments.length ? this.totalTime(this.totalDuration() * t, e) : this._totalTime / this.totalDuration() || 0
                    }, c.totalDuration = function (e) {
                        return arguments.length ? this._repeat !== -1 && e ? this.timeScale(this.totalDuration() / e) : this : (this._dirty && (t.prototype.totalDuration.call(this), this._totalDuration = this._repeat === -1 ? 999999999999 : this._duration * (this._repeat + 1) + this._repeatDelay * this._repeat), this._totalDuration)
                    }, c.time = function (t, e) {
                        return arguments.length ? (this._dirty && this.totalDuration(), t > this._duration && (t = this._duration), this._yoyo && 0 !== (1 & this._cycle) ? t = this._duration - t + this._cycle * (this._duration + this._repeatDelay) : 0 !== this._repeat && (t += this._cycle * (this._duration + this._repeatDelay)), this.totalTime(t, e)) : this._time
                    }, c.repeat = function (t) {
                        return arguments.length ? (this._repeat = t, this._uncache(!0)) : this._repeat
                    }, c.repeatDelay = function (t) {
                        return arguments.length ? (this._repeatDelay = t, this._uncache(!0)) : this._repeatDelay
                    }, c.yoyo = function (t) {
                        return arguments.length ? (this._yoyo = t, this) : this._yoyo
                    }, c.currentLabel = function (t) {
                        return arguments.length ? this.seek(t, !0) : this.getLabelBefore(this._time + 1e-8)
                    }, s
                }, !0),
                    function () {
                        var t = 180 / Math.PI,
                            e = [],
                            i = [],
                            s = [],
                            r = {},
                            a = n._gsDefine.globals,
                            o = function (t, e, i, s) {
                                i === s && (i = s - (s - e) / 1e6), t === e && (e = t + (i - t) / 1e6), this.a = t, this.b = e, this.c = i, this.d = s, this.da = s - t, this.ca = i - t, this.ba = e - t
                            },
                            l = ",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",
                            h = function (t, e, i, s) {
                                var r = {
                                    a: t
                                },
                                    n = {},
                                    a = {},
                                    o = {
                                        c: s
                                    },
                                    l = (t + e) / 2,
                                    h = (e + i) / 2,
                                    u = (i + s) / 2,
                                    c = (l + h) / 2,
                                    f = (h + u) / 2,
                                    _ = (f - c) / 8;
                                return r.b = l + (t - l) / 4, n.b = c + _, r.c = n.a = (r.b + n.b) / 2, n.c = a.a = (c + f) / 2, a.b = f - _, o.b = u + (s - u) / 4, a.c = o.a = (a.b + o.b) / 2, [r, n, a, o]
                            },
                            u = function (t, r, n, a, o) {
                                var l, u, c, f, _, d, p, m, g, v, y, x, T, w = t.length - 1,
                                    b = 0,
                                    P = t[0].a;
                                for (l = 0; l < w; l++) _ = t[b], u = _.a, c = _.d, f = t[b + 1].d, o ? (y = e[l], x = i[l], T = (x + y) * r * .25 / (a ? .5 : s[l] || .5), d = c - (c - u) * (a ? .5 * r : 0 !== y ? T / y : 0), p = c + (f - c) * (a ? .5 * r : 0 !== x ? T / x : 0), m = c - (d + ((p - d) * (3 * y / (y + x) + .5) / 4 || 0))) : (d = c - (c - u) * r * .5, p = c + (f - c) * r * .5, m = c - (d + p) / 2), d += m, p += m, _.c = g = d, 0 !== l ? _.b = P : _.b = P = _.a + .6 * (_.c - _.a), _.da = c - u, _.ca = g - u, _.ba = P - u, n ? (v = h(u, P, g, c), t.splice(b, 1, v[0], v[1], v[2], v[3]), b += 4) : b++ , P = p;
                                _ = t[b], _.b = P, _.c = P + .4 * (_.d - P), _.da = _.d - _.a, _.ca = _.c - _.a, _.ba = P - _.a, n && (v = h(_.a, P, _.c, _.d), t.splice(b, 1, v[0], v[1], v[2], v[3]))
                            },
                            c = function (t, s, r, n) {
                                var a, l, h, u, c, f, _ = [];
                                if (n)
                                    for (t = [n].concat(t), l = t.length; --l > -1;) "string" == typeof (f = t[l][s]) && "=" === f.charAt(1) && (t[l][s] = n[s] + Number(f.charAt(0) + f.substr(2)));
                                if (a = t.length - 2, a < 0) return _[0] = new o(t[0][s], 0, 0, t[0][s]), _;
                                for (l = 0; l < a; l++) h = t[l][s], u = t[l + 1][s], _[l] = new o(h, 0, 0, u), r && (c = t[l + 2][s], e[l] = (e[l] || 0) + (u - h) * (u - h), i[l] = (i[l] || 0) + (c - u) * (c - u));
                                return _[l] = new o(t[l][s], 0, 0, t[l + 1][s]), _
                            },
                            f = function (t, n, a, o, h, f) {
                                var _, d, p, m, g, v, y, x, T = {},
                                    w = [],
                                    b = f || t[0];
                                h = "string" == typeof h ? "," + h + "," : l, null == n && (n = 1);
                                for (d in t[0]) w.push(d);
                                if (t.length > 1) {
                                    for (x = t[t.length - 1], y = !0, _ = w.length; --_ > -1;)
                                        if (d = w[_], Math.abs(b[d] - x[d]) > .05) {
                                            y = !1;
                                            break
                                        }
                                    y && (t = t.concat(), f && t.unshift(f), t.push(t[1]), f = t[t.length - 3])
                                }
                                for (e.length = i.length = s.length = 0, _ = w.length; --_ > -1;) d = w[_], r[d] = h.indexOf("," + d + ",") !== -1, T[d] = c(t, d, r[d], f);
                                for (_ = e.length; --_ > -1;) e[_] = Math.sqrt(e[_]), i[_] = Math.sqrt(i[_]);
                                if (!o) {
                                    for (_ = w.length; --_ > -1;)
                                        if (r[d])
                                            for (p = T[w[_]], v = p.length - 1, m = 0; m < v; m++) g = p[m + 1].da / i[m] + p[m].da / e[m] || 0, s[m] = (s[m] || 0) + g * g;
                                    for (_ = s.length; --_ > -1;) s[_] = Math.sqrt(s[_])
                                }
                                for (_ = w.length, m = a ? 4 : 1; --_ > -1;) d = w[_], p = T[d], u(p, n, a, o, r[d]), y && (p.splice(0, m), p.splice(p.length - m, m));
                                return T
                            },
                            _ = function (t, e, i) {
                                e = e || "soft";
                                var s, r, n, a, l, h, u, c, f, _, d, p = {},
                                    m = "cubic" === e ? 3 : 2,
                                    g = "soft" === e,
                                    v = [];
                                if (g && i && (t = [i].concat(t)), null == t || t.length < m + 1) throw "invalid Bezier data";
                                for (f in t[0]) v.push(f);
                                for (h = v.length; --h > -1;) {
                                    for (f = v[h], p[f] = l = [],
                                        _ = 0, c = t.length, u = 0; u < c; u++) s = null == i ? t[u][f] : "string" == typeof (d = t[u][f]) && "=" === d.charAt(1) ? i[f] + Number(d.charAt(0) + d.substr(2)) : Number(d), g && u > 1 && u < c - 1 && (l[_++] = (s + l[_ - 2]) / 2), l[_++] = s;
                                    for (c = _ - m + 1, _ = 0, u = 0; u < c; u += m) s = l[u], r = l[u + 1], n = l[u + 2], a = 2 === m ? 0 : l[u + 3], l[_++] = d = 3 === m ? new o(s, r, n, a) : new o(s, (2 * r + s) / 3, (2 * r + n) / 3, n);
                                    l.length = _
                                }
                                return p
                            },
                            d = function (t, e, i) {
                                for (var s, r, n, a, o, l, h, u, c, f, _, d = 1 / i, p = t.length; --p > -1;)
                                    for (f = t[p], n = f.a, a = f.d - n, o = f.c - n, l = f.b - n, s = r = 0, u = 1; u <= i; u++) h = d * u, c = 1 - h, s = r - (r = (h * h * a + 3 * c * (h * o + c * l)) * h), _ = p * i + u - 1, e[_] = (e[_] || 0) + s * s
                            },
                            p = function (t, e) {
                                e = e >> 0 || 6;
                                var i, s, r, n, a = [],
                                    o = [],
                                    l = 0,
                                    h = 0,
                                    u = e - 1,
                                    c = [],
                                    f = [];
                                for (i in t) d(t[i], a, e);
                                for (r = a.length, s = 0; s < r; s++) l += Math.sqrt(a[s]), n = s % e, f[n] = l, n === u && (h += l, n = s / e >> 0, c[n] = f, o[n] = h, l = 0, f = []);
                                return {
                                    length: h,
                                    lengths: o,
                                    segments: c
                                }
                            },
                            m = n._gsDefine.plugin({
                                propName: "bezier",
                                priority: -1,
                                version: "1.3.8",
                                API: 2,
                                global: !0,
                                init: function (t, e, i) {
                                    this._target = t, e instanceof Array && (e = {
                                        values: e
                                    }), this._func = {}, this._mod = {}, this._props = [], this._timeRes = null == e.timeResolution ? 6 : parseInt(e.timeResolution, 10);
                                    var s, r, n, a, o, l = e.values || [],
                                        h = {},
                                        u = l[0],
                                        c = e.autoRotate || i.vars.orientToBezier;
                                    this._autoRotate = c ? c instanceof Array ? c : [
                                        ["x", "y", "rotation", c === !0 ? 0 : Number(c) || 0]
                                    ] : null;
                                    for (s in u) this._props.push(s);
                                    for (n = this._props.length; --n > -1;) s = this._props[n], this._overwriteProps.push(s), r = this._func[s] = "function" == typeof t[s], h[s] = r ? t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)]() : parseFloat(t[s]), o || h[s] !== l[0][s] && (o = h);
                                    if (this._beziers = "cubic" !== e.type && "quadratic" !== e.type && "soft" !== e.type ? f(l, isNaN(e.curviness) ? 1 : e.curviness, !1, "thruBasic" === e.type, e.correlate, o) : _(l, e.type, h), this._segCount = this._beziers[s].length, this._timeRes) {
                                        var d = p(this._beziers, this._timeRes);
                                        this._length = d.length, this._lengths = d.lengths, this._segments = d.segments, this._l1 = this._li = this._s1 = this._si = 0, this._l2 = this._lengths[0], this._curSeg = this._segments[0], this._s2 = this._curSeg[0], this._prec = 1 / this._curSeg.length
                                    }
                                    if (c = this._autoRotate)
                                        for (this._initialRotations = [], c[0] instanceof Array || (this._autoRotate = c = [c]), n = c.length; --n > -1;) {
                                            for (a = 0; a < 3; a++) s = c[n][a], this._func[s] = "function" == typeof t[s] && t[s.indexOf("set") || "function" != typeof t["get" + s.substr(3)] ? s : "get" + s.substr(3)];
                                            s = c[n][2], this._initialRotations[n] = (this._func[s] ? this._func[s].call(this._target) : this._target[s]) || 0, this._overwriteProps.push(s)
                                        }
                                    return this._startRatio = i.vars.runBackwards ? 1 : 0, !0
                                },
                                set: function (e) {
                                    var i, s, r, n, a, o, l, h, u, c, f = this._segCount,
                                        _ = this._func,
                                        d = this._target,
                                        p = e !== this._startRatio;
                                    if (this._timeRes) {
                                        if (u = this._lengths, c = this._curSeg, e *= this._length, r = this._li, e > this._l2 && r < f - 1) {
                                            for (h = f - 1; r < h && (this._l2 = u[++r]) <= e;);
                                            this._l1 = u[r - 1], this._li = r, this._curSeg = c = this._segments[r], this._s2 = c[this._s1 = this._si = 0]
                                        } else if (e < this._l1 && r > 0) {
                                            for (; r > 0 && (this._l1 = u[--r]) >= e;);
                                            0 === r && e < this._l1 ? this._l1 = 0 : r++ , this._l2 = u[r], this._li = r, this._curSeg = c = this._segments[r], this._s1 = c[(this._si = c.length - 1) - 1] || 0, this._s2 = c[this._si]
                                        }
                                        if (i = r, e -= this._l1, r = this._si, e > this._s2 && r < c.length - 1) {
                                            for (h = c.length - 1; r < h && (this._s2 = c[++r]) <= e;);
                                            this._s1 = c[r - 1], this._si = r
                                        } else if (e < this._s1 && r > 0) {
                                            for (; r > 0 && (this._s1 = c[--r]) >= e;);
                                            0 === r && e < this._s1 ? this._s1 = 0 : r++ , this._s2 = c[r], this._si = r
                                        }
                                        o = (r + (e - this._s1) / (this._s2 - this._s1)) * this._prec || 0
                                    } else i = e < 0 ? 0 : e >= 1 ? f - 1 : f * e >> 0, o = (e - i * (1 / f)) * f;
                                    for (s = 1 - o, r = this._props.length; --r > -1;) n = this._props[r], a = this._beziers[n][i], l = (o * o * a.da + 3 * s * (o * a.ca + s * a.ba)) * o + a.a, this._mod[n] && (l = this._mod[n](l, d)), _[n] ? d[n](l) : d[n] = l;
                                    if (this._autoRotate) {
                                        var m, g, v, y, x, T, w, b = this._autoRotate;
                                        for (r = b.length; --r > -1;) n = b[r][2], T = b[r][3] || 0, w = b[r][4] === !0 ? 1 : t, a = this._beziers[b[r][0]], m = this._beziers[b[r][1]], a && m && (a = a[i], m = m[i], g = a.a + (a.b - a.a) * o, y = a.b + (a.c - a.b) * o, g += (y - g) * o, y += (a.c + (a.d - a.c) * o - y) * o, v = m.a + (m.b - m.a) * o, x = m.b + (m.c - m.b) * o, v += (x - v) * o, x += (m.c + (m.d - m.c) * o - x) * o, l = p ? Math.atan2(x - v, y - g) * w + T : this._initialRotations[r], this._mod[n] && (l = this._mod[n](l, d)), _[n] ? d[n](l) : d[n] = l)
                                    }
                                }
                            }),
                            g = m.prototype;
                        m.bezierThrough = f, m.cubicToQuadratic = h, m._autoCSS = !0, m.quadraticToCubic = function (t, e, i) {
                            return new o(t, (2 * e + t) / 3, (2 * e + i) / 3, i)
                        }, m._cssRegister = function () {
                            var t = a.CSSPlugin;
                            if (t) {
                                var e = t._internals,
                                    i = e._parseToProxy,
                                    s = e._setPluginRatio,
                                    r = e.CSSPropTween;
                                e._registerComplexSpecialProp("bezier", {
                                    parser: function (t, e, n, a, o, l) {
                                        e instanceof Array && (e = {
                                            values: e
                                        }), l = new m;
                                        var h, u, c, f = e.values,
                                            _ = f.length - 1,
                                            d = [],
                                            p = {};
                                        if (_ < 0) return o;
                                        for (h = 0; h <= _; h++) c = i(t, f[h], a, o, l, _ !== h), d[h] = c.end;
                                        for (u in e) p[u] = e[u];
                                        return p.values = d, o = new r(t, "bezier", 0, 0, c.pt, 2), o.data = c, o.plugin = l, o.setRatio = s, 0 === p.autoRotate && (p.autoRotate = !0), !p.autoRotate || p.autoRotate instanceof Array || (h = p.autoRotate === !0 ? 0 : Number(p.autoRotate), p.autoRotate = null != c.end.left ? [
                                            ["left", "top", "rotation", h, !1]
                                        ] : null != c.end.x && [
                                            ["x", "y", "rotation", h, !1]
                                        ]), p.autoRotate && (a._transform || a._enableTransforms(!1), c.autoRotate = a._target._gsTransform, c.proxy.rotation = c.autoRotate.rotation || 0, a._overwriteProps.push("rotation")), l._onInitTween(c.proxy, p, a._tween), o
                                    }
                                })
                            }
                        }, g._mod = function (t) {
                            for (var e, i = this._overwriteProps, s = i.length; --s > -1;) e = t[i[s]], e && "function" == typeof e && (this._mod[i[s]] = e)
                        }, g._kill = function (t) {
                            var e, i, s = this._props;
                            for (e in this._beziers)
                                if (e in t)
                                    for (delete this._beziers[e], delete this._func[e], i = s.length; --i > -1;) s[i] === e && s.splice(i, 1);
                            if (s = this._autoRotate)
                                for (i = s.length; --i > -1;) t[s[i][2]] && s.splice(i, 1);
                            return this._super._kill.call(this, t)
                        }
                    }(), n._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function (t, e) {
                        var i, s, r, a, o = function () {
                            t.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = o.prototype.setRatio
                        },
                            l = n._gsDefine.globals,
                            h = {},
                            u = o.prototype = new t("css");
                        u.constructor = o, o.version = "1.20.0", o.API = 2, o.defaultTransformPerspective = 0, o.defaultSkewType = "compensated", o.defaultSmoothOrigin = !0, u = "px", o.suffixMap = {
                            top: u,
                            right: u,
                            bottom: u,
                            left: u,
                            width: u,
                            height: u,
                            fontSize: u,
                            padding: u,
                            margin: u,
                            perspective: u,
                            lineHeight: ""
                        };
                        var c, f, _, d, p, m, g, v, y = /(?:\-|\.|\b)(\d|\.|e\-)+/g,
                            x = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,
                            T = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,
                            w = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,
                            b = /(?:\d|\-|\+|=|#|\.)*/g,
                            P = /opacity *= *([^)]*)/i,
                            k = /opacity:([^;]*)/i,
                            O = /alpha\(opacity *=.+?\)/i,
                            C = /^(rgb|hsl)/,
                            S = /([A-Z])/g,
                            A = /-([a-z])/gi,
                            R = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,
                            D = function (t, e) {
                                return e.toUpperCase()
                            },
                            M = /(?:Left|Right|Width)/i,
                            E = /(M11|M12|M21|M22)=[\d\-\.e]+/gi,
                            F = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,
                            z = /,(?=[^\)]*(?:\(|$))/gi,
                            j = /[\s,\(]/i,
                            L = Math.PI / 180,
                            I = 180 / Math.PI,
                            N = {},
                            X = {
                                style: {}
                            },
                            B = n.document || {
                                createElement: function () {
                                    return X
                                }
                            },
                            $ = function (t, e) {
                                return B.createElementNS ? B.createElementNS(e || "http://www.w3.org/1999/xhtml", t) : B.createElement(t)
                            },
                            Y = $("div"),
                            U = $("img"),
                            q = o._internals = {
                                _specialProps: h
                            },
                            V = (n.navigator || {}).userAgent || "",
                            W = function () {
                                var t = V.indexOf("Android"),
                                    e = $("a");
                                return _ = V.indexOf("Safari") !== -1 && V.indexOf("Chrome") === -1 && (t === -1 || parseFloat(V.substr(t + 8, 2)) > 3), p = _ && parseFloat(V.substr(V.indexOf("Version/") + 8, 2)) < 6, d = V.indexOf("Firefox") !== -1, (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(V) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(V)) && (m = parseFloat(RegExp.$1)), !!e && (e.style.cssText = "top:1px;opacity:.55;", /^0.55/.test(e.style.opacity))
                            }(),
                            H = function (t) {
                                return P.test("string" == typeof t ? t : (t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
                            },
                            Z = function (t) {
                                n.console && console.log(t)
                            },
                            G = "",
                            Q = "",
                            J = function (t, e) {
                                e = e || Y;
                                var i, s, r = e.style;
                                if (void 0 !== r[t]) return t;
                                for (t = t.charAt(0).toUpperCase() + t.substr(1), i = ["O", "Moz", "ms", "Ms", "Webkit"], s = 5; --s > -1 && void 0 === r[i[s] + t];);
                                return s >= 0 ? (Q = 3 === s ? "ms" : i[s], G = "-" + Q.toLowerCase() + "-", Q + t) : null
                            },
                            K = B.defaultView ? B.defaultView.getComputedStyle : function () { },
                            tt = o.getStyle = function (t, e, i, s, r) {
                                var n;
                                return W || "opacity" !== e ? (!s && t.style[e] ? n = t.style[e] : (i = i || K(t)) ? n = i[e] || i.getPropertyValue(e) || i.getPropertyValue(e.replace(S, "-$1").toLowerCase()) : t.currentStyle && (n = t.currentStyle[e]), null == r || n && "none" !== n && "auto" !== n && "auto auto" !== n ? n : r) : H(t)
                            },
                            et = q.convertToPixels = function (t, i, s, r, n) {
                                if ("px" === r || !r && "lineHeight" !== i) return s;
                                if ("auto" === r || !s) return 0;
                                var a, l, h, u = M.test(i),
                                    c = t,
                                    f = Y.style,
                                    _ = s < 0,
                                    d = 1 === s;
                                if (_ && (s = -s), d && (s *= 100), "lineHeight" !== i || r)
                                    if ("%" === r && i.indexOf("border") !== -1) a = s / 100 * (u ? t.clientWidth : t.clientHeight);
                                    else {
                                        if (f.cssText = "border:0 solid red;position:" + tt(t, "position") + ";line-height:0;", "%" !== r && c.appendChild && "v" !== r.charAt(0) && "rem" !== r) f[u ? "borderLeftWidth" : "borderTopWidth"] = s + r;
                                        else {
                                            if (c = t.parentNode || B.body, tt(c, "display").indexOf("flex") !== -1 && (f.position = "absolute"), l = c._gsCache, h = e.ticker.frame, l && u && l.time === h) return l.width * s / 100;
                                            f[u ? "width" : "height"] = s + r
                                        }
                                        c.appendChild(Y), a = parseFloat(Y[u ? "offsetWidth" : "offsetHeight"]), c.removeChild(Y), u && "%" === r && o.cacheWidths !== !1 && (l = c._gsCache = c._gsCache || {}, l.time = h, l.width = a / s * 100), 0 !== a || n || (a = et(t, i, s, r, !0))
                                    } else l = K(t).lineHeight, t.style.lineHeight = s, a = parseFloat(K(t).lineHeight), t.style.lineHeight = l;
                                return d && (a /= 100), _ ? -a : a
                            },
                            it = q.calculateOffset = function (t, e, i) {
                                if ("absolute" !== tt(t, "position", i)) return 0;
                                var s = "left" === e ? "Left" : "Top",
                                    r = tt(t, "margin" + s, i);
                                return t["offset" + s] - (et(t, e, parseFloat(r), r.replace(b, "")) || 0)
                            },
                            st = function (t, e) {
                                var i, s, r, n = {};
                                if (e = e || K(t, null))
                                    if (i = e.length)
                                        for (; --i > -1;) r = e[i], r.indexOf("-transform") !== -1 && Rt !== r || (n[r.replace(A, D)] = e.getPropertyValue(r));
                                    else
                                        for (i in e) i.indexOf("Transform") !== -1 && At !== i || (n[i] = e[i]);
                                else if (e = t.currentStyle || t.style)
                                    for (i in e) "string" == typeof i && void 0 === n[i] && (n[i.replace(A, D)] = e[i]);
                                return W || (n.opacity = H(t)), s = Ut(t, e, !1), n.rotation = s.rotation, n.skewX = s.skewX, n.scaleX = s.scaleX, n.scaleY = s.scaleY, n.x = s.x, n.y = s.y, Mt && (n.z = s.z, n.rotationX = s.rotationX, n.rotationY = s.rotationY, n.scaleZ = s.scaleZ), n.filters && delete n.filters, n
                            },
                            rt = function (t, e, i, s, r) {
                                var n, a, o, l = {},
                                    h = t.style;
                                for (a in i) "cssText" !== a && "length" !== a && isNaN(a) && (e[a] !== (n = i[a]) || r && r[a]) && a.indexOf("Origin") === -1 && ("number" != typeof n && "string" != typeof n || (l[a] = "auto" !== n || "left" !== a && "top" !== a ? "" !== n && "auto" !== n && "none" !== n || "string" != typeof e[a] || "" === e[a].replace(w, "") ? n : 0 : it(t, a), void 0 !== h[a] && (o = new yt(h, a, h[a], o))));
                                if (s)
                                    for (a in s) "className" !== a && (l[a] = s[a]);
                                return {
                                    difs: l,
                                    firstMPT: o
                                }
                            },
                            nt = {
                                width: ["Left", "Right"],
                                height: ["Top", "Bottom"]
                            },
                            at = ["marginLeft", "marginRight", "marginTop", "marginBottom"],
                            ot = function (t, e, i) {
                                if ("svg" === (t.nodeName + "").toLowerCase()) return (i || K(t))[e] || 0;
                                if (t.getCTM && Bt(t)) return t.getBBox()[e] || 0;
                                var s = parseFloat("width" === e ? t.offsetWidth : t.offsetHeight),
                                    r = nt[e],
                                    n = r.length;
                                for (i = i || K(t, null); --n > -1;) s -= parseFloat(tt(t, "padding" + r[n], i, !0)) || 0, s -= parseFloat(tt(t, "border" + r[n] + "Width", i, !0)) || 0;
                                return s
                            },
                            lt = function (t, e) {
                                if ("contain" === t || "auto" === t || "auto auto" === t) return t + " ";
                                null != t && "" !== t || (t = "0 0");
                                var i, s = t.split(" "),
                                    r = t.indexOf("left") !== -1 ? "0%" : t.indexOf("right") !== -1 ? "100%" : s[0],
                                    n = t.indexOf("top") !== -1 ? "0%" : t.indexOf("bottom") !== -1 ? "100%" : s[1];
                                if (s.length > 3 && !e) {
                                    for (s = t.split(", ").join(",").split(","), t = [], i = 0; i < s.length; i++) t.push(lt(s[i]));
                                    return t.join(",")
                                }
                                return null == n ? n = "center" === r ? "50%" : "0" : "center" === n && (n = "50%"), ("center" === r || isNaN(parseFloat(r)) && (r + "").indexOf("=") === -1) && (r = "50%"), t = r + " " + n + (s.length > 2 ? " " + s[2] : ""), e && (e.oxp = r.indexOf("%") !== -1, e.oyp = n.indexOf("%") !== -1, e.oxr = "=" === r.charAt(1), e.oyr = "=" === n.charAt(1), e.ox = parseFloat(r.replace(w, "")), e.oy = parseFloat(n.replace(w, "")), e.v = t), e || t
                            },
                            ht = function (t, e) {
                                return "function" == typeof t && (t = t(v, g)), "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) : parseFloat(t) - parseFloat(e) || 0
                            },
                            ut = function (t, e) {
                                return "function" == typeof t && (t = t(v, g)), null == t ? e : "string" == typeof t && "=" === t.charAt(1) ? parseInt(t.charAt(0) + "1", 10) * parseFloat(t.substr(2)) + e : parseFloat(t) || 0
                            },
                            ct = function (t, e, i, s) {
                                var r, n, a, o, l, h = 1e-6;
                                return "function" == typeof t && (t = t(v, g)), null == t ? o = e : "number" == typeof t ? o = t : (r = 360, n = t.split("_"), l = "=" === t.charAt(1), a = (l ? parseInt(t.charAt(0) + "1", 10) * parseFloat(n[0].substr(2)) : parseFloat(n[0])) * (t.indexOf("rad") === -1 ? 1 : I) - (l ? 0 : e), n.length && (s && (s[i] = e + a), t.indexOf("short") !== -1 && (a %= r, a !== a % (r / 2) && (a = a < 0 ? a + r : a - r)), t.indexOf("_cw") !== -1 && a < 0 ? a = (a + 9999999999 * r) % r - (a / r | 0) * r : t.indexOf("ccw") !== -1 && a > 0 && (a = (a - 9999999999 * r) % r - (a / r | 0) * r)), o = e + a), o < h && o > -h && (o = 0), o
                            },
                            ft = {
                                aqua: [0, 255, 255],
                                lime: [0, 255, 0],
                                silver: [192, 192, 192],
                                black: [0, 0, 0],
                                maroon: [128, 0, 0],
                                teal: [0, 128, 128],
                                blue: [0, 0, 255],
                                navy: [0, 0, 128],
                                white: [255, 255, 255],
                                fuchsia: [255, 0, 255],
                                olive: [128, 128, 0],
                                yellow: [255, 255, 0],
                                orange: [255, 165, 0],
                                gray: [128, 128, 128],
                                purple: [128, 0, 128],
                                green: [0, 128, 0],
                                red: [255, 0, 0],
                                pink: [255, 192, 203],
                                cyan: [0, 255, 255],
                                transparent: [255, 255, 255, 0]
                            },
                            _t = function (t, e, i) {
                                return t = t < 0 ? t + 1 : t > 1 ? t - 1 : t, 255 * (6 * t < 1 ? e + (i - e) * t * 6 : t < .5 ? i : 3 * t < 2 ? e + (i - e) * (2 / 3 - t) * 6 : e) + .5 | 0
                            },
                            dt = o.parseColor = function (t, e) {
                                var i, s, r, n, a, o, l, h, u, c, f;
                                if (t)
                                    if ("number" == typeof t) i = [t >> 16, t >> 8 & 255, 255 & t];
                                    else {
                                        if ("," === t.charAt(t.length - 1) && (t = t.substr(0, t.length - 1)), ft[t]) i = ft[t];
                                        else if ("#" === t.charAt(0)) 4 === t.length && (s = t.charAt(1), r = t.charAt(2), n = t.charAt(3), t = "#" + s + s + r + r + n + n), t = parseInt(t.substr(1), 16), i = [t >> 16, t >> 8 & 255, 255 & t];
                                        else if ("hsl" === t.substr(0, 3))
                                            if (i = f = t.match(y), e) {
                                                if (t.indexOf("=") !== -1) return t.match(x)
                                            } else a = Number(i[0]) % 360 / 360, o = Number(i[1]) / 100, l = Number(i[2]) / 100, r = l <= .5 ? l * (o + 1) : l + o - l * o, s = 2 * l - r, i.length > 3 && (i[3] = Number(t[3])), i[0] = _t(a + 1 / 3, s, r), i[1] = _t(a, s, r), i[2] = _t(a - 1 / 3, s, r);
                                        else i = t.match(y) || ft.transparent;
                                        i[0] = Number(i[0]), i[1] = Number(i[1]), i[2] = Number(i[2]), i.length > 3 && (i[3] = Number(i[3]))
                                    } else i = ft.black;
                                return e && !f && (s = i[0] / 255, r = i[1] / 255, n = i[2] / 255, h = Math.max(s, r, n), u = Math.min(s, r, n), l = (h + u) / 2, h === u ? a = o = 0 : (c = h - u, o = l > .5 ? c / (2 - h - u) : c / (h + u), a = h === s ? (r - n) / c + (r < n ? 6 : 0) : h === r ? (n - s) / c + 2 : (s - r) / c + 4, a *= 60), i[0] = a + .5 | 0, i[1] = 100 * o + .5 | 0, i[2] = 100 * l + .5 | 0), i
                            },
                            pt = function (t, e) {
                                var i, s, r, n = t.match(mt) || [],
                                    a = 0,
                                    o = "";
                                if (!n.length) return t;
                                for (i = 0; i < n.length; i++) s = n[i], r = t.substr(a, t.indexOf(s, a) - a), a += r.length + s.length, s = dt(s, e), 3 === s.length && s.push(1), o += r + (e ? "hsla(" + s[0] + "," + s[1] + "%," + s[2] + "%," + s[3] : "rgba(" + s.join(",")) + ")";
                                return o + t.substr(a)
                            },
                            mt = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";
                        for (u in ft) mt += "|" + u + "\\b";
                        mt = new RegExp(mt + ")", "gi"), o.colorStringFilter = function (t) {
                            var e, i = t[0] + " " + t[1];
                            mt.test(i) && (e = i.indexOf("hsl(") !== -1 || i.indexOf("hsla(") !== -1, t[0] = pt(t[0], e), t[1] = pt(t[1], e)), mt.lastIndex = 0
                        }, e.defaultStringFilter || (e.defaultStringFilter = o.colorStringFilter);
                        var gt = function (t, e, i, s) {
                            if (null == t) return function (t) {
                                return t
                            };
                            var r, n = e ? (t.match(mt) || [""])[0] : "",
                                a = t.split(n).join("").match(T) || [],
                                o = t.substr(0, t.indexOf(a[0])),
                                l = ")" === t.charAt(t.length - 1) ? ")" : "",
                                h = t.indexOf(" ") !== -1 ? " " : ",",
                                u = a.length,
                                c = u > 0 ? a[0].replace(y, "") : "";
                            return u ? r = e ? function (t) {
                                var e, f, _, d;
                                if ("number" == typeof t) t += c;
                                else if (s && z.test(t)) {
                                    for (d = t.replace(z, "|").split("|"), _ = 0; _ < d.length; _++) d[_] = r(d[_]);
                                    return d.join(",")
                                }
                                if (e = (t.match(mt) || [n])[0], f = t.split(e).join("").match(T) || [], _ = f.length, u > _--)
                                    for (; ++_ < u;) f[_] = i ? f[(_ - 1) / 2 | 0] : a[_];
                                return o + f.join(h) + h + e + l + (t.indexOf("inset") !== -1 ? " inset" : "")
                            } : function (t) {
                                var e, n, f;
                                if ("number" == typeof t) t += c;
                                else if (s && z.test(t)) {
                                    for (n = t.replace(z, "|").split("|"), f = 0; f < n.length; f++) n[f] = r(n[f]);
                                    return n.join(",")
                                }
                                if (e = t.match(T) || [], f = e.length, u > f--)
                                    for (; ++f < u;) e[f] = i ? e[(f - 1) / 2 | 0] : a[f];
                                return o + e.join(h) + l
                            } : function (t) {
                                return t
                            }
                        },
                            vt = function (t) {
                                return t = t.split(","),
                                    function (e, i, s, r, n, a, o) {
                                        var l, h = (i + "").split(" ");
                                        for (o = {}, l = 0; l < 4; l++) o[t[l]] = h[l] = h[l] || h[(l - 1) / 2 >> 0];
                                        return r.parse(e, o, n, a)
                                    }
                            },
                            yt = (q._setPluginRatio = function (t) {
                                this.plugin.setRatio(t);
                                for (var e, i, s, r, n, a = this.data, o = a.proxy, l = a.firstMPT, h = 1e-6; l;) e = o[l.v], l.r ? e = Math.round(e) : e < h && e > -h && (e = 0), l.t[l.p] = e, l = l._next;
                                if (a.autoRotate && (a.autoRotate.rotation = a.mod ? a.mod(o.rotation, this.t) : o.rotation), 1 === t || 0 === t)
                                    for (l = a.firstMPT, n = 1 === t ? "e" : "b"; l;) {
                                        if (i = l.t, i.type) {
                                            if (1 === i.type) {
                                                for (r = i.xs0 + i.s + i.xs1, s = 1; s < i.l; s++) r += i["xn" + s] + i["xs" + (s + 1)];
                                                i[n] = r
                                            }
                                        } else i[n] = i.s + i.xs0;
                                        l = l._next
                                    }
                            }, function (t, e, i, s, r) {
                                this.t = t, this.p = e, this.v = i, this.r = r, s && (s._prev = this, this._next = s)
                            }),
                            xt = (q._parseToProxy = function (t, e, i, s, r, n) {
                                var a, o, l, h, u, c = s,
                                    f = {},
                                    _ = {},
                                    d = i._transform,
                                    p = N;
                                for (i._transform = null, N = e, s = u = i.parse(t, e, s, r), N = p, n && (i._transform = d, c && (c._prev = null, c._prev && (c._prev._next = null))); s && s !== c;) {
                                    if (s.type <= 1 && (o = s.p, _[o] = s.s + s.c, f[o] = s.s, n || (h = new yt(s, "s", o, h, s.r), s.c = 0), 1 === s.type))
                                        for (a = s.l; --a > 0;) l = "xn" + a, o = s.p + "_" + l, _[o] = s.data[l], f[o] = s[l], n || (h = new yt(s, l, o, h, s.rxp[l]));
                                    s = s._next
                                }
                                return {
                                    proxy: f,
                                    end: _,
                                    firstMPT: h,
                                    pt: u
                                }
                            }, q.CSSPropTween = function (t, e, s, r, n, o, l, h, u, c, f) {
                                this.t = t, this.p = e, this.s = s, this.c = r, this.n = l || e, t instanceof xt || a.push(this.n), this.r = h, this.type = o || 0, u && (this.pr = u, i = !0), this.b = void 0 === c ? s : c, this.e = void 0 === f ? s + r : f, n && (this._next = n, n._prev = this)
                            }),
                            Tt = function (t, e, i, s, r, n) {
                                var a = new xt(t, e, i, s - i, r, (-1), n);
                                return a.b = i, a.e = a.xs0 = s, a
                            },
                            wt = o.parseComplex = function (t, e, i, s, r, n, a, l, h, u) {
                                i = i || n || "", "function" == typeof s && (s = s(v, g)), a = new xt(t, e, 0, 0, a, u ? 2 : 1, null, (!1), l, i, s), s += "", r && mt.test(s + i) && (s = [i, s], o.colorStringFilter(s), i = s[0], s = s[1]);
                                var f, _, d, p, m, T, w, b, P, k, O, C, S, A = i.split(", ").join(",").split(" "),
                                    R = s.split(", ").join(",").split(" "),
                                    D = A.length,
                                    M = c !== !1;
                                for (s.indexOf(",") === -1 && i.indexOf(",") === -1 || (A = A.join(" ").replace(z, ", ").split(" "), R = R.join(" ").replace(z, ", ").split(" "), D = A.length), D !== R.length && (A = (n || "").split(" "), D = A.length), a.plugin = h, a.setRatio = u, mt.lastIndex = 0, f = 0; f < D; f++)
                                    if (p = A[f], m = R[f], b = parseFloat(p), b || 0 === b) a.appendXtra("", b, ht(m, b), m.replace(x, ""), M && m.indexOf("px") !== -1, !0);
                                    else if (r && mt.test(p)) C = m.indexOf(")") + 1, C = ")" + (C ? m.substr(C) : ""), S = m.indexOf("hsl") !== -1 && W, k = m, p = dt(p, S), m = dt(m, S), P = p.length + m.length > 6, P && !W && 0 === m[3] ? (a["xs" + a.l] += a.l ? " transparent" : "transparent", a.e = a.e.split(R[f]).join("transparent")) : (W || (P = !1), S ? a.appendXtra(k.substr(0, k.indexOf("hsl")) + (P ? "hsla(" : "hsl("), p[0], ht(m[0], p[0]), ",", !1, !0).appendXtra("", p[1], ht(m[1], p[1]), "%,", !1).appendXtra("", p[2], ht(m[2], p[2]), P ? "%," : "%" + C, !1) : a.appendXtra(k.substr(0, k.indexOf("rgb")) + (P ? "rgba(" : "rgb("), p[0], m[0] - p[0], ",", !0, !0).appendXtra("", p[1], m[1] - p[1], ",", !0).appendXtra("", p[2], m[2] - p[2], P ? "," : C, !0), P && (p = p.length < 4 ? 1 : p[3], a.appendXtra("", p, (m.length < 4 ? 1 : m[3]) - p, C, !1))), mt.lastIndex = 0;
                                    else if (T = p.match(y)) {
                                        if (w = m.match(x), !w || w.length !== T.length) return a;
                                        for (d = 0, _ = 0; _ < T.length; _++) O = T[_], k = p.indexOf(O, d), a.appendXtra(p.substr(d, k - d), Number(O), ht(w[_], O), "", M && "px" === p.substr(k + O.length, 2), 0 === _), d = k + O.length;
                                        a["xs" + a.l] += p.substr(d)
                                    } else a["xs" + a.l] += a.l || a["xs" + a.l] ? " " + m : m;
                                if (s.indexOf("=") !== -1 && a.data) {
                                    for (C = a.xs0 + a.data.s, f = 1; f < a.l; f++) C += a["xs" + f] + a.data["xn" + f];
                                    a.e = C + a["xs" + f]
                                }
                                return a.l || (a.type = -1, a.xs0 = a.e), a.xfirst || a
                            },
                            bt = 9;
                        for (u = xt.prototype, u.l = u.pr = 0; --bt > 0;) u["xn" + bt] = 0, u["xs" + bt] = "";
                        u.xs0 = "", u._next = u._prev = u.xfirst = u.data = u.plugin = u.setRatio = u.rxp = null, u.appendXtra = function (t, e, i, s, r, n) {
                            var a = this,
                                o = a.l;
                            return a["xs" + o] += n && (o || a["xs" + o]) ? " " + t : t || "", i || 0 === o || a.plugin ? (a.l++ , a.type = a.setRatio ? 2 : 1, a["xs" + a.l] = s || "", o > 0 ? (a.data["xn" + o] = e + i, a.rxp["xn" + o] = r, a["xn" + o] = e, a.plugin || (a.xfirst = new xt(a, "xn" + o, e, i, a.xfirst || a, 0, a.n, r, a.pr), a.xfirst.xs0 = 0), a) : (a.data = {
                                s: e + i
                            }, a.rxp = {}, a.s = e, a.c = i, a.r = r, a)) : (a["xs" + o] += e + (s || ""), a)
                        };
                        var Pt = function (t, e) {
                            e = e || {}, this.p = e.prefix ? J(t) || t : t, h[t] = h[this.p] = this, this.format = e.formatter || gt(e.defaultValue, e.color, e.collapsible, e.multi), e.parser && (this.parse = e.parser), this.clrs = e.color, this.multi = e.multi, this.keyword = e.keyword, this.dflt = e.defaultValue, this.pr = e.priority || 0
                        },
                            kt = q._registerComplexSpecialProp = function (t, e, i) {
                                "object" != typeof e && (e = {
                                    parser: i
                                });
                                var s, r, n = t.split(","),
                                    a = e.defaultValue;
                                for (i = i || [a], s = 0; s < n.length; s++) e.prefix = 0 === s && e.prefix, e.defaultValue = i[s] || a, r = new Pt(n[s], e)
                            },
                            Ot = q._registerPluginProp = function (t) {
                                if (!h[t]) {
                                    var e = t.charAt(0).toUpperCase() + t.substr(1) + "Plugin";
                                    kt(t, {
                                        parser: function (t, i, s, r, n, a, o) {
                                            var u = l.com.greensock.plugins[e];
                                            return u ? (u._cssRegister(), h[s].parse(t, i, s, r, n, a, o)) : (Z("Error: " + e + " js file not loaded."), n)
                                        }
                                    })
                                }
                            };
                        u = Pt.prototype, u.parseComplex = function (t, e, i, s, r, n) {
                            var a, o, l, h, u, c, f = this.keyword;
                            if (this.multi && (z.test(i) || z.test(e) ? (o = e.replace(z, "|").split("|"), l = i.replace(z, "|").split("|")) : f && (o = [e], l = [i])), l) {
                                for (h = l.length > o.length ? l.length : o.length, a = 0; a < h; a++) e = o[a] = o[a] || this.dflt, i = l[a] = l[a] || this.dflt, f && (u = e.indexOf(f), c = i.indexOf(f), u !== c && (c === -1 ? o[a] = o[a].split(f).join("") : u === -1 && (o[a] += " " + f)));
                                e = o.join(", "), i = l.join(", ")
                            }
                            return wt(t, this.p, e, i, this.clrs, this.dflt, s, this.pr, r, n)
                        }, u.parse = function (t, e, i, s, n, a, o) {
                            return this.parseComplex(t.style, this.format(tt(t, this.p, r, !1, this.dflt)), this.format(e), n, a)
                        }, o.registerSpecialProp = function (t, e, i) {
                            kt(t, {
                                parser: function (t, s, r, n, a, o, l) {
                                    var h = new xt(t, r, 0, 0, a, 2, r, (!1), i);
                                    return h.plugin = o, h.setRatio = e(t, s, n._tween, r), h
                                },
                                priority: i
                            })
                        }, o.useSVGTransformAttr = !0;
                        var Ct, St = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),
                            At = J("transform"),
                            Rt = G + "transform",
                            Dt = J("transformOrigin"),
                            Mt = null !== J("perspective"),
                            Et = q.Transform = function () {
                                this.perspective = parseFloat(o.defaultTransformPerspective) || 0, this.force3D = !(o.defaultForce3D === !1 || !Mt) && (o.defaultForce3D || "auto")
                            },
                            Ft = n.SVGElement,
                            zt = function (t, e, i) {
                                var s, r = B.createElementNS("http://www.w3.org/2000/svg", t),
                                    n = /([a-z])([A-Z])/g;
                                for (s in i) r.setAttributeNS(null, s.replace(n, "$1-$2").toLowerCase(), i[s]);
                                return e.appendChild(r), r
                            },
                            jt = B.documentElement || {},
                            Lt = function () {
                                var t, e, i, s = m || /Android/i.test(V) && !n.chrome;
                                return B.createElementNS && !s && (t = zt("svg", jt), e = zt("rect", t, {
                                    width: 100,
                                    height: 50,
                                    x: 100
                                }), i = e.getBoundingClientRect().width, e.style[Dt] = "50% 50%", e.style[At] = "scaleX(0.5)", s = i === e.getBoundingClientRect().width && !(d && Mt), jt.removeChild(t)), s
                            }(),
                            It = function (t, e, i, s, r, n) {
                                var a, l, h, u, c, f, _, d, p, m, g, v, y, x, T = t._gsTransform,
                                    w = Yt(t, !0);
                                T && (y = T.xOrigin, x = T.yOrigin), (!s || (a = s.split(" ")).length < 2) && (_ = t.getBBox(), 0 === _.x && 0 === _.y && _.width + _.height === 0 && (_ = {
                                    x: parseFloat(t.hasAttribute("x") ? t.getAttribute("x") : t.hasAttribute("cx") ? t.getAttribute("cx") : 0) || 0,
                                    y: parseFloat(t.hasAttribute("y") ? t.getAttribute("y") : t.hasAttribute("cy") ? t.getAttribute("cy") : 0) || 0,
                                    width: 0,
                                    height: 0
                                }), e = lt(e).split(" "), a = [(e[0].indexOf("%") !== -1 ? parseFloat(e[0]) / 100 * _.width : parseFloat(e[0])) + _.x, (e[1].indexOf("%") !== -1 ? parseFloat(e[1]) / 100 * _.height : parseFloat(e[1])) + _.y]), i.xOrigin = u = parseFloat(a[0]), i.yOrigin = c = parseFloat(a[1]), s && w !== $t && (f = w[0], _ = w[1], d = w[2], p = w[3], m = w[4], g = w[5], v = f * p - _ * d, v && (l = u * (p / v) + c * (-d / v) + (d * g - p * m) / v, h = u * (-_ / v) + c * (f / v) - (f * g - _ * m) / v, u = i.xOrigin = a[0] = l, c = i.yOrigin = a[1] = h)), T && (n && (i.xOffset = T.xOffset, i.yOffset = T.yOffset, T = i), r || r !== !1 && o.defaultSmoothOrigin !== !1 ? (l = u - y, h = c - x, T.xOffset += l * w[0] + h * w[2] - l, T.yOffset += l * w[1] + h * w[3] - h) : T.xOffset = T.yOffset = 0), n || t.setAttribute("data-svg-origin", a.join(" "))
                            },
                            Nt = function (t) {
                                var e, i = $("svg", this.ownerSVGElement.getAttribute("xmlns") || "http://www.w3.org/2000/svg"),
                                    s = this.parentNode,
                                    r = this.nextSibling,
                                    n = this.style.cssText;
                                if (jt.appendChild(i), i.appendChild(this), this.style.display = "block", t) try {
                                    e = this.getBBox(), this._originalGetBBox = this.getBBox, this.getBBox = Nt
                                } catch (a) { } else this._originalGetBBox && (e = this._originalGetBBox());
                                return r ? s.insertBefore(this, r) : s.appendChild(this), jt.removeChild(i), this.style.cssText = n, e
                            },
                            Xt = function (t) {
                                try {
                                    return t.getBBox()
                                } catch (e) {
                                    return Nt.call(t, !0)
                                }
                            },
                            Bt = function (t) {
                                return !(!(Ft && t.getCTM && Xt(t)) || t.parentNode && !t.ownerSVGElement)
                            },
                            $t = [1, 0, 0, 1, 0, 0],
                            Yt = function (t, e) {
                                var i, s, r, n, a, o, l = t._gsTransform || new Et,
                                    h = 1e5,
                                    u = t.style;
                                if (At ? s = tt(t, Rt, null, !0) : t.currentStyle && (s = t.currentStyle.filter.match(E), s = s && 4 === s.length ? [s[0].substr(4), Number(s[2].substr(4)), Number(s[1].substr(4)), s[3].substr(4), l.x || 0, l.y || 0].join(",") : ""), i = !s || "none" === s || "matrix(1, 0, 0, 1, 0, 0)" === s, !At || !(o = "none" === K(t).display) && t.parentNode || (o && (n = u.display, u.display = "block"), t.parentNode || (a = 1, jt.appendChild(t)), s = tt(t, Rt, null, !0), i = !s || "none" === s || "matrix(1, 0, 0, 1, 0, 0)" === s, n ? u.display = n : o && Ht(u, "display"), a && jt.removeChild(t)), (l.svg || t.getCTM && Bt(t)) && (i && (u[At] + "").indexOf("matrix") !== -1 && (s = u[At], i = 0), r = t.getAttribute("transform"), i && r && (r.indexOf("matrix") !== -1 ? (s = r, i = 0) : r.indexOf("translate") !== -1 && (s = "matrix(1,0,0,1," + r.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",") + ")", i = 0))), i) return $t;
                                for (r = (s || "").match(y) || [], bt = r.length; --bt > -1;) n = Number(r[bt]), r[bt] = (a = n - (n |= 0)) ? (a * h + (a < 0 ? -.5 : .5) | 0) / h + n : n;
                                return e && r.length > 6 ? [r[0], r[1], r[4], r[5], r[12], r[13]] : r
                            },
                            Ut = q.getTransform = function (t, i, s, r) {
                                if (t._gsTransform && s && !r) return t._gsTransform;
                                var n, a, l, h, u, c, f = s ? t._gsTransform || new Et : new Et,
                                    _ = f.scaleX < 0,
                                    d = 2e-5,
                                    p = 1e5,
                                    m = Mt ? parseFloat(tt(t, Dt, i, !1, "0 0 0").split(" ")[2]) || f.zOrigin || 0 : 0,
                                    g = parseFloat(o.defaultTransformPerspective) || 0;
                                if (f.svg = !(!t.getCTM || !Bt(t)), f.svg && (It(t, tt(t, Dt, i, !1, "50% 50%") + "", f, t.getAttribute("data-svg-origin")), Ct = o.useSVGTransformAttr || Lt), n = Yt(t), n !== $t) {
                                    if (16 === n.length) {
                                        var v, y, x, T, w, b = n[0],
                                            P = n[1],
                                            k = n[2],
                                            O = n[3],
                                            C = n[4],
                                            S = n[5],
                                            A = n[6],
                                            R = n[7],
                                            D = n[8],
                                            M = n[9],
                                            E = n[10],
                                            F = n[12],
                                            z = n[13],
                                            j = n[14],
                                            L = n[11],
                                            N = Math.atan2(A, E);
                                        f.zOrigin && (j = -f.zOrigin, F = D * j - n[12], z = M * j - n[13], j = E * j + f.zOrigin - n[14]), f.rotationX = N * I, N && (T = Math.cos(-N), w = Math.sin(-N), v = C * T + D * w, y = S * T + M * w, x = A * T + E * w, D = C * -w + D * T, M = S * -w + M * T, E = A * -w + E * T, L = R * -w + L * T, C = v, S = y, A = x), N = Math.atan2(-k, E), f.rotationY = N * I, N && (T = Math.cos(-N), w = Math.sin(-N), v = b * T - D * w, y = P * T - M * w, x = k * T - E * w, M = P * w + M * T, E = k * w + E * T, L = O * w + L * T, b = v, P = y, k = x), N = Math.atan2(P, b), f.rotation = N * I, N && (T = Math.cos(N), w = Math.sin(N), v = b * T + P * w, y = C * T + S * w, x = D * T + M * w, P = P * T - b * w, S = S * T - C * w, M = M * T - D * w, b = v, C = y, D = x), f.rotationX && Math.abs(f.rotationX) + Math.abs(f.rotation) > 359.9 && (f.rotationX = f.rotation = 0, f.rotationY = 180 - f.rotationY), N = Math.atan2(C, S), f.scaleX = (Math.sqrt(b * b + P * P + k * k) * p + .5 | 0) / p, f.scaleY = (Math.sqrt(S * S + A * A) * p + .5 | 0) / p, f.scaleZ = (Math.sqrt(D * D + M * M + E * E) * p + .5 | 0) / p, b /= f.scaleX, C /= f.scaleY, P /= f.scaleX, S /= f.scaleY, Math.abs(N) > d ? (f.skewX = N * I, C = 0, "simple" !== f.skewType && (f.scaleY *= 1 / Math.cos(N))) : f.skewX = 0, f.perspective = L ? 1 / (L < 0 ? -L : L) : 0, f.x = F, f.y = z, f.z = j, f.svg && (f.x -= f.xOrigin - (f.xOrigin * b - f.yOrigin * C), f.y -= f.yOrigin - (f.yOrigin * P - f.xOrigin * S))
                                    } else if (!Mt || r || !n.length || f.x !== n[4] || f.y !== n[5] || !f.rotationX && !f.rotationY) {
                                        var X = n.length >= 6,
                                            B = X ? n[0] : 1,
                                            $ = n[1] || 0,
                                            Y = n[2] || 0,
                                            U = X ? n[3] : 1;
                                        f.x = n[4] || 0, f.y = n[5] || 0, l = Math.sqrt(B * B + $ * $), h = Math.sqrt(U * U + Y * Y), u = B || $ ? Math.atan2($, B) * I : f.rotation || 0, c = Y || U ? Math.atan2(Y, U) * I + u : f.skewX || 0, f.scaleX = l, f.scaleY = h, f.rotation = u, f.skewX = c, Mt && (f.rotationX = f.rotationY = f.z = 0, f.perspective = g, f.scaleZ = 1), f.svg && (f.x -= f.xOrigin - (f.xOrigin * B + f.yOrigin * Y), f.y -= f.yOrigin - (f.xOrigin * $ + f.yOrigin * U))
                                    }
                                    Math.abs(f.skewX) > 90 && Math.abs(f.skewX) < 270 && (_ ? (f.scaleX *= -1, f.skewX += f.rotation <= 0 ? 180 : -180, f.rotation += f.rotation <= 0 ? 180 : -180) : (f.scaleY *= -1, f.skewX += f.skewX <= 0 ? 180 : -180)), f.zOrigin = m;
                                    for (a in f) f[a] < d && f[a] > -d && (f[a] = 0)
                                }
                                return s && (t._gsTransform = f, f.svg && (Ct && t.style[At] ? e.delayedCall(.001, function () {
                                    Ht(t.style, At)
                                }) : !Ct && t.getAttribute("transform") && e.delayedCall(.001, function () {
                                    t.removeAttribute("transform")
                                }))), f
                            },
                            qt = function (t) {
                                var e, i, s = this.data,
                                    r = -s.rotation * L,
                                    n = r + s.skewX * L,
                                    a = 1e5,
                                    o = (Math.cos(r) * s.scaleX * a | 0) / a,
                                    l = (Math.sin(r) * s.scaleX * a | 0) / a,
                                    h = (Math.sin(n) * -s.scaleY * a | 0) / a,
                                    u = (Math.cos(n) * s.scaleY * a | 0) / a,
                                    c = this.t.style,
                                    f = this.t.currentStyle;
                                if (f) {
                                    i = l, l = -h, h = -i, e = f.filter, c.filter = "";
                                    var _, d, p = this.t.offsetWidth,
                                        g = this.t.offsetHeight,
                                        v = "absolute" !== f.position,
                                        y = "progid:DXImageTransform.Microsoft.Matrix(M11=" + o + ", M12=" + l + ", M21=" + h + ", M22=" + u,
                                        x = s.x + p * s.xPercent / 100,
                                        T = s.y + g * s.yPercent / 100;
                                    if (null != s.ox && (_ = (s.oxp ? p * s.ox * .01 : s.ox) - p / 2, d = (s.oyp ? g * s.oy * .01 : s.oy) - g / 2, x += _ - (_ * o + d * l), T += d - (_ * h + d * u)), v ? (_ = p / 2, d = g / 2, y += ", Dx=" + (_ - (_ * o + d * l) + x) + ", Dy=" + (d - (_ * h + d * u) + T) + ")") : y += ", sizingMethod='auto expand')", e.indexOf("DXImageTransform.Microsoft.Matrix(") !== -1 ? c.filter = e.replace(F, y) : c.filter = y + " " + e, 0 !== t && 1 !== t || 1 === o && 0 === l && 0 === h && 1 === u && (v && y.indexOf("Dx=0, Dy=0") === -1 || P.test(e) && 100 !== parseFloat(RegExp.$1) || e.indexOf(e.indexOf("Alpha")) === -1 && c.removeAttribute("filter")), !v) {
                                        var w, k, O, C = m < 8 ? 1 : -1;
                                        for (_ = s.ieOffsetX || 0, d = s.ieOffsetY || 0, s.ieOffsetX = Math.round((p - ((o < 0 ? -o : o) * p + (l < 0 ? -l : l) * g)) / 2 + x), s.ieOffsetY = Math.round((g - ((u < 0 ? -u : u) * g + (h < 0 ? -h : h) * p)) / 2 + T), bt = 0; bt < 4; bt++) k = at[bt], w = f[k], i = w.indexOf("px") !== -1 ? parseFloat(w) : et(this.t, k, parseFloat(w), w.replace(b, "")) || 0, O = i !== s[k] ? bt < 2 ? -s.ieOffsetX : -s.ieOffsetY : bt < 2 ? _ - s.ieOffsetX : d - s.ieOffsetY, c[k] = (s[k] = Math.round(i - O * (0 === bt || 2 === bt ? 1 : C))) + "px"
                                    }
                                }
                            },
                            Vt = q.set3DTransformRatio = q.setTransformRatio = function (t) {
                                var e, i, s, r, n, a, o, l, h, u, c, f, _, p, m, g, v, y, x, T, w, b, P, k = this.data,
                                    O = this.t.style,
                                    C = k.rotation,
                                    S = k.rotationX,
                                    A = k.rotationY,
                                    R = k.scaleX,
                                    D = k.scaleY,
                                    M = k.scaleZ,
                                    E = k.x,
                                    F = k.y,
                                    z = k.z,
                                    j = k.svg,
                                    I = k.perspective,
                                    N = k.force3D,
                                    X = k.skewY,
                                    B = k.skewX;
                                if (X && (B += X, C += X), ((1 === t || 0 === t) && "auto" === N && (this.tween._totalTime === this.tween._totalDuration || !this.tween._totalTime) || !N) && !z && !I && !A && !S && 1 === M || Ct && j || !Mt) return void (C || B || j ? (C *= L, b = B * L, P = 1e5, i = Math.cos(C) * R, n = Math.sin(C) * R, s = Math.sin(C - b) * -D, a = Math.cos(C - b) * D, b && "simple" === k.skewType && (e = Math.tan(b - X * L), e = Math.sqrt(1 + e * e), s *= e, a *= e, X && (e = Math.tan(X * L), e = Math.sqrt(1 + e * e), i *= e, n *= e)), j && (E += k.xOrigin - (k.xOrigin * i + k.yOrigin * s) + k.xOffset, F += k.yOrigin - (k.xOrigin * n + k.yOrigin * a) + k.yOffset, Ct && (k.xPercent || k.yPercent) && (m = this.t.getBBox(), E += .01 * k.xPercent * m.width, F += .01 * k.yPercent * m.height), m = 1e-6, E < m && E > -m && (E = 0), F < m && F > -m && (F = 0)), x = (i * P | 0) / P + "," + (n * P | 0) / P + "," + (s * P | 0) / P + "," + (a * P | 0) / P + "," + E + "," + F + ")", j && Ct ? this.t.setAttribute("transform", "matrix(" + x) : O[At] = (k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) matrix(" : "matrix(") + x) : O[At] = (k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) matrix(" : "matrix(") + R + ",0,0," + D + "," + E + "," + F + ")");
                                if (d && (m = 1e-4, R < m && R > -m && (R = M = 2e-5), D < m && D > -m && (D = M = 2e-5), !I || k.z || k.rotationX || k.rotationY || (I = 0)), C || B) C *= L, g = i = Math.cos(C), v = n = Math.sin(C), B && (C -= B * L, g = Math.cos(C), v = Math.sin(C), "simple" === k.skewType && (e = Math.tan((B - X) * L), e = Math.sqrt(1 + e * e), g *= e, v *= e, k.skewY && (e = Math.tan(X * L), e = Math.sqrt(1 + e * e), i *= e, n *= e))), s = -v, a = g;
                                else {
                                    if (!(A || S || 1 !== M || I || j)) return void (O[At] = (k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) translate3d(" : "translate3d(") + E + "px," + F + "px," + z + "px)" + (1 !== R || 1 !== D ? " scale(" + R + "," + D + ")" : ""));
                                    i = a = 1, s = n = 0
                                }
                                u = 1, r = o = l = h = c = f = 0, _ = I ? -1 / I : 0, p = k.zOrigin, m = 1e-6, T = ",", w = "0", C = A * L, C && (g = Math.cos(C), v = Math.sin(C), l = -v, c = _ * -v, r = i * v, o = n * v, u = g, _ *= g, i *= g, n *= g), C = S * L, C && (g = Math.cos(C), v = Math.sin(C), e = s * g + r * v, y = a * g + o * v, h = u * v, f = _ * v, r = s * -v + r * g, o = a * -v + o * g, u *= g, _ *= g, s = e, a = y), 1 !== M && (r *= M, o *= M, u *= M, _ *= M), 1 !== D && (s *= D, a *= D, h *= D, f *= D), 1 !== R && (i *= R, n *= R, l *= R, c *= R), (p || j) && (p && (E += r * -p, F += o * -p, z += u * -p + p), j && (E += k.xOrigin - (k.xOrigin * i + k.yOrigin * s) + k.xOffset, F += k.yOrigin - (k.xOrigin * n + k.yOrigin * a) + k.yOffset), E < m && E > -m && (E = w), F < m && F > -m && (F = w), z < m && z > -m && (z = 0)), x = k.xPercent || k.yPercent ? "translate(" + k.xPercent + "%," + k.yPercent + "%) matrix3d(" : "matrix3d(", x += (i < m && i > -m ? w : i) + T + (n < m && n > -m ? w : n) + T + (l < m && l > -m ? w : l), x += T + (c < m && c > -m ? w : c) + T + (s < m && s > -m ? w : s) + T + (a < m && a > -m ? w : a), S || A || 1 !== M ? (x += T + (h < m && h > -m ? w : h) + T + (f < m && f > -m ? w : f) + T + (r < m && r > -m ? w : r), x += T + (o < m && o > -m ? w : o) + T + (u < m && u > -m ? w : u) + T + (_ < m && _ > -m ? w : _) + T) : x += ",0,0,0,0,1,0,", x += E + T + F + T + z + T + (I ? 1 + -z / I : 1) + ")", O[At] = x
                            };
                        u = Et.prototype, u.x = u.y = u.z = u.skewX = u.skewY = u.rotation = u.rotationX = u.rotationY = u.zOrigin = u.xPercent = u.yPercent = u.xOffset = u.yOffset = 0, u.scaleX = u.scaleY = u.scaleZ = 1, kt("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", {
                            parser: function (t, e, i, s, n, a, l) {
                                if (s._lastParsedTransform === l) return n;
                                s._lastParsedTransform = l;
                                var h, u = l.scale && "function" == typeof l.scale ? l.scale : 0;
                                "function" == typeof l[i] && (h = l[i], l[i] = e), u && (l.scale = u(v, t));
                                var c, f, _, d, p, m, y, x, T, w = t._gsTransform,
                                    b = t.style,
                                    P = 1e-6,
                                    k = St.length,
                                    O = l,
                                    C = {},
                                    S = "transformOrigin",
                                    A = Ut(t, r, !0, O.parseTransform),
                                    R = O.transform && ("function" == typeof O.transform ? O.transform(v, g) : O.transform);
                                if (A.skewType = O.skewType || A.skewType || o.defaultSkewType, s._transform = A, R && "string" == typeof R && At) f = Y.style, f[At] = R, f.display = "block", f.position = "absolute", B.body.appendChild(Y), c = Ut(Y, null, !1), "simple" === A.skewType && (c.scaleY *= Math.cos(c.skewX * L)),
                                    A.svg && (m = A.xOrigin, y = A.yOrigin, c.x -= A.xOffset, c.y -= A.yOffset, (O.transformOrigin || O.svgOrigin) && (R = {}, It(t, lt(O.transformOrigin), R, O.svgOrigin, O.smoothOrigin, !0), m = R.xOrigin, y = R.yOrigin, c.x -= R.xOffset - A.xOffset, c.y -= R.yOffset - A.yOffset), (m || y) && (x = Yt(Y, !0), c.x -= m - (m * x[0] + y * x[2]), c.y -= y - (m * x[1] + y * x[3]))), B.body.removeChild(Y), c.perspective || (c.perspective = A.perspective), null != O.xPercent && (c.xPercent = ut(O.xPercent, A.xPercent)), null != O.yPercent && (c.yPercent = ut(O.yPercent, A.yPercent));
                                else if ("object" == typeof O) {
                                    if (c = {
                                        scaleX: ut(null != O.scaleX ? O.scaleX : O.scale, A.scaleX),
                                        scaleY: ut(null != O.scaleY ? O.scaleY : O.scale, A.scaleY),
                                        scaleZ: ut(O.scaleZ, A.scaleZ),
                                        x: ut(O.x, A.x),
                                        y: ut(O.y, A.y),
                                        z: ut(O.z, A.z),
                                        xPercent: ut(O.xPercent, A.xPercent),
                                        yPercent: ut(O.yPercent, A.yPercent),
                                        perspective: ut(O.transformPerspective, A.perspective)
                                    }, p = O.directionalRotation, null != p)
                                        if ("object" == typeof p)
                                            for (f in p) O[f] = p[f];
                                        else O.rotation = p;
                                    "string" == typeof O.x && O.x.indexOf("%") !== -1 && (c.x = 0, c.xPercent = ut(O.x, A.xPercent)), "string" == typeof O.y && O.y.indexOf("%") !== -1 && (c.y = 0, c.yPercent = ut(O.y, A.yPercent)), c.rotation = ct("rotation" in O ? O.rotation : "shortRotation" in O ? O.shortRotation + "_short" : "rotationZ" in O ? O.rotationZ : A.rotation, A.rotation, "rotation", C), Mt && (c.rotationX = ct("rotationX" in O ? O.rotationX : "shortRotationX" in O ? O.shortRotationX + "_short" : A.rotationX || 0, A.rotationX, "rotationX", C), c.rotationY = ct("rotationY" in O ? O.rotationY : "shortRotationY" in O ? O.shortRotationY + "_short" : A.rotationY || 0, A.rotationY, "rotationY", C)), c.skewX = ct(O.skewX, A.skewX), c.skewY = ct(O.skewY, A.skewY)
                                }
                                for (Mt && null != O.force3D && (A.force3D = O.force3D, d = !0), _ = A.force3D || A.z || A.rotationX || A.rotationY || c.z || c.rotationX || c.rotationY || c.perspective, _ || null == O.scale || (c.scaleZ = 1); --k > -1;) T = St[k], R = c[T] - A[T], (R > P || R < -P || null != O[T] || null != N[T]) && (d = !0, n = new xt(A, T, A[T], R, n), T in C && (n.e = C[T]), n.xs0 = 0, n.plugin = a, s._overwriteProps.push(n.n));
                                return R = O.transformOrigin, A.svg && (R || O.svgOrigin) && (m = A.xOffset, y = A.yOffset, It(t, lt(R), c, O.svgOrigin, O.smoothOrigin), n = Tt(A, "xOrigin", (w ? A : c).xOrigin, c.xOrigin, n, S), n = Tt(A, "yOrigin", (w ? A : c).yOrigin, c.yOrigin, n, S), m === A.xOffset && y === A.yOffset || (n = Tt(A, "xOffset", w ? m : A.xOffset, A.xOffset, n, S), n = Tt(A, "yOffset", w ? y : A.yOffset, A.yOffset, n, S)), R = "0px 0px"), (R || Mt && _ && A.zOrigin) && (At ? (d = !0, T = Dt, R = (R || tt(t, T, r, !1, "50% 50%")) + "", n = new xt(b, T, 0, 0, n, (-1), S), n.b = b[T], n.plugin = a, Mt ? (f = A.zOrigin, R = R.split(" "), A.zOrigin = (R.length > 2 && (0 === f || "0px" !== R[2]) ? parseFloat(R[2]) : f) || 0, n.xs0 = n.e = R[0] + " " + (R[1] || "50%") + " 0px", n = new xt(A, "zOrigin", 0, 0, n, (-1), n.n), n.b = f, n.xs0 = n.e = A.zOrigin) : n.xs0 = n.e = R) : lt(R + "", A)), d && (s._transformType = A.svg && Ct || !_ && 3 !== this._transformType ? 2 : 3), h && (l[i] = h), u && (l.scale = u), n
                            },
                            prefix: !0
                        }), kt("boxShadow", {
                            defaultValue: "0px 0px 0px 0px #999",
                            prefix: !0,
                            color: !0,
                            multi: !0,
                            keyword: "inset"
                        }), kt("borderRadius", {
                            defaultValue: "0px",
                            parser: function (t, e, i, n, a, o) {
                                e = this.format(e);
                                var l, h, u, c, f, _, d, p, m, g, v, y, x, T, w, b, P = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"],
                                    k = t.style;
                                for (m = parseFloat(t.offsetWidth), g = parseFloat(t.offsetHeight), l = e.split(" "), h = 0; h < P.length; h++) this.p.indexOf("border") && (P[h] = J(P[h])), f = c = tt(t, P[h], r, !1, "0px"), f.indexOf(" ") !== -1 && (c = f.split(" "), f = c[0], c = c[1]), _ = u = l[h], d = parseFloat(f), y = f.substr((d + "").length), x = "=" === _.charAt(1), x ? (p = parseInt(_.charAt(0) + "1", 10), _ = _.substr(2), p *= parseFloat(_), v = _.substr((p + "").length - (p < 0 ? 1 : 0)) || "") : (p = parseFloat(_), v = _.substr((p + "").length)), "" === v && (v = s[i] || y), v !== y && (T = et(t, "borderLeft", d, y), w = et(t, "borderTop", d, y), "%" === v ? (f = T / m * 100 + "%", c = w / g * 100 + "%") : "em" === v ? (b = et(t, "borderLeft", 1, "em"), f = T / b + "em", c = w / b + "em") : (f = T + "px", c = w + "px"), x && (_ = parseFloat(f) + p + v, u = parseFloat(c) + p + v)), a = wt(k, P[h], f + " " + c, _ + " " + u, !1, "0px", a);
                                return a
                            },
                            prefix: !0,
                            formatter: gt("0px 0px 0px 0px", !1, !0)
                        }), kt("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", {
                            defaultValue: "0px",
                            parser: function (t, e, i, s, n, a) {
                                return wt(t.style, i, this.format(tt(t, i, r, !1, "0px 0px")), this.format(e), !1, "0px", n)
                            },
                            prefix: !0,
                            formatter: gt("0px 0px", !1, !0)
                        }), kt("backgroundPosition", {
                            defaultValue: "0 0",
                            parser: function (t, e, i, s, n, a) {
                                var o, l, h, u, c, f, _ = "background-position",
                                    d = r || K(t, null),
                                    p = this.format((d ? m ? d.getPropertyValue(_ + "-x") + " " + d.getPropertyValue(_ + "-y") : d.getPropertyValue(_) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"),
                                    g = this.format(e);
                                if (p.indexOf("%") !== -1 != (g.indexOf("%") !== -1) && g.split(",").length < 2 && (f = tt(t, "backgroundImage").replace(R, ""), f && "none" !== f)) {
                                    for (o = p.split(" "), l = g.split(" "), U.setAttribute("src", f), h = 2; --h > -1;) p = o[h], u = p.indexOf("%") !== -1, u !== (l[h].indexOf("%") !== -1) && (c = 0 === h ? t.offsetWidth - U.width : t.offsetHeight - U.height, o[h] = u ? parseFloat(p) / 100 * c + "px" : parseFloat(p) / c * 100 + "%");
                                    p = o.join(" ")
                                }
                                return this.parseComplex(t.style, p, g, n, a)
                            },
                            formatter: lt
                        }), kt("backgroundSize", {
                            defaultValue: "0 0",
                            formatter: function (t) {
                                return t += "", lt(t.indexOf(" ") === -1 ? t + " " + t : t)
                            }
                        }), kt("perspective", {
                            defaultValue: "0px",
                            prefix: !0
                        }), kt("perspectiveOrigin", {
                            defaultValue: "50% 50%",
                            prefix: !0
                        }), kt("transformStyle", {
                            prefix: !0
                        }), kt("backfaceVisibility", {
                            prefix: !0
                        }), kt("userSelect", {
                            prefix: !0
                        }), kt("margin", {
                            parser: vt("marginTop,marginRight,marginBottom,marginLeft")
                        }), kt("padding", {
                            parser: vt("paddingTop,paddingRight,paddingBottom,paddingLeft")
                        }), kt("clip", {
                            defaultValue: "rect(0px,0px,0px,0px)",
                            parser: function (t, e, i, s, n, a) {
                                var o, l, h;
                                return m < 9 ? (l = t.currentStyle, h = m < 8 ? " " : ",", o = "rect(" + l.clipTop + h + l.clipRight + h + l.clipBottom + h + l.clipLeft + ")", e = this.format(e).split(",").join(h)) : (o = this.format(tt(t, this.p, r, !1, this.dflt)), e = this.format(e)), this.parseComplex(t.style, o, e, n, a)
                            }
                        }), kt("textShadow", {
                            defaultValue: "0px 0px 0px #999",
                            color: !0,
                            multi: !0
                        }), kt("autoRound,strictUnits", {
                            parser: function (t, e, i, s, r) {
                                return r
                            }
                        }), kt("border", {
                            defaultValue: "0px solid #000",
                            parser: function (t, e, i, s, n, a) {
                                var o = tt(t, "borderTopWidth", r, !1, "0px"),
                                    l = this.format(e).split(" "),
                                    h = l[0].replace(b, "");
                                return "px" !== h && (o = parseFloat(o) / et(t, "borderTopWidth", 1, h) + h), this.parseComplex(t.style, this.format(o + " " + tt(t, "borderTopStyle", r, !1, "solid") + " " + tt(t, "borderTopColor", r, !1, "#000")), l.join(" "), n, a)
                            },
                            color: !0,
                            formatter: function (t) {
                                var e = t.split(" ");
                                return e[0] + " " + (e[1] || "solid") + " " + (t.match(mt) || ["#000"])[0]
                            }
                        }), kt("borderWidth", {
                            parser: vt("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
                        }), kt("float,cssFloat,styleFloat", {
                            parser: function (t, e, i, s, r, n) {
                                var a = t.style,
                                    o = "cssFloat" in a ? "cssFloat" : "styleFloat";
                                return new xt(a, o, 0, 0, r, (-1), i, (!1), 0, a[o], e)
                            }
                        });
                        var Wt = function (t) {
                            var e, i = this.t,
                                s = i.filter || tt(this.data, "filter") || "",
                                r = this.s + this.c * t | 0;
                            100 === r && (s.indexOf("atrix(") === -1 && s.indexOf("radient(") === -1 && s.indexOf("oader(") === -1 ? (i.removeAttribute("filter"), e = !tt(this.data, "filter")) : (i.filter = s.replace(O, ""), e = !0)), e || (this.xn1 && (i.filter = s = s || "alpha(opacity=" + r + ")"), s.indexOf("pacity") === -1 ? 0 === r && this.xn1 || (i.filter = s + " alpha(opacity=" + r + ")") : i.filter = s.replace(P, "opacity=" + r))
                        };
                        kt("opacity,alpha,autoAlpha", {
                            defaultValue: "1",
                            parser: function (t, e, i, s, n, a) {
                                var o = parseFloat(tt(t, "opacity", r, !1, "1")),
                                    l = t.style,
                                    h = "autoAlpha" === i;
                                return "string" == typeof e && "=" === e.charAt(1) && (e = ("-" === e.charAt(0) ? -1 : 1) * parseFloat(e.substr(2)) + o), h && 1 === o && "hidden" === tt(t, "visibility", r) && 0 !== e && (o = 0), W ? n = new xt(l, "opacity", o, e - o, n) : (n = new xt(l, "opacity", 100 * o, 100 * (e - o), n), n.xn1 = h ? 1 : 0, l.zoom = 1, n.type = 2, n.b = "alpha(opacity=" + n.s + ")", n.e = "alpha(opacity=" + (n.s + n.c) + ")", n.data = t, n.plugin = a, n.setRatio = Wt), h && (n = new xt(l, "visibility", 0, 0, n, (-1), null, (!1), 0, 0 !== o ? "inherit" : "hidden", 0 === e ? "hidden" : "inherit"), n.xs0 = "inherit", s._overwriteProps.push(n.n), s._overwriteProps.push(i)), n
                            }
                        });
                        var Ht = function (t, e) {
                            e && (t.removeProperty ? ("ms" !== e.substr(0, 2) && "webkit" !== e.substr(0, 6) || (e = "-" + e), t.removeProperty(e.replace(S, "-$1").toLowerCase())) : t.removeAttribute(e))
                        },
                            Zt = function (t) {
                                if (this.t._gsClassPT = this, 1 === t || 0 === t) {
                                    this.t.setAttribute("class", 0 === t ? this.b : this.e);
                                    for (var e = this.data, i = this.t.style; e;) e.v ? i[e.p] = e.v : Ht(i, e.p), e = e._next;
                                    1 === t && this.t._gsClassPT === this && (this.t._gsClassPT = null)
                                } else this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
                            };
                        kt("className", {
                            parser: function (t, e, s, n, a, o, l) {
                                var h, u, c, f, _, d = t.getAttribute("class") || "",
                                    p = t.style.cssText;
                                if (a = n._classNamePT = new xt(t, s, 0, 0, a, 2), a.setRatio = Zt, a.pr = -11, i = !0, a.b = d, u = st(t, r), c = t._gsClassPT) {
                                    for (f = {}, _ = c.data; _;) f[_.p] = 1, _ = _._next;
                                    c.setRatio(1)
                                }
                                return t._gsClassPT = a, a.e = "=" !== e.charAt(1) ? e : d.replace(new RegExp("(?:\\s|^)" + e.substr(2) + "(?![\\w-])"), "") + ("+" === e.charAt(0) ? " " + e.substr(2) : ""), t.setAttribute("class", a.e), h = rt(t, u, st(t), l, f), t.setAttribute("class", d), a.data = h.firstMPT, t.style.cssText = p, a = a.xfirst = n.parse(t, h.difs, a, o)
                            }
                        });
                        var Gt = function (t) {
                            if ((1 === t || 0 === t) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
                                var e, i, s, r, n, a = this.t.style,
                                    o = h.transform.parse;
                                if ("all" === this.e) a.cssText = "", r = !0;
                                else
                                    for (e = this.e.split(" ").join("").split(","), s = e.length; --s > -1;) i = e[s], h[i] && (h[i].parse === o ? r = !0 : i = "transformOrigin" === i ? Dt : h[i].p), Ht(a, i);
                                r && (Ht(a, At), n = this.t._gsTransform, n && (n.svg && (this.t.removeAttribute("data-svg-origin"), this.t.removeAttribute("transform")), delete this.t._gsTransform))
                            }
                        };
                        for (kt("clearProps", {
                            parser: function (t, e, s, r, n) {
                                return n = new xt(t, s, 0, 0, n, 2), n.setRatio = Gt, n.e = e, n.pr = -10, n.data = r._tween, i = !0, n
                            }
                        }), u = "bezier,throwProps,physicsProps,physics2D".split(","), bt = u.length; bt--;) Ot(u[bt]);
                        u = o.prototype, u._firstPT = u._lastParsedTransform = u._transform = null, u._onInitTween = function (t, e, n, l) {
                            if (!t.nodeType) return !1;
                            this._target = g = t, this._tween = n, this._vars = e, v = l, c = e.autoRound, i = !1, s = e.suffixMap || o.suffixMap, r = K(t, ""), a = this._overwriteProps;
                            var u, d, m, y, x, T, w, b, P, O = t.style;
                            if (f && "" === O.zIndex && (u = tt(t, "zIndex", r), "auto" !== u && "" !== u || this._addLazySet(O, "zIndex", 0)), "string" == typeof e && (y = O.cssText, u = st(t, r), O.cssText = y + ";" + e, u = rt(t, u, st(t)).difs, !W && k.test(e) && (u.opacity = parseFloat(RegExp.$1)), e = u, O.cssText = y), e.className ? this._firstPT = d = h.className.parse(t, e.className, "className", this, null, null, e) : this._firstPT = d = this.parse(t, e, null), this._transformType) {
                                for (P = 3 === this._transformType, At ? _ && (f = !0, "" === O.zIndex && (w = tt(t, "zIndex", r), "auto" !== w && "" !== w || this._addLazySet(O, "zIndex", 0)), p && this._addLazySet(O, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (P ? "visible" : "hidden"))) : O.zoom = 1, m = d; m && m._next;) m = m._next;
                                b = new xt(t, "transform", 0, 0, null, 2), this._linkCSSP(b, null, m), b.setRatio = At ? Vt : qt, b.data = this._transform || Ut(t, r, !0), b.tween = n, b.pr = -1, a.pop()
                            }
                            if (i) {
                                for (; d;) {
                                    for (T = d._next, m = y; m && m.pr > d.pr;) m = m._next;
                                    (d._prev = m ? m._prev : x) ? d._prev._next = d : y = d, (d._next = m) ? m._prev = d : x = d, d = T
                                }
                                this._firstPT = y
                            }
                            return !0
                        }, u.parse = function (t, e, i, n) {
                            var a, o, l, u, f, _, d, p, m, y, x = t.style;
                            for (a in e) {
                                if (_ = e[a], "function" == typeof _ && (_ = _(v, g)), o = h[a]) i = o.parse(t, _, a, this, i, n, e);
                                else {
                                    if ("--" === a.substr(0, 2)) {
                                        this._tween._propLookup[a] = this._addTween.call(this._tween, t.style, "setProperty", K(t).getPropertyValue(a) + "", _ + "", a, !1, a);
                                        continue
                                    }
                                    f = tt(t, a, r) + "", m = "string" == typeof _, "color" === a || "fill" === a || "stroke" === a || a.indexOf("Color") !== -1 || m && C.test(_) ? (m || (_ = dt(_), _ = (_.length > 3 ? "rgba(" : "rgb(") + _.join(",") + ")"), i = wt(x, a, f, _, !0, "transparent", i, 0, n)) : m && j.test(_) ? i = wt(x, a, f, _, !0, null, i, 0, n) : (l = parseFloat(f), d = l || 0 === l ? f.substr((l + "").length) : "", "" !== f && "auto" !== f || ("width" === a || "height" === a ? (l = ot(t, a, r), d = "px") : "left" === a || "top" === a ? (l = it(t, a, r), d = "px") : (l = "opacity" !== a ? 0 : 1, d = "")), y = m && "=" === _.charAt(1), y ? (u = parseInt(_.charAt(0) + "1", 10), _ = _.substr(2), u *= parseFloat(_), p = _.replace(b, "")) : (u = parseFloat(_), p = m ? _.replace(b, "") : ""), "" === p && (p = a in s ? s[a] : d), _ = u || 0 === u ? (y ? u + l : u) + p : e[a], d !== p && ("" === p && "lineHeight" !== a || (u || 0 === u) && l && (l = et(t, a, l, d), "%" === p ? (l /= et(t, a, 100, "%") / 100, e.strictUnits !== !0 && (f = l + "%")) : "em" === p || "rem" === p || "vw" === p || "vh" === p ? l /= et(t, a, 1, p) : "px" !== p && (u = et(t, a, u, p), p = "px"), y && (u || 0 === u) && (_ = u + l + p))), y && (u += l), !l && 0 !== l || !u && 0 !== u ? void 0 !== x[a] && (_ || _ + "" != "NaN" && null != _) ? (i = new xt(x, a, u || l || 0, 0, i, (-1), a, (!1), 0, f, _), i.xs0 = "none" !== _ || "display" !== a && a.indexOf("Style") === -1 ? _ : f) : Z("invalid " + a + " tween value: " + e[a]) : (i = new xt(x, a, l, u - l, i, 0, a, c !== !1 && ("px" === p || "zIndex" === a), 0, f, _), i.xs0 = p))
                                }
                                n && i && !i.plugin && (i.plugin = n)
                            }
                            return i
                        }, u.setRatio = function (t) {
                            var e, i, s, r = this._firstPT,
                                n = 1e-6;
                            if (1 !== t || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
                                if (t || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)
                                    for (; r;) {
                                        if (e = r.c * t + r.s, r.r ? e = Math.round(e) : e < n && e > -n && (e = 0), r.type)
                                            if (1 === r.type)
                                                if (s = r.l, 2 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2;
                                                else if (3 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3;
                                                else if (4 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4;
                                                else if (5 === s) r.t[r.p] = r.xs0 + e + r.xs1 + r.xn1 + r.xs2 + r.xn2 + r.xs3 + r.xn3 + r.xs4 + r.xn4 + r.xs5;
                                                else {
                                                    for (i = r.xs0 + e + r.xs1, s = 1; s < r.l; s++) i += r["xn" + s] + r["xs" + (s + 1)];
                                                    r.t[r.p] = i
                                                } else r.type === -1 ? r.t[r.p] = r.xs0 : r.setRatio && r.setRatio(t);
                                        else r.t[r.p] = e + r.xs0;
                                        r = r._next
                                    } else
                                    for (; r;) 2 !== r.type ? r.t[r.p] = r.b : r.setRatio(t), r = r._next;
                            else
                                for (; r;) {
                                    if (2 !== r.type)
                                        if (r.r && r.type !== -1)
                                            if (e = Math.round(r.s + r.c), r.type) {
                                                if (1 === r.type) {
                                                    for (s = r.l, i = r.xs0 + e + r.xs1, s = 1; s < r.l; s++) i += r["xn" + s] + r["xs" + (s + 1)];
                                                    r.t[r.p] = i
                                                }
                                            } else r.t[r.p] = e + r.xs0;
                                        else r.t[r.p] = r.e;
                                    else r.setRatio(t);
                                    r = r._next
                                }
                        }, u._enableTransforms = function (t) {
                            this._transform = this._transform || Ut(this._target, r, !0), this._transformType = this._transform.svg && Ct || !t && 3 !== this._transformType ? 2 : 3
                        };
                        var Qt = function (t) {
                            this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0)
                        };
                        u._addLazySet = function (t, e, i) {
                            var s = this._firstPT = new xt(t, e, 0, 0, this._firstPT, 2);
                            s.e = i, s.setRatio = Qt, s.data = this
                        }, u._linkCSSP = function (t, e, i, s) {
                            return t && (e && (e._prev = t), t._next && (t._next._prev = t._prev), t._prev ? t._prev._next = t._next : this._firstPT === t && (this._firstPT = t._next, s = !0), i ? i._next = t : s || null !== this._firstPT || (this._firstPT = t), t._next = e, t._prev = i), t
                        }, u._mod = function (t) {
                            for (var e = this._firstPT; e;) "function" == typeof t[e.p] && t[e.p] === Math.round && (e.r = 1), e = e._next
                        }, u._kill = function (e) {
                            var i, s, r, n = e;
                            if (e.autoAlpha || e.alpha) {
                                n = {};
                                for (s in e) n[s] = e[s];
                                n.opacity = 1, n.autoAlpha && (n.visibility = 1)
                            }
                            for (e.className && (i = this._classNamePT) && (r = i.xfirst, r && r._prev ? this._linkCSSP(r._prev, i._next, r._prev._prev) : r === this._firstPT && (this._firstPT = i._next), i._next && this._linkCSSP(i._next, i._next._next, r._prev), this._classNamePT = null), i = this._firstPT; i;) i.plugin && i.plugin !== s && i.plugin._kill && (i.plugin._kill(e), s = i.plugin), i = i._next;
                            return t.prototype._kill.call(this, n)
                        };
                        var Jt = function (t, e, i) {
                            var s, r, n, a;
                            if (t.slice)
                                for (r = t.length; --r > -1;) Jt(t[r], e, i);
                            else
                                for (s = t.childNodes, r = s.length; --r > -1;) n = s[r], a = n.type, n.style && (e.push(st(n)), i && i.push(n)), 1 !== a && 9 !== a && 11 !== a || !n.childNodes.length || Jt(n, e, i)
                        };
                        return o.cascadeTo = function (t, i, s) {
                            var r, n, a, o, l = e.to(t, i, s),
                                h = [l],
                                u = [],
                                c = [],
                                f = [],
                                _ = e._internals.reservedProps;
                            for (t = l._targets || l.target, Jt(t, u, f), l.render(i, !0, !0), Jt(t, c), l.render(0, !0, !0), l._enabled(!0), r = f.length; --r > -1;)
                                if (n = rt(f[r], u[r], c[r]), n.firstMPT) {
                                    n = n.difs;
                                    for (a in s) _[a] && (n[a] = s[a]);
                                    o = {};
                                    for (a in n) o[a] = u[r][a];
                                    h.push(e.fromTo(f[r], i, o, n))
                                }
                            return h
                        }, t.activate([o]), o
                    }, !0),
                    function () {
                        var t = n._gsDefine.plugin({
                            propName: "roundProps",
                            version: "1.6.0",
                            priority: -1,
                            API: 2,
                            init: function (t, e, i) {
                                return this._tween = i, !0
                            }
                        }),
                            e = function (t) {
                                for (; t;) t.f || t.blob || (t.m = Math.round), t = t._next
                            },
                            i = t.prototype;
                        i._onInitAllProps = function () {
                            for (var t, i, s, r = this._tween, n = r.vars.roundProps.join ? r.vars.roundProps : r.vars.roundProps.split(","), a = n.length, o = {}, l = r._propLookup.roundProps; --a > -1;) o[n[a]] = Math.round;
                            for (a = n.length; --a > -1;)
                                for (t = n[a], i = r._firstPT; i;) s = i._next, i.pg ? i.t._mod(o) : i.n === t && (2 === i.f && i.t ? e(i.t._firstPT) : (this._add(i.t, t, i.s, i.c), s && (s._prev = i._prev), i._prev ? i._prev._next = s : r._firstPT === i && (r._firstPT = s), i._next = i._prev = null, r._propLookup[t] = l)), i = s;
                            return !1
                        }, i._add = function (t, e, i, s) {
                            this._addTween(t, e, i, i + s, e, Math.round), this._overwriteProps.push(e)
                        }
                    }(),
                    function () {
                        n._gsDefine.plugin({
                            propName: "attr",
                            API: 2,
                            version: "0.6.1",
                            init: function (t, e, i, s) {
                                var r, n;
                                if ("function" != typeof t.setAttribute) return !1;
                                for (r in e) n = e[r], "function" == typeof n && (n = n(s, t)), this._addTween(t, "setAttribute", t.getAttribute(r) + "", n + "", r, !1, r), this._overwriteProps.push(r);
                                return !0
                            }
                        })
                    }(), n._gsDefine.plugin({
                        propName: "directionalRotation",
                        version: "0.3.1",
                        API: 2,
                        init: function (t, e, i, s) {
                            "object" != typeof e && (e = {
                                rotation: e
                            }), this.finals = {};
                            var r, n, a, o, l, h, u = e.useRadians === !0 ? 2 * Math.PI : 360,
                                c = 1e-6;
                            for (r in e) "useRadians" !== r && (o = e[r], "function" == typeof o && (o = o(s, t)), h = (o + "").split("_"), n = h[0], a = parseFloat("function" != typeof t[r] ? t[r] : t[r.indexOf("set") || "function" != typeof t["get" + r.substr(3)] ? r : "get" + r.substr(3)]()), o = this.finals[r] = "string" == typeof n && "=" === n.charAt(1) ? a + parseInt(n.charAt(0) + "1", 10) * Number(n.substr(2)) : Number(n) || 0, l = o - a, h.length && (n = h.join("_"), n.indexOf("short") !== -1 && (l %= u, l !== l % (u / 2) && (l = l < 0 ? l + u : l - u)), n.indexOf("_cw") !== -1 && l < 0 ? l = (l + 9999999999 * u) % u - (l / u | 0) * u : n.indexOf("ccw") !== -1 && l > 0 && (l = (l - 9999999999 * u) % u - (l / u | 0) * u)), (l > c || l < -c) && (this._addTween(t, r, a, a + l, r), this._overwriteProps.push(r)));
                            return !0
                        },
                        set: function (t) {
                            var e;
                            if (1 !== t) this._super.setRatio.call(this, t);
                            else
                                for (e = this._firstPT; e;) e.f ? e.t[e.p](this.finals[e.p]) : e.t[e.p] = this.finals[e.p], e = e._next
                        }
                    })._autoCSS = !0, n._gsDefine("easing.Back", ["easing.Ease"], function (t) {
                        var e, i, s, r = n.GreenSockGlobals || n,
                            a = r.com.greensock,
                            o = 2 * Math.PI,
                            l = Math.PI / 2,
                            h = a._class,
                            u = function (e, i) {
                                var s = h("easing." + e, function () { }, !0),
                                    r = s.prototype = new t;
                                return r.constructor = s, r.getRatio = i, s
                            },
                            c = t.register || function () { },
                            f = function (t, e, i, s, r) {
                                var n = h("easing." + t, {
                                    easeOut: new e,
                                    easeIn: new i,
                                    easeInOut: new s
                                }, !0);
                                return c(n, t), n
                            },
                            _ = function (t, e, i) {
                                this.t = t, this.v = e, i && (this.next = i, i.prev = this, this.c = i.v - e, this.gap = i.t - t)
                            },
                            d = function (e, i) {
                                var s = h("easing." + e, function (t) {
                                    this._p1 = t || 0 === t ? t : 1.70158, this._p2 = 1.525 * this._p1
                                }, !0),
                                    r = s.prototype = new t;
                                return r.constructor = s, r.getRatio = i, r.config = function (t) {
                                    return new s(t)
                                }, s
                            },
                            p = f("Back", d("BackOut", function (t) {
                                return (t -= 1) * t * ((this._p1 + 1) * t + this._p1) + 1
                            }), d("BackIn", function (t) {
                                return t * t * ((this._p1 + 1) * t - this._p1)
                            }), d("BackInOut", function (t) {
                                return (t *= 2) < 1 ? .5 * t * t * ((this._p2 + 1) * t - this._p2) : .5 * ((t -= 2) * t * ((this._p2 + 1) * t + this._p2) + 2)
                            })),
                            m = h("easing.SlowMo", function (t, e, i) {
                                e = e || 0 === e ? e : .7, null == t ? t = .7 : t > 1 && (t = 1), this._p = 1 !== t ? e : 0, this._p1 = (1 - t) / 2, this._p2 = t, this._p3 = this._p1 + this._p2, this._calcEnd = i === !0
                            }, !0),
                            g = m.prototype = new t;
                        return g.constructor = m, g.getRatio = function (t) {
                            var e = t + (.5 - t) * this._p;
                            return t < this._p1 ? this._calcEnd ? 1 - (t = 1 - t / this._p1) * t : e - (t = 1 - t / this._p1) * t * t * t * e : t > this._p3 ? this._calcEnd ? 1 - (t = (t - this._p3) / this._p1) * t : e + (t - e) * (t = (t - this._p3) / this._p1) * t * t * t : this._calcEnd ? 1 : e
                        }, m.ease = new m(.7, .7), g.config = m.config = function (t, e, i) {
                            return new m(t, e, i)
                        }, e = h("easing.SteppedEase", function (t, e) {
                            t = t || 1, this._p1 = 1 / t, this._p2 = t + (e ? 0 : 1), this._p3 = e ? 1 : 0
                        }, !0), g = e.prototype = new t, g.constructor = e, g.getRatio = function (t) {
                            return t < 0 ? t = 0 : t >= 1 && (t = .999999999), ((this._p2 * t | 0) + this._p3) * this._p1
                        }, g.config = e.config = function (t, i) {
                            return new e(t, i)
                        }, i = h("easing.RoughEase", function (e) {
                            e = e || {};
                            for (var i, s, r, n, a, o, l = e.taper || "none", h = [], u = 0, c = 0 | (e.points || 20), f = c, d = e.randomize !== !1, p = e.clamp === !0, m = e.template instanceof t ? e.template : null, g = "number" == typeof e.strength ? .4 * e.strength : .4; --f > -1;) i = d ? Math.random() : 1 / c * f, s = m ? m.getRatio(i) : i, "none" === l ? r = g : "out" === l ? (n = 1 - i, r = n * n * g) : "in" === l ? r = i * i * g : i < .5 ? (n = 2 * i, r = n * n * .5 * g) : (n = 2 * (1 - i), r = n * n * .5 * g), d ? s += Math.random() * r - .5 * r : f % 2 ? s += .5 * r : s -= .5 * r, p && (s > 1 ? s = 1 : s < 0 && (s = 0)), h[u++] = {
                                x: i,
                                y: s
                            };
                            for (h.sort(function (t, e) {
                                return t.x - e.x
                            }), o = new _(1, 1, null), f = c; --f > -1;) a = h[f], o = new _(a.x, a.y, o);
                            this._prev = new _(0, 0, 0 !== o.t ? o : o.next)
                        }, !0), g = i.prototype = new t, g.constructor = i, g.getRatio = function (t) {
                            var e = this._prev;
                            if (t > e.t) {
                                for (; e.next && t >= e.t;) e = e.next;
                                e = e.prev
                            } else
                                for (; e.prev && t <= e.t;) e = e.prev;
                            return this._prev = e, e.v + (t - e.t) / e.gap * e.c
                        }, g.config = function (t) {
                            return new i(t)
                        }, i.ease = new i, f("Bounce", u("BounceOut", function (t) {
                            return t < 1 / 2.75 ? 7.5625 * t * t : t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375
                        }), u("BounceIn", function (t) {
                            return (t = 1 - t) < 1 / 2.75 ? 1 - 7.5625 * t * t : t < 2 / 2.75 ? 1 - (7.5625 * (t -= 1.5 / 2.75) * t + .75) : t < 2.5 / 2.75 ? 1 - (7.5625 * (t -= 2.25 / 2.75) * t + .9375) : 1 - (7.5625 * (t -= 2.625 / 2.75) * t + .984375)
                        }), u("BounceInOut", function (t) {
                            var e = t < .5;
                            return t = e ? 1 - 2 * t : 2 * t - 1, t = t < 1 / 2.75 ? 7.5625 * t * t : t < 2 / 2.75 ? 7.5625 * (t -= 1.5 / 2.75) * t + .75 : t < 2.5 / 2.75 ? 7.5625 * (t -= 2.25 / 2.75) * t + .9375 : 7.5625 * (t -= 2.625 / 2.75) * t + .984375, e ? .5 * (1 - t) : .5 * t + .5
                        })), f("Circ", u("CircOut", function (t) {
                            return Math.sqrt(1 - (t -= 1) * t)
                        }), u("CircIn", function (t) {
                            return -(Math.sqrt(1 - t * t) - 1)
                        }), u("CircInOut", function (t) {
                            return (t *= 2) < 1 ? -.5 * (Math.sqrt(1 - t * t) - 1) : .5 * (Math.sqrt(1 - (t -= 2) * t) + 1)
                        })), s = function (e, i, s) {
                            var r = h("easing." + e, function (t, e) {
                                this._p1 = t >= 1 ? t : 1, this._p2 = (e || s) / (t < 1 ? t : 1), this._p3 = this._p2 / o * (Math.asin(1 / this._p1) || 0), this._p2 = o / this._p2
                            }, !0),
                                n = r.prototype = new t;
                            return n.constructor = r, n.getRatio = i, n.config = function (t, e) {
                                return new r(t, e)
                            }, r
                        }, f("Elastic", s("ElasticOut", function (t) {
                            return this._p1 * Math.pow(2, -10 * t) * Math.sin((t - this._p3) * this._p2) + 1
                        }, .3), s("ElasticIn", function (t) {
                            return -(this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2))
                        }, .3), s("ElasticInOut", function (t) {
                            return (t *= 2) < 1 ? -.5 * (this._p1 * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2)) : this._p1 * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - this._p3) * this._p2) * .5 + 1
                        }, .45)), f("Expo", u("ExpoOut", function (t) {
                            return 1 - Math.pow(2, -10 * t)
                        }), u("ExpoIn", function (t) {
                            return Math.pow(2, 10 * (t - 1)) - .001
                        }), u("ExpoInOut", function (t) {
                            return (t *= 2) < 1 ? .5 * Math.pow(2, 10 * (t - 1)) : .5 * (2 - Math.pow(2, -10 * (t - 1)))
                        })), f("Sine", u("SineOut", function (t) {
                            return Math.sin(t * l)
                        }), u("SineIn", function (t) {
                            return -Math.cos(t * l) + 1
                        }), u("SineInOut", function (t) {
                            return -.5 * (Math.cos(Math.PI * t) - 1)
                        })), h("easing.EaseLookup", {
                            find: function (e) {
                                return t.map[e]
                            }
                        }, !0), c(r.SlowMo, "SlowMo", "ease,"), c(i, "RoughEase", "ease,"), c(e, "SteppedEase", "ease,"), p
                    }, !0)
            }), n._gsDefine && n._gsQueue.pop()(),
                function (i, n) {
                    "use strict";
                    var a = {},
                        o = i.document,
                        l = i.GreenSockGlobals = i.GreenSockGlobals || i;
                    if (!l.TweenLite) {
                        var h, u, c, f, _, d = function (t) {
                            var e, i = t.split("."),
                                s = l;
                            for (e = 0; e < i.length; e++) s[i[e]] = s = s[i[e]] || {};
                            return s
                        },
                            p = d("com.greensock"),
                            m = 1e-10,
                            g = function (t) {
                                var e, i = [],
                                    s = t.length;
                                for (e = 0; e !== s; i.push(t[e++]));
                                return i
                            },
                            v = function () { },
                            y = function () {
                                var t = Object.prototype.toString,
                                    e = t.call([]);
                                return function (i) {
                                    return null != i && (i instanceof Array || "object" == typeof i && !!i.push && t.call(i) === e)
                                }
                            }(),
                            x = {},
                            T = function (i, o, h, u) {
                                this.sc = x[i] ? x[i].sc : [], x[i] = this, this.gsClass = null, this.func = h;
                                var c = [];
                                this.check = function (f) {
                                    for (var _, p, m, g, v = o.length, y = v; --v > -1;)(_ = x[o[v]] || new T(o[v], [])).gsClass ? (c[v] = _.gsClass, y--) : f && _.sc.push(this);
                                    if (0 === y && h) {
                                        if (p = ("com.greensock." + i).split("."), m = p.pop(), g = d(p.join("."))[m] = this.gsClass = h.apply(h, c), u)
                                            if (l[m] = a[m] = g, "undefined" != typeof t && t.exports)
                                                if (i === n) {
                                                    t.exports = a[n] = g;
                                                    for (v in a) g[v] = a[v]
                                                } else a[n] && (a[n][m] = g);
                                            else s = [], r = function () {
                                                return g
                                            }.apply(e, s), !(void 0 !== r && (t.exports = r));
                                        for (v = 0; v < this.sc.length; v++) this.sc[v].check()
                                    }
                                }, this.check(!0)
                            },
                            w = i._gsDefine = function (t, e, i, s) {
                                return new T(t, e, i, s)
                            },
                            b = p._class = function (t, e, i) {
                                return e = e || function () { }, w(t, [], function () {
                                    return e
                                }, i), e
                            };
                        w.globals = l;
                        var P = [0, 0, 1, 1],
                            k = b("easing.Ease", function (t, e, i, s) {
                                this._func = t, this._type = i || 0, this._power = s || 0, this._params = e ? P.concat(e) : P
                            }, !0),
                            O = k.map = {},
                            C = k.register = function (t, e, i, s) {
                                for (var r, n, a, o, l = e.split(","), h = l.length, u = (i || "easeIn,easeOut,easeInOut").split(","); --h > -1;)
                                    for (n = l[h], r = s ? b("easing." + n, null, !0) : p.easing[n] || {}, a = u.length; --a > -1;) o = u[a], O[n + "." + o] = O[o + n] = r[o] = t.getRatio ? t : t[o] || new t
                            };
                        for (c = k.prototype, c._calcEnd = !1, c.getRatio = function (t) {
                            if (this._func) return this._params[0] = t, this._func.apply(null, this._params);
                            var e = this._type,
                                i = this._power,
                                s = 1 === e ? 1 - t : 2 === e ? t : t < .5 ? 2 * t : 2 * (1 - t);
                            return 1 === i ? s *= s : 2 === i ? s *= s * s : 3 === i ? s *= s * s * s : 4 === i && (s *= s * s * s * s), 1 === e ? 1 - s : 2 === e ? s : t < .5 ? s / 2 : 1 - s / 2
                        }, h = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], u = h.length; --u > -1;) c = h[u] + ",Power" + u, C(new k(null, null, 1, u), c, "easeOut", !0), C(new k(null, null, 2, u), c, "easeIn" + (0 === u ? ",easeNone" : "")), C(new k(null, null, 3, u), c, "easeInOut");
                        O.linear = p.easing.Linear.easeIn, O.swing = p.easing.Quad.easeInOut;
                        var S = b("events.EventDispatcher", function (t) {
                            this._listeners = {}, this._eventTarget = t || this
                        });
                        c = S.prototype, c.addEventListener = function (t, e, i, s, r) {
                            r = r || 0;
                            var n, a, o = this._listeners[t],
                                l = 0;
                            for (this !== f || _ || f.wake(), null == o && (this._listeners[t] = o = []), a = o.length; --a > -1;) n = o[a], n.c === e && n.s === i ? o.splice(a, 1) : 0 === l && n.pr < r && (l = a + 1);
                            o.splice(l, 0, {
                                c: e,
                                s: i,
                                up: s,
                                pr: r
                            })
                        }, c.removeEventListener = function (t, e) {
                            var i, s = this._listeners[t];
                            if (s)
                                for (i = s.length; --i > -1;)
                                    if (s[i].c === e) return void s.splice(i, 1)
                        }, c.dispatchEvent = function (t) {
                            var e, i, s, r = this._listeners[t];
                            if (r)
                                for (e = r.length, e > 1 && (r = r.slice(0)), i = this._eventTarget; --e > -1;) s = r[e], s && (s.up ? s.c.call(s.s || i, {
                                    type: t,
                                    target: i
                                }) : s.c.call(s.s || i))
                        };
                        var A = i.requestAnimationFrame,
                            R = i.cancelAnimationFrame,
                            D = Date.now || function () {
                                return (new Date).getTime()
                            },
                            M = D();
                        for (h = ["ms", "moz", "webkit", "o"], u = h.length; --u > -1 && !A;) A = i[h[u] + "RequestAnimationFrame"], R = i[h[u] + "CancelAnimationFrame"] || i[h[u] + "CancelRequestAnimationFrame"];
                        b("Ticker", function (t, e) {
                            var i, s, r, n, a, l = this,
                                h = D(),
                                u = !(e === !1 || !A) && "auto",
                                c = 500,
                                d = 33,
                                p = "tick",
                                g = function (t) {
                                    var e, o, u = D() - M;
                                    u > c && (h += u - d), M += u, l.time = (M - h) / 1e3, e = l.time - a, (!i || e > 0 || t === !0) && (l.frame++ , a += e + (e >= n ? .004 : n - e), o = !0), t !== !0 && (r = s(g)), o && l.dispatchEvent(p)
                                };
                            S.call(l), l.time = l.frame = 0, l.tick = function () {
                                g(!0)
                            }, l.lagSmoothing = function (t, e) {
                                c = t || 1 / m, d = Math.min(e, c, 0)
                            }, l.sleep = function () {
                                null != r && (u && R ? R(r) : clearTimeout(r), s = v, r = null, l === f && (_ = !1))
                            }, l.wake = function (t) {
                                null !== r ? l.sleep() : t ? h += -M + (M = D()) : l.frame > 10 && (M = D() - c + 5), s = 0 === i ? v : u && A ? A : function (t) {
                                    return setTimeout(t, 1e3 * (a - l.time) + 1 | 0)
                                }, l === f && (_ = !0), g(2)
                            }, l.fps = function (t) {
                                return arguments.length ? (i = t, n = 1 / (i || 60), a = this.time + n, void l.wake()) : i
                            }, l.useRAF = function (t) {
                                return arguments.length ? (l.sleep(), u = t, void l.fps(i)) : u
                            }, l.fps(t), setTimeout(function () {
                                "auto" === u && l.frame < 5 && "hidden" !== o.visibilityState && l.useRAF(!1)
                            }, 1500)
                        }), c = p.Ticker.prototype = new p.events.EventDispatcher, c.constructor = p.Ticker;
                        var E = b("core.Animation", function (t, e) {
                            if (this.vars = e = e || {}, this._duration = this._totalDuration = t || 0, this._delay = Number(e.delay) || 0, this._timeScale = 1, this._active = e.immediateRender === !0, this.data = e.data, this._reversed = e.reversed === !0, K) {
                                _ || f.wake();
                                var i = this.vars.useFrames ? J : K;
                                i.add(this, i._time), this.vars.paused && this.paused(!0)
                            }
                        });
                        f = E.ticker = new p.Ticker, c = E.prototype, c._dirty = c._gc = c._initted = c._paused = !1, c._totalTime = c._time = 0, c._rawPrevTime = -1, c._next = c._last = c._onUpdate = c._timeline = c.timeline = null, c._paused = !1;
                        var F = function () {
                            _ && D() - M > 2e3 && "hidden" !== o.visibilityState && f.wake();
                            var t = setTimeout(F, 2e3);
                            t.unref && t.unref()
                        };
                        F(), c.play = function (t, e) {
                            return null != t && this.seek(t, e), this.reversed(!1).paused(!1)
                        }, c.pause = function (t, e) {
                            return null != t && this.seek(t, e), this.paused(!0)
                        }, c.resume = function (t, e) {
                            return null != t && this.seek(t, e), this.paused(!1)
                        }, c.seek = function (t, e) {
                            return this.totalTime(Number(t), e !== !1)
                        }, c.restart = function (t, e) {
                            return this.reversed(!1).paused(!1).totalTime(t ? -this._delay : 0, e !== !1, !0)
                        }, c.reverse = function (t, e) {
                            return null != t && this.seek(t || this.totalDuration(), e), this.reversed(!0).paused(!1)
                        }, c.render = function (t, e, i) { }, c.invalidate = function () {
                            return this._time = this._totalTime = 0, this._initted = this._gc = !1, this._rawPrevTime = -1, !this._gc && this.timeline || this._enabled(!0), this
                        }, c.isActive = function () {
                            var t, e = this._timeline,
                                i = this._startTime;
                            return !e || !this._gc && !this._paused && e.isActive() && (t = e.rawTime(!0)) >= i && t < i + this.totalDuration() / this._timeScale - 1e-7
                        }, c._enabled = function (t, e) {
                            return _ || f.wake(), this._gc = !t, this._active = this.isActive(), e !== !0 && (t && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !t && this.timeline && this._timeline._remove(this, !0)), !1
                        }, c._kill = function (t, e) {
                            return this._enabled(!1, !1)
                        }, c.kill = function (t, e) {
                            return this._kill(t, e), this
                        }, c._uncache = function (t) {
                            for (var e = t ? this : this.timeline; e;) e._dirty = !0, e = e.timeline;
                            return this
                        }, c._swapSelfInParams = function (t) {
                            for (var e = t.length, i = t.concat(); --e > -1;) "{self}" === t[e] && (i[e] = this);
                            return i
                        }, c._callback = function (t) {
                            var e = this.vars,
                                i = e[t],
                                s = e[t + "Params"],
                                r = e[t + "Scope"] || e.callbackScope || this,
                                n = s ? s.length : 0;
                            switch (n) {
                                case 0:
                                    i.call(r);
                                    break;
                                case 1:
                                    i.call(r, s[0]);
                                    break;
                                case 2:
                                    i.call(r, s[0], s[1]);
                                    break;
                                default:
                                    i.apply(r, s)
                            }
                        }, c.eventCallback = function (t, e, i, s) {
                            if ("on" === (t || "").substr(0, 2)) {
                                var r = this.vars;
                                if (1 === arguments.length) return r[t];
                                null == e ? delete r[t] : (r[t] = e, r[t + "Params"] = y(i) && i.join("").indexOf("{self}") !== -1 ? this._swapSelfInParams(i) : i, r[t + "Scope"] = s), "onUpdate" === t && (this._onUpdate = e)
                            }
                            return this
                        }, c.delay = function (t) {
                            return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + t - this._delay), this._delay = t, this) : this._delay
                        }, c.duration = function (t) {
                            return arguments.length ? (this._duration = this._totalDuration = t, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== t && this.totalTime(this._totalTime * (t / this._duration), !0), this) : (this._dirty = !1, this._duration)
                        }, c.totalDuration = function (t) {
                            return this._dirty = !1, arguments.length ? this.duration(t) : this._totalDuration
                        }, c.time = function (t, e) {
                            return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(t > this._duration ? this._duration : t, e)) : this._time
                        }, c.totalTime = function (t, e, i) {
                            if (_ || f.wake(), !arguments.length) return this._totalTime;
                            if (this._timeline) {
                                if (t < 0 && !i && (t += this.totalDuration()), this._timeline.smoothChildTiming) {
                                    this._dirty && this.totalDuration();
                                    var s = this._totalDuration,
                                        r = this._timeline;
                                    if (t > s && !i && (t = s), this._startTime = (this._paused ? this._pauseTime : r._time) - (this._reversed ? s - t : t) / this._timeScale, r._dirty || this._uncache(!1), r._timeline)
                                        for (; r._timeline;) r._timeline._time !== (r._startTime + r._totalTime) / r._timeScale && r.totalTime(r._totalTime, !0), r = r._timeline
                                }
                                this._gc && this._enabled(!0, !1), this._totalTime === t && 0 !== this._duration || (N.length && et(), this.render(t, e, !1), N.length && et())
                            }
                            return this
                        }, c.progress = c.totalProgress = function (t, e) {
                            var i = this.duration();
                            return arguments.length ? this.totalTime(i * t, e) : i ? this._time / i : this.ratio
                        }, c.startTime = function (t) {
                            return arguments.length ? (t !== this._startTime && (this._startTime = t, this.timeline && this.timeline._sortChildren && this.timeline.add(this, t - this._delay)), this) : this._startTime
                        }, c.endTime = function (t) {
                            return this._startTime + (0 != t ? this.totalDuration() : this.duration()) / this._timeScale
                        }, c.timeScale = function (t) {
                            if (!arguments.length) return this._timeScale;
                            if (t = t || m, this._timeline && this._timeline.smoothChildTiming) {
                                var e = this._pauseTime,
                                    i = e || 0 === e ? e : this._timeline.totalTime();
                                this._startTime = i - (i - this._startTime) * this._timeScale / t
                            }
                            return this._timeScale = t, this._uncache(!1)
                        }, c.reversed = function (t) {
                            return arguments.length ? (t != this._reversed && (this._reversed = t, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed
                        }, c.paused = function (t) {
                            if (!arguments.length) return this._paused;
                            var e, i, s = this._timeline;
                            return t != this._paused && s && (_ || t || f.wake(), e = s.rawTime(), i = e - this._pauseTime, !t && s.smoothChildTiming && (this._startTime += i, this._uncache(!1)), this._pauseTime = t ? e : null, this._paused = t, this._active = this.isActive(), !t && 0 !== i && this._initted && this.duration() && (e = s.smoothChildTiming ? this._totalTime : (e - this._startTime) / this._timeScale, this.render(e, e === this._totalTime, !0))), this._gc && !t && this._enabled(!0, !1), this
                        };
                        var z = b("core.SimpleTimeline", function (t) {
                            E.call(this, 0, t), this.autoRemoveChildren = this.smoothChildTiming = !0
                        });
                        c = z.prototype = new E, c.constructor = z, c.kill()._gc = !1, c._first = c._last = c._recent = null, c._sortChildren = !1, c.add = c.insert = function (t, e, i, s) {
                            var r, n;
                            if (t._startTime = Number(e || 0) + t._delay, t._paused && this !== t._timeline && (t._pauseTime = t._startTime + (this.rawTime() - t._startTime) / t._timeScale), t.timeline && t.timeline._remove(t, !0), t.timeline = t._timeline = this, t._gc && t._enabled(!0, !0), r = this._last, this._sortChildren)
                                for (n = t._startTime; r && r._startTime > n;) r = r._prev;
                            return r ? (t._next = r._next, r._next = t) : (t._next = this._first,
                                this._first = t), t._next ? t._next._prev = t : this._last = t, t._prev = r, this._recent = t, this._timeline && this._uncache(!0), this
                        }, c._remove = function (t, e) {
                            return t.timeline === this && (e || t._enabled(!1, !0), t._prev ? t._prev._next = t._next : this._first === t && (this._first = t._next), t._next ? t._next._prev = t._prev : this._last === t && (this._last = t._prev), t._next = t._prev = t.timeline = null, t === this._recent && (this._recent = this._last), this._timeline && this._uncache(!0)), this
                        }, c.render = function (t, e, i) {
                            var s, r = this._first;
                            for (this._totalTime = this._time = this._rawPrevTime = t; r;) s = r._next, (r._active || t >= r._startTime && !r._paused && !r._gc) && (r._reversed ? r.render((r._dirty ? r.totalDuration() : r._totalDuration) - (t - r._startTime) * r._timeScale, e, i) : r.render((t - r._startTime) * r._timeScale, e, i)), r = s
                        }, c.rawTime = function () {
                            return _ || f.wake(), this._totalTime
                        };
                        var j = b("TweenLite", function (t, e, s) {
                            if (E.call(this, e, s), this.render = j.prototype.render, null == t) throw "Cannot tween a null target.";
                            this.target = t = "string" != typeof t ? t : j.selector(t) || t;
                            var r, n, a, o = t.jquery || t.length && t !== i && t[0] && (t[0] === i || t[0].nodeType && t[0].style && !t.nodeType),
                                l = this.vars.overwrite;
                            if (this._overwrite = l = null == l ? Q[j.defaultOverwrite] : "number" == typeof l ? l >> 0 : Q[l], (o || t instanceof Array || t.push && y(t)) && "number" != typeof t[0])
                                for (this._targets = a = g(t), this._propLookup = [], this._siblings = [], r = 0; r < a.length; r++) n = a[r], n ? "string" != typeof n ? n.length && n !== i && n[0] && (n[0] === i || n[0].nodeType && n[0].style && !n.nodeType) ? (a.splice(r--, 1), this._targets = a = a.concat(g(n))) : (this._siblings[r] = it(n, this, !1), 1 === l && this._siblings[r].length > 1 && rt(n, this, null, 1, this._siblings[r])) : (n = a[r--] = j.selector(n), "string" == typeof n && a.splice(r + 1, 1)) : a.splice(r--, 1);
                            else this._propLookup = {}, this._siblings = it(t, this, !1), 1 === l && this._siblings.length > 1 && rt(t, this, null, 1, this._siblings);
                            (this.vars.immediateRender || 0 === e && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -m, this.render(Math.min(0, -this._delay)))
                        }, !0),
                            L = function (t) {
                                return t && t.length && t !== i && t[0] && (t[0] === i || t[0].nodeType && t[0].style && !t.nodeType)
                            },
                            I = function (t, e) {
                                var i, s = {};
                                for (i in t) G[i] || i in e && "transform" !== i && "x" !== i && "y" !== i && "width" !== i && "height" !== i && "className" !== i && "border" !== i || !(!W[i] || W[i] && W[i]._autoCSS) || (s[i] = t[i], delete t[i]);
                                t.css = s
                            };
                        c = j.prototype = new E, c.constructor = j, c.kill()._gc = !1, c.ratio = 0, c._firstPT = c._targets = c._overwrittenProps = c._startAt = null, c._notifyPluginsOfEnabled = c._lazy = !1, j.version = "1.20.2", j.defaultEase = c._ease = new k(null, null, 1, 1), j.defaultOverwrite = "auto", j.ticker = f, j.autoSleep = 120, j.lagSmoothing = function (t, e) {
                            f.lagSmoothing(t, e)
                        }, j.selector = i.$ || i.jQuery || function (t) {
                            var e = i.$ || i.jQuery;
                            return e ? (j.selector = e, e(t)) : "undefined" == typeof o ? t : o.querySelectorAll ? o.querySelectorAll(t) : o.getElementById("#" === t.charAt(0) ? t.substr(1) : t)
                        };
                        var N = [],
                            X = {},
                            B = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,
                            $ = /[\+-]=-?[\.\d]/,
                            Y = function (t) {
                                for (var e, i = this._firstPT, s = 1e-6; i;) e = i.blob ? 1 === t && this.end ? this.end : t ? this.join("") : this.start : i.c * t + i.s, i.m ? e = i.m(e, this._target || i.t) : e < s && e > -s && !i.blob && (e = 0), i.f ? i.fp ? i.t[i.p](i.fp, e) : i.t[i.p](e) : i.t[i.p] = e, i = i._next
                            },
                            U = function (t, e, i, s) {
                                var r, n, a, o, l, h, u, c = [],
                                    f = 0,
                                    _ = "",
                                    d = 0;
                                for (c.start = t, c.end = e, t = c[0] = t + "", e = c[1] = e + "", i && (i(c), t = c[0], e = c[1]), c.length = 0, r = t.match(B) || [], n = e.match(B) || [], s && (s._next = null, s.blob = 1, c._firstPT = c._applyPT = s), l = n.length, o = 0; o < l; o++) u = n[o], h = e.substr(f, e.indexOf(u, f) - f), _ += h || !o ? h : ",", f += h.length, d ? d = (d + 1) % 5 : "rgba(" === h.substr(-5) && (d = 1), u === r[o] || r.length <= o ? _ += u : (_ && (c.push(_), _ = ""), a = parseFloat(r[o]), c.push(a), c._firstPT = {
                                    _next: c._firstPT,
                                    t: c,
                                    p: c.length - 1,
                                    s: a,
                                    c: ("=" === u.charAt(1) ? parseInt(u.charAt(0) + "1", 10) * parseFloat(u.substr(2)) : parseFloat(u) - a) || 0,
                                    f: 0,
                                    m: d && d < 4 ? Math.round : 0
                                }), f += u.length;
                                return _ += e.substr(f), _ && c.push(_), c.setRatio = Y, $.test(e) && (c.end = 0), c
                            },
                            q = function (t, e, i, s, r, n, a, o, l) {
                                "function" == typeof s && (s = s(l || 0, t));
                                var h, u = typeof t[e],
                                    c = "function" !== u ? "" : e.indexOf("set") || "function" != typeof t["get" + e.substr(3)] ? e : "get" + e.substr(3),
                                    f = "get" !== i ? i : c ? a ? t[c](a) : t[c]() : t[e],
                                    _ = "string" == typeof s && "=" === s.charAt(1),
                                    d = {
                                        t: t,
                                        p: e,
                                        s: f,
                                        f: "function" === u,
                                        pg: 0,
                                        n: r || e,
                                        m: n ? "function" == typeof n ? n : Math.round : 0,
                                        pr: 0,
                                        c: _ ? parseInt(s.charAt(0) + "1", 10) * parseFloat(s.substr(2)) : parseFloat(s) - f || 0
                                    };
                                if (("number" != typeof f || "number" != typeof s && !_) && (a || isNaN(f) || !_ && isNaN(s) || "boolean" == typeof f || "boolean" == typeof s ? (d.fp = a, h = U(f, _ ? parseFloat(d.s) + d.c : s, o || j.defaultStringFilter, d), d = {
                                    t: h,
                                    p: "setRatio",
                                    s: 0,
                                    c: 1,
                                    f: 2,
                                    pg: 0,
                                    n: r || e,
                                    pr: 0,
                                    m: 0
                                }) : (d.s = parseFloat(f), _ || (d.c = parseFloat(s) - d.s || 0))), d.c) return (d._next = this._firstPT) && (d._next._prev = d), this._firstPT = d, d
                            },
                            V = j._internals = {
                                isArray: y,
                                isSelector: L,
                                lazyTweens: N,
                                blobDif: U
                            },
                            W = j._plugins = {},
                            H = V.tweenLookup = {},
                            Z = 0,
                            G = V.reservedProps = {
                                ease: 1,
                                delay: 1,
                                overwrite: 1,
                                onComplete: 1,
                                onCompleteParams: 1,
                                onCompleteScope: 1,
                                useFrames: 1,
                                runBackwards: 1,
                                startAt: 1,
                                onUpdate: 1,
                                onUpdateParams: 1,
                                onUpdateScope: 1,
                                onStart: 1,
                                onStartParams: 1,
                                onStartScope: 1,
                                onReverseComplete: 1,
                                onReverseCompleteParams: 1,
                                onReverseCompleteScope: 1,
                                onRepeat: 1,
                                onRepeatParams: 1,
                                onRepeatScope: 1,
                                easeParams: 1,
                                yoyo: 1,
                                immediateRender: 1,
                                repeat: 1,
                                repeatDelay: 1,
                                data: 1,
                                paused: 1,
                                reversed: 1,
                                autoCSS: 1,
                                lazy: 1,
                                onOverwrite: 1,
                                callbackScope: 1,
                                stringFilter: 1,
                                id: 1,
                                yoyoEase: 1
                            },
                            Q = {
                                none: 0,
                                all: 1,
                                auto: 2,
                                concurrent: 3,
                                allOnStart: 4,
                                preexisting: 5,
                                "true": 1,
                                "false": 0
                            },
                            J = E._rootFramesTimeline = new z,
                            K = E._rootTimeline = new z,
                            tt = 30,
                            et = V.lazyRender = function () {
                                var t, e = N.length;
                                for (X = {}; --e > -1;) t = N[e], t && t._lazy !== !1 && (t.render(t._lazy[0], t._lazy[1], !0), t._lazy = !1);
                                N.length = 0
                            };
                        K._startTime = f.time, J._startTime = f.frame, K._active = J._active = !0, setTimeout(et, 1), E._updateRoot = j.render = function () {
                            var t, e, i;
                            if (N.length && et(), K.render((f.time - K._startTime) * K._timeScale, !1, !1), J.render((f.frame - J._startTime) * J._timeScale, !1, !1), N.length && et(), f.frame >= tt) {
                                tt = f.frame + (parseInt(j.autoSleep, 10) || 120);
                                for (i in H) {
                                    for (e = H[i].tweens, t = e.length; --t > -1;) e[t]._gc && e.splice(t, 1);
                                    0 === e.length && delete H[i]
                                }
                                if (i = K._first, (!i || i._paused) && j.autoSleep && !J._first && 1 === f._listeners.tick.length) {
                                    for (; i && i._paused;) i = i._next;
                                    i || f.sleep()
                                }
                            }
                        }, f.addEventListener("tick", E._updateRoot);
                        var it = function (t, e, i) {
                            var s, r, n = t._gsTweenID;
                            if (H[n || (t._gsTweenID = n = "t" + Z++)] || (H[n] = {
                                target: t,
                                tweens: []
                            }), e && (s = H[n].tweens, s[r = s.length] = e, i))
                                for (; --r > -1;) s[r] === e && s.splice(r, 1);
                            return H[n].tweens
                        },
                            st = function (t, e, i, s) {
                                var r, n, a = t.vars.onOverwrite;
                                return a && (r = a(t, e, i, s)), a = j.onOverwrite, a && (n = a(t, e, i, s)), r !== !1 && n !== !1
                            },
                            rt = function (t, e, i, s, r) {
                                var n, a, o, l;
                                if (1 === s || s >= 4) {
                                    for (l = r.length, n = 0; n < l; n++)
                                        if ((o = r[n]) !== e) o._gc || o._kill(null, t, e) && (a = !0);
                                        else if (5 === s) break;
                                    return a
                                }
                                var h, u = e._startTime + m,
                                    c = [],
                                    f = 0,
                                    _ = 0 === e._duration;
                                for (n = r.length; --n > -1;)(o = r[n]) === e || o._gc || o._paused || (o._timeline !== e._timeline ? (h = h || nt(e, 0, _), 0 === nt(o, h, _) && (c[f++] = o)) : o._startTime <= u && o._startTime + o.totalDuration() / o._timeScale > u && ((_ || !o._initted) && u - o._startTime <= 2e-10 || (c[f++] = o)));
                                for (n = f; --n > -1;)
                                    if (o = c[n], 2 === s && o._kill(i, t, e) && (a = !0), 2 !== s || !o._firstPT && o._initted) {
                                        if (2 !== s && !st(o, e)) continue;
                                        o._enabled(!1, !1) && (a = !0)
                                    }
                                return a
                            },
                            nt = function (t, e, i) {
                                for (var s = t._timeline, r = s._timeScale, n = t._startTime; s._timeline;) {
                                    if (n += s._startTime, r *= s._timeScale, s._paused) return -100;
                                    s = s._timeline
                                }
                                return n /= r, n > e ? n - e : i && n === e || !t._initted && n - e < 2 * m ? m : (n += t.totalDuration() / t._timeScale / r) > e + m ? 0 : n - e - m
                            };
                        c._init = function () {
                            var t, e, i, s, r, n, a = this.vars,
                                o = this._overwrittenProps,
                                l = this._duration,
                                h = !!a.immediateRender,
                                u = a.ease;
                            if (a.startAt) {
                                this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), r = {};
                                for (s in a.startAt) r[s] = a.startAt[s];
                                if (r.overwrite = !1, r.immediateRender = !0, r.lazy = h && a.lazy !== !1, r.startAt = r.delay = null, r.onUpdate = a.onUpdate, r.onUpdateScope = a.onUpdateScope || a.callbackScope || this, this._startAt = j.to(this.target, 0, r), h)
                                    if (this._time > 0) this._startAt = null;
                                    else if (0 !== l) return
                            } else if (a.runBackwards && 0 !== l)
                                if (this._startAt) this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null;
                                else {
                                    0 !== this._time && (h = !1), i = {};
                                    for (s in a) G[s] && "autoCSS" !== s || (i[s] = a[s]);
                                    if (i.overwrite = 0, i.data = "isFromStart", i.lazy = h && a.lazy !== !1, i.immediateRender = h, this._startAt = j.to(this.target, 0, i), h) {
                                        if (0 === this._time) return
                                    } else this._startAt._init(), this._startAt._enabled(!1), this.vars.immediateRender && (this._startAt = null)
                                }
                            if (this._ease = u = u ? u instanceof k ? u : "function" == typeof u ? new k(u, a.easeParams) : O[u] || j.defaultEase : j.defaultEase, a.easeParams instanceof Array && u.config && (this._ease = u.config.apply(u, a.easeParams)), this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)
                                for (n = this._targets.length, t = 0; t < n; t++) this._initProps(this._targets[t], this._propLookup[t] = {}, this._siblings[t], o ? o[t] : null, t) && (e = !0);
                            else e = this._initProps(this.target, this._propLookup, this._siblings, o, 0);
                            if (e && j._onPluginEvent("_onInitAllProps", this), o && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), a.runBackwards)
                                for (i = this._firstPT; i;) i.s += i.c, i.c = -i.c, i = i._next;
                            this._onUpdate = a.onUpdate, this._initted = !0
                        }, c._initProps = function (t, e, s, r, n) {
                            var a, o, l, h, u, c;
                            if (null == t) return !1;
                            X[t._gsTweenID] && et(), this.vars.css || t.style && t !== i && t.nodeType && W.css && this.vars.autoCSS !== !1 && I(this.vars, t);
                            for (a in this.vars)
                                if (c = this.vars[a], G[a]) c && (c instanceof Array || c.push && y(c)) && c.join("").indexOf("{self}") !== -1 && (this.vars[a] = c = this._swapSelfInParams(c, this));
                                else if (W[a] && (h = new W[a])._onInitTween(t, this.vars[a], this, n)) {
                                    for (this._firstPT = u = {
                                        _next: this._firstPT,
                                        t: h,
                                        p: "setRatio",
                                        s: 0,
                                        c: 1,
                                        f: 1,
                                        n: a,
                                        pg: 1,
                                        pr: h._priority,
                                        m: 0
                                    }, o = h._overwriteProps.length; --o > -1;) e[h._overwriteProps[o]] = this._firstPT;
                                    (h._priority || h._onInitAllProps) && (l = !0), (h._onDisable || h._onEnable) && (this._notifyPluginsOfEnabled = !0), u._next && (u._next._prev = u)
                                } else e[a] = q.call(this, t, a, "get", c, a, 0, null, this.vars.stringFilter, n);
                            return r && this._kill(r, t) ? this._initProps(t, e, s, r, n) : this._overwrite > 1 && this._firstPT && s.length > 1 && rt(t, this, e, this._overwrite, s) ? (this._kill(e, t), this._initProps(t, e, s, r, n)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (X[t._gsTweenID] = !0), l)
                        }, c.render = function (t, e, i) {
                            var s, r, n, a, o = this._time,
                                l = this._duration,
                                h = this._rawPrevTime;
                            if (t >= l - 1e-7 && t >= 0) this._totalTime = this._time = l, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (s = !0, r = "onComplete", i = i || this._timeline.autoRemoveChildren), 0 === l && (this._initted || !this.vars.lazy || i) && (this._startTime === this._timeline._duration && (t = 0), (h < 0 || t <= 0 && t >= -1e-7 || h === m && "isPause" !== this.data) && h !== t && (i = !0, h > m && (r = "onReverseComplete")), this._rawPrevTime = a = !e || t || h === t ? t : m);
                            else if (t < 1e-7) this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== o || 0 === l && h > 0) && (r = "onReverseComplete", s = this._reversed), t < 0 && (this._active = !1, 0 === l && (this._initted || !this.vars.lazy || i) && (h >= 0 && (h !== m || "isPause" !== this.data) && (i = !0), this._rawPrevTime = a = !e || t || h === t ? t : m)), (!this._initted || this._startAt && this._startAt.progress()) && (i = !0);
                            else if (this._totalTime = this._time = t, this._easeType) {
                                var u = t / l,
                                    c = this._easeType,
                                    f = this._easePower;
                                (1 === c || 3 === c && u >= .5) && (u = 1 - u), 3 === c && (u *= 2), 1 === f ? u *= u : 2 === f ? u *= u * u : 3 === f ? u *= u * u * u : 4 === f && (u *= u * u * u * u), 1 === c ? this.ratio = 1 - u : 2 === c ? this.ratio = u : t / l < .5 ? this.ratio = u / 2 : this.ratio = 1 - u / 2
                            } else this.ratio = this._ease.getRatio(t / l);
                            if (this._time !== o || i) {
                                if (!this._initted) {
                                    if (this._init(), !this._initted || this._gc) return;
                                    if (!i && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = this._totalTime = o, this._rawPrevTime = h, N.push(this), void (this._lazy = [t, e]);
                                    this._time && !s ? this.ratio = this._ease.getRatio(this._time / l) : s && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                                }
                                for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== o && t >= 0 && (this._active = !0), 0 === o && (this._startAt && (t >= 0 ? this._startAt.render(t, e, i) : r || (r = "_dummyGS")), this.vars.onStart && (0 === this._time && 0 !== l || e || this._callback("onStart"))), n = this._firstPT; n;) n.f ? n.t[n.p](n.c * this.ratio + n.s) : n.t[n.p] = n.c * this.ratio + n.s, n = n._next;
                                this._onUpdate && (t < 0 && this._startAt && t !== -1e-4 && this._startAt.render(t, e, i), e || (this._time !== o || s || i) && this._callback("onUpdate")), r && (this._gc && !i || (t < 0 && this._startAt && !this._onUpdate && t !== -1e-4 && this._startAt.render(t, e, i), s && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[r] && this._callback(r), 0 === l && this._rawPrevTime === m && a !== m && (this._rawPrevTime = 0)))
                            }
                        }, c._kill = function (t, e, i) {
                            if ("all" === t && (t = null), null == t && (null == e || e === this.target)) return this._lazy = !1, this._enabled(!1, !1);
                            e = "string" != typeof e ? e || this._targets || this.target : j.selector(e) || e;
                            var s, r, n, a, o, l, h, u, c, f = i && this._time && i._startTime === this._startTime && this._timeline === i._timeline;
                            if ((y(e) || L(e)) && "number" != typeof e[0])
                                for (s = e.length; --s > -1;) this._kill(t, e[s], i) && (l = !0);
                            else {
                                if (this._targets) {
                                    for (s = this._targets.length; --s > -1;)
                                        if (e === this._targets[s]) {
                                            o = this._propLookup[s] || {}, this._overwrittenProps = this._overwrittenProps || [], r = this._overwrittenProps[s] = t ? this._overwrittenProps[s] || {} : "all";
                                            break
                                        }
                                } else {
                                    if (e !== this.target) return !1;
                                    o = this._propLookup, r = this._overwrittenProps = t ? this._overwrittenProps || {} : "all"
                                }
                                if (o) {
                                    if (h = t || o, u = t !== r && "all" !== r && t !== o && ("object" != typeof t || !t._tempKill), i && (j.onOverwrite || this.vars.onOverwrite)) {
                                        for (n in h) o[n] && (c || (c = []), c.push(n));
                                        if ((c || !t) && !st(this, i, e, c)) return !1
                                    }
                                    for (n in h) (a = o[n]) && (f && (a.f ? a.t[a.p](a.s) : a.t[a.p] = a.s, l = !0), a.pg && a.t._kill(h) && (l = !0), a.pg && 0 !== a.t._overwriteProps.length || (a._prev ? a._prev._next = a._next : a === this._firstPT && (this._firstPT = a._next), a._next && (a._next._prev = a._prev), a._next = a._prev = null), delete o[n]), u && (r[n] = 1);
                                    !this._firstPT && this._initted && this._enabled(!1, !1)
                                }
                            }
                            return l
                        }, c.invalidate = function () {
                            return this._notifyPluginsOfEnabled && j._onPluginEvent("_onDisable", this), this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null, this._notifyPluginsOfEnabled = this._active = this._lazy = !1, this._propLookup = this._targets ? {} : [], E.prototype.invalidate.call(this), this.vars.immediateRender && (this._time = -m, this.render(Math.min(0, -this._delay))), this
                        }, c._enabled = function (t, e) {
                            if (_ || f.wake(), t && this._gc) {
                                var i, s = this._targets;
                                if (s)
                                    for (i = s.length; --i > -1;) this._siblings[i] = it(s[i], this, !0);
                                else this._siblings = it(this.target, this, !0)
                            }
                            return E.prototype._enabled.call(this, t, e), !(!this._notifyPluginsOfEnabled || !this._firstPT) && j._onPluginEvent(t ? "_onEnable" : "_onDisable", this)
                        }, j.to = function (t, e, i) {
                            return new j(t, e, i)
                        }, j.from = function (t, e, i) {
                            return i.runBackwards = !0, i.immediateRender = 0 != i.immediateRender, new j(t, e, i)
                        }, j.fromTo = function (t, e, i, s) {
                            return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, new j(t, e, s)
                        }, j.delayedCall = function (t, e, i, s, r) {
                            return new j(e, 0, {
                                delay: t,
                                onComplete: e,
                                onCompleteParams: i,
                                callbackScope: s,
                                onReverseComplete: e,
                                onReverseCompleteParams: i,
                                immediateRender: !1,
                                lazy: !1,
                                useFrames: r,
                                overwrite: 0
                            })
                        }, j.set = function (t, e) {
                            return new j(t, 0, e)
                        }, j.getTweensOf = function (t, e) {
                            if (null == t) return [];
                            t = "string" != typeof t ? t : j.selector(t) || t;
                            var i, s, r, n;
                            if ((y(t) || L(t)) && "number" != typeof t[0]) {
                                for (i = t.length, s = []; --i > -1;) s = s.concat(j.getTweensOf(t[i], e));
                                for (i = s.length; --i > -1;)
                                    for (n = s[i], r = i; --r > -1;) n === s[r] && s.splice(i, 1)
                            } else if (t._gsTweenID)
                                for (s = it(t).concat(), i = s.length; --i > -1;)(s[i]._gc || e && !s[i].isActive()) && s.splice(i, 1);
                            return s || []
                        }, j.killTweensOf = j.killDelayedCallsTo = function (t, e, i) {
                            "object" == typeof e && (i = e, e = !1);
                            for (var s = j.getTweensOf(t, e), r = s.length; --r > -1;) s[r]._kill(i, t)
                        };
                        var at = b("plugins.TweenPlugin", function (t, e) {
                            this._overwriteProps = (t || "").split(","), this._propName = this._overwriteProps[0], this._priority = e || 0, this._super = at.prototype
                        }, !0);
                        if (c = at.prototype, at.version = "1.19.0", at.API = 2, c._firstPT = null, c._addTween = q, c.setRatio = Y, c._kill = function (t) {
                            var e, i = this._overwriteProps,
                                s = this._firstPT;
                            if (null != t[this._propName]) this._overwriteProps = [];
                            else
                                for (e = i.length; --e > -1;) null != t[i[e]] && i.splice(e, 1);
                            for (; s;) null != t[s.n] && (s._next && (s._next._prev = s._prev), s._prev ? (s._prev._next = s._next, s._prev = null) : this._firstPT === s && (this._firstPT = s._next)), s = s._next;
                            return !1
                        }, c._mod = c._roundProps = function (t) {
                            for (var e, i = this._firstPT; i;) e = t[this._propName] || null != i.n && t[i.n.split(this._propName + "_").join("")], e && "function" == typeof e && (2 === i.f ? i.t._applyPT.m = e : i.m = e), i = i._next
                        }, j._onPluginEvent = function (t, e) {
                            var i, s, r, n, a, o = e._firstPT;
                            if ("_onInitAllProps" === t) {
                                for (; o;) {
                                    for (a = o._next, s = r; s && s.pr > o.pr;) s = s._next;
                                    (o._prev = s ? s._prev : n) ? o._prev._next = o : r = o, (o._next = s) ? s._prev = o : n = o, o = a
                                }
                                o = e._firstPT = r
                            }
                            for (; o;) o.pg && "function" == typeof o.t[t] && o.t[t]() && (i = !0), o = o._next;
                            return i
                        }, at.activate = function (t) {
                            for (var e = t.length; --e > -1;) t[e].API === at.API && (W[(new t[e])._propName] = t[e]);
                            return !0
                        }, w.plugin = function (t) {
                            if (!(t && t.propName && t.init && t.API)) throw "illegal plugin definition.";
                            var e, i = t.propName,
                                s = t.priority || 0,
                                r = t.overwriteProps,
                                n = {
                                    init: "_onInitTween",
                                    set: "setRatio",
                                    kill: "_kill",
                                    round: "_mod",
                                    mod: "_mod",
                                    initAll: "_onInitAllProps"
                                },
                                a = b("plugins." + i.charAt(0).toUpperCase() + i.substr(1) + "Plugin", function () {
                                    at.call(this, i, s), this._overwriteProps = r || []
                                }, t.global === !0),
                                o = a.prototype = new at(i);
                            o.constructor = a, a.API = t.API;
                            for (e in n) "function" == typeof t[e] && (o[n[e]] = t[e]);
                            return a.version = t.version, at.activate([a]), a
                        }, h = i._gsQueue) {
                            for (u = 0; u < h.length; u++) h[u]();
                            for (c in x) x[c].func || i.console.log("GSAP encountered missing dependency: " + c)
                        }
                        _ = !1
                    }
                }("undefined" != typeof t && t.exports && "undefined" != typeof i ? i : this || window, "TweenMax")
        }).call(e, function () {
            return this
        }())
    },
    20: function (t, e, i) {
        ! function (e) {
            "object" == typeof t && "object" == typeof t.exports ? e(i(1), window, document) : e(jQuery, window, document)
        }(function (t, e, i, s) {
            var r = [],
                n = function () {
                    return r.length ? r[r.length - 1] : null
                },
                a = function () {
                    var t, e = !1;
                    for (t = r.length - 1; t >= 0; t--) r[t].$blocker && (r[t].$blocker.toggleClass("current", !e).toggleClass("behind", e), e = !0)
                };
            t.modal = function (e, i) {
                var s, a;
                if (this.$body = t("body"), this.options = t.extend({}, t.modal.defaults, i), this.options.doFade = !isNaN(parseInt(this.options.fadeDuration, 10)), this.$blocker = null, this.options.closeExisting)
                    for (; t.modal.isActive();) t.modal.close();
                if (r.push(this), e.is("a"))
                    if (a = e.attr("href"), this.anchor = e, /^#/.test(a)) {
                        if (this.$elm = t(a), 1 !== this.$elm.length) return null;
                        this.$body.append(this.$elm), this.open()
                    } else this.$elm = t("<div>"), this.$body.append(this.$elm), s = function (t, e) {
                        e.elm.remove()
                    }, this.showSpinner(), e.trigger(t.modal.AJAX_SEND), t.get(a).done(function (i) {
                        if (t.modal.isActive()) {
                            e.trigger(t.modal.AJAX_SUCCESS);
                            var r = n();
                            r.$elm.empty().append(i).on(t.modal.CLOSE, s), r.hideSpinner(), r.open(), e.trigger(t.modal.AJAX_COMPLETE)
                        }
                    }).fail(function () {
                        e.trigger(t.modal.AJAX_FAIL);
                        var i = n();
                        i.hideSpinner(), r.pop(), e.trigger(t.modal.AJAX_COMPLETE)
                    });
                else this.$elm = e, this.anchor = e, this.$body.append(this.$elm), this.open()
            }, t.modal.prototype = {
                constructor: t.modal,
                open: function () {
                    var e = this;
                    this.block(), this.anchor.blur(), this.options.doFade ? setTimeout(function () {
                        e.show()
                    }, this.options.fadeDuration * this.options.fadeDelay) : this.show(), t(i).off("keydown.modal").on("keydown.modal", function (t) {
                        var e = n();
                        27 === t.which && e.options.escapeClose && e.close()
                    }), this.options.clickClose && this.$blocker.click(function (e) {
                        e.target === this && t.modal.close()
                    })
                },
                close: function () {
                    r.pop(), this.unblock(), this.hide(), t.modal.isActive() || t(i).off("keydown.modal")
                },
                block: function () {
                    this.$elm.trigger(t.modal.BEFORE_BLOCK, [this._ctx()]), this.$body.css("overflow", "hidden"), this.$blocker = t('<div class="' + this.options.blockerClass + ' blocker current"></div>').appendTo(this.$body), a(), this.options.doFade && this.$blocker.css("opacity", 0).animate({
                        opacity: 1
                    }, this.options.fadeDuration), this.$elm.trigger(t.modal.BLOCK, [this._ctx()])
                },
                unblock: function (e) {
                    !e && this.options.doFade ? this.$blocker.fadeOut(this.options.fadeDuration, this.unblock.bind(this, !0)) : (this.$blocker.children().appendTo(this.$body), this.$blocker.remove(), this.$blocker = null, a(), t.modal.isActive() || this.$body.css("overflow", ""))
                },
                show: function () {
                    this.$elm.trigger(t.modal.BEFORE_OPEN, [this._ctx()]), this.options.showClose && (this.closeButton = t('<a href="#close-modal" rel="modal:close" class="close-modal ' + this.options.closeClass + '">' + this.options.closeText + "</a>"), this.$elm.append(this.closeButton)), this.$elm.addClass(this.options.modalClass).appendTo(this.$blocker), this.options.doFade ? this.$elm.css({
                        opacity: 0,
                        display: "inline-block"
                    }).animate({
                        opacity: 1
                    }, this.options.fadeDuration) : this.$elm.css("display", "inline-block"), this.$elm.trigger(t.modal.OPEN, [this._ctx()])
                },
                hide: function () {
                    this.$elm.trigger(t.modal.BEFORE_CLOSE, [this._ctx()]), this.closeButton && this.closeButton.remove();
                    var e = this;
                    this.options.doFade ? this.$elm.fadeOut(this.options.fadeDuration, function () {
                        e.$elm.trigger(t.modal.AFTER_CLOSE, [e._ctx()])
                    }) : this.$elm.hide(0, function () {
                        e.$elm.trigger(t.modal.AFTER_CLOSE, [e._ctx()])
                    }), this.$elm.trigger(t.modal.CLOSE, [this._ctx()])
                },
                showSpinner: function () {
                    this.options.showSpinner && (this.spinner = this.spinner || t('<div class="' + this.options.modalClass + '-spinner"></div>').append(this.options.spinnerHtml), this.$body.append(this.spinner), this.spinner.show())
                },
                hideSpinner: function () {
                    this.spinner && this.spinner.remove()
                },
                _ctx: function () {
                    return {
                        elm: this.$elm,
                        $elm: this.$elm,
                        $blocker: this.$blocker,
                        options: this.options
                    }
                }
            }, t.modal.close = function (e) {
                if (t.modal.isActive()) {
                    e && e.preventDefault();
                    var i = n();
                    return i.close(), i.$elm
                }
            }, t.modal.isActive = function () {
                return r.length > 0
            }, t.modal.getCurrent = n, t.modal.defaults = {
                closeExisting: !0,
                escapeClose: !0,
                clickClose: !0,
                closeText: "Close",
                closeClass: "",
                modalClass: "modal",
                blockerClass: "jquery-modal",
                spinnerHtml: '<div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div>',
                showSpinner: !0,
                showClose: !0,
                fadeDuration: null,
                fadeDelay: 1
            }, t.modal.BEFORE_BLOCK = "modal:before-block", t.modal.BLOCK = "modal:block", t.modal.BEFORE_OPEN = "modal:before-open", t.modal.OPEN = "modal:open", t.modal.BEFORE_CLOSE = "modal:before-close", t.modal.CLOSE = "modal:close", t.modal.AFTER_CLOSE = "modal:after-close", t.modal.AJAX_SEND = "modal:ajax:send", t.modal.AJAX_SUCCESS = "modal:ajax:success", t.modal.AJAX_FAIL = "modal:ajax:fail", t.modal.AJAX_COMPLETE = "modal:ajax:complete", t.fn.modal = function (e) {
                return 1 === this.length && new t.modal(this, e), this
            }, t(i).on("click.modal", 'a[rel~="modal:close"]', t.modal.close), t(i).on("click.modal", 'a[rel~="modal:open"]', function (e) {
                e.preventDefault(), t(this).modal()
            })
        })
    }
});
